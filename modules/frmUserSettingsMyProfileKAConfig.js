var frmUserSettingsMyProfileKAConfig = {
	"formid":"frmUserSettingsMyProfileKA",
	"frmUserSettingsMyProfileKA":{
		"entity":"User",
		"objectServiceName":"RBObjects",
		"objectServiceOptions":{"access":"online"},
	},
	"lblCustomerSegmet":{
		"fieldprops": {
			"entity":"User",
			"field":"segment_code",
			"widgettype":"Label"
		}
	},
	"lblFirstName":{
		"fieldprops": {
			"entity":"User",
			"field":"customer_name",
			"widgettype":"Label"
		}
	},
	"lblLastName":{
		"fieldprops": {
			"entity":"User",
			"field":"last_name",
			"widgettype":"Label"
		}
	},
	"lblSMSNumber":{
		"fieldprops": {
			"entity":"User",
			"field":"smsno",
			"widgettype":"Label"
		}
	},
	"lblMobileNumber":{
    	"fieldprops":{
        	"entity":"User",
        	"field":"telno",
        	"widgettype":"Label"
        }
    },
	"lblLandLineNumber":{
    	"fieldprops":{
        	"entity":"User",
        	"field":"telno",
        	"widgettype":"Label"
        }
    },
	"lblEmailAddress":{
		"fieldprops": {
			"entity":"User",
			"field":"email",
			"widgettype":"Label"
		}
	},
	"lblAddressLine01":{
		"fieldprops": {
			"entity":"User",
			"field":"addr1",
			"widgettype":"Label"
		}
	},
	"lblAddressLine02":{
		"fieldprops": {
			"entity":"User",
			"field":"addr2",
			"widgettype":"Label"
		}
	},
	"lblProfileCity":{
		"fieldprops": {
			"entity":"User",
			"field":"city",
			"widgettype":"Label"
		}
	},
	"lblProfileCountry":{
		"fieldprops": {
			"entity":"User",
			"field":"country_code",
			"widgettype":"Label"
		}
	}
};