var getAppConfig = true;
var isLoggedIn = false;
var fromBackground = false;
var autoTouchShowFlag = false;


function uncaughtExceptionHandler(exceptionObject) {
  kony.print("In uncaughtExceptionHandler...exceptionObject Json :: " + JSON.stringify(exceptionObject) + "  ::  Normal err :: " + exceptionObject);
  if(!isiWatchRequest){
    var exceptionString = "";
    kony.application.dismissLoadingScreen();
    var errorMessage = kony.i18n.getLocalizedString("i18n.common.somethingwentwrong");
    if(exceptionObject.errorCode === "2300" || exceptionObject.errorCode === 2300){
      customAlertPopup(exceptionObject.name, exceptionObject.message, UncaughtExceptionOk, "");
    }else{
      //#ifdef iphone
      if(kony.application.getCurrentForm().id === "frmJoMoPay" || kony.application.getCurrentForm().id === "frmJOMOPayAddBiller"){
        customAlertPopup(exceptionObject.NSLocalizedDescription, exceptionObject.NSLocalizedFailureReason, UncaughtExceptionOk, "");
      }else{
        customAlertPopup(geti18nkey("i18n.common.internalHeader"), errorMessage, UncaughtExceptionOk, "");
      }
      //#endif
      //#ifdef android
      customAlertPopup(geti18nkey("i18n.common.internalHeader"), errorMessage, UncaughtExceptionOk, "");
      //#endif
    }
  }else{
    kony.print("Iwatch :: Inside uncaughtExceptionHandler call "+ JSON.stringify(exceptionObject));
    if ((!isEmpty(exceptionObject))){
      if(!isLoggedIn){
        kony.print("Iwatch :: Inside calling the fetch ");
        fetchApplicationPropertiesWithoutLoading();
      }
    }

    if(iwatchServicecall == "cards"){
      return_watchrequest({"cards": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "accounts"){
      return_watchrequest({"accounts": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "transactions"){
      return_watchrequest({"transactions": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "loans"){
      return_watchrequest({"loans": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "deposits"){
      return_watchrequest({"deposits": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "isUserLoggedIn"){
      return_watchrequest({"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "cardTransactions"){
      return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "loanInfo"){
      return_watchrequest({"loanInfo": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }else if(iwatchServicecall == "depositInfo"){
      return_watchrequest({"depositInfo": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
    }
    isiWatchRequest = false;
    return;
  }
}

function UncaughtExceptionOk() {
  popupCommonAlertDimiss();
}

function onactiveCallback() {
  kony.print("------onactiveCallback-------------");
}

function oninactiveCallback() {
  kony.print("------oninactiveCallback-------------");
}

function onbackgroundCallback() {
  kony.print("------onbackgroundCallback-------------");
  fromBackground = true;
}

function onforegroundCallback() {
  try {
    kony.print("------onforegroundCallback-------------");
    if (fromBackground && (isLoggedIn === false)) { //isLoggedIn &&
      if (kony.sdk.isNetworkAvailable()) {
        fetchApplicationPropertiesWithoutLoading();
      } else {
        kony.print("-->onforegroundCallback err");
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.common.appalaunchnointernet"), popupCommonAlertDimissclose, "");
      }
    }
    var currentform = kony.application.getCurrentForm();
    if(!isEmpty(currentform) &&  currentform.id === "frmLoginKA")
      kony.boj.maintainenceCHECK();
    fromBackground = false;
  } catch (err) {
    kony.print("err in call :: " + err);
    if((isLoggedIn === false)){
      fetchApplicationPropertiesWithoutLoading();
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }
}


function popupCommonAlertDimissclose() {
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  kony.application.exit();
}

function checkbhu(error)
{
  if (error.details !== null && error.details !== undefined && error.details.indexOf("InvalidToken")>0) {
    if (!isLoggedIn) {
      fetchApplicationPropertiesWithoutLoading();
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
  }
}