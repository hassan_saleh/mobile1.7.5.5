//For logging the Excpetion related to UI or Service.

function exceptionLogCall(exserviceName,errorCode,errorType,description){
  if(kony.sdk.isNetworkAvailable()){
kony.print("exceptionLogCall description :: "+ description);
    var scopeObj = this;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {
      "session_token": kony.retailBanking.globalData.session_token
    };
    var serviceName = "RBObjects";

    var record = {};
    var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
    exserviceName = kony.application.getCurrentForm().id+"->"+exserviceName;
    if(isEmpty(kony.application.getCurrentForm().id)){
      if(!isLoggedIn){
        if (kony.store.getItem("langFlagSettingObj")) {
          exserviceName =  frmLoginKA+"->"+exserviceName;
        } else {
          exserviceName =  frmLanguageChange+"->"+exserviceName;
        }
      }
    }
    if(!isEmpty(gblTModule))
    	exserviceName = gblTModule+"->"+exserviceName;
    dataObject.addField("custId",custid);
    dataObject.addField("loggerchannel","MOBILE");
    dataObject.addField("serviceName",exserviceName);
    dataObject.addField("errorCode",errorCode);
    dataObject.addField("errorType",errorType);
    if(description!= undefined && description!= null && description!=""){
      description = ""+description;
      description = description.replace(/\n/g, " ");
    }
    else{
      description = "something went wrong";
    }

    if(description.length >2000){
      description= description.substring(0,2000);
    }

    dataObject.addField("description",description);


    var serviceOptions = {"dataObject":dataObject,"headers":headers};

    kony.print("Input params exceptionLogCall -->"+ JSON.stringify(serviceOptions) );
    modelObj.customVerb("ExceptionLogVerb",serviceOptions, BOJExceptionLogVerbSuccessCallback, BOJExceptionLogVerbErrorCallback);
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function BOJExceptionLogVerbSuccessCallback(resObj) {
  excLogcall = false;
  kony.print("In success of BOJExceptionLogVerbSuccessCallback ::  " + JSON.stringify(resObj));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}


function BOJExceptionLogVerbErrorCallback(errObj) {
  excLogcall = false;
  kony.print("In Error of BOJExceptionLogVerbErrorCallback ::  " + JSON.stringify(errObj));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}