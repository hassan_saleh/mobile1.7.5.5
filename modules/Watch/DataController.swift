//
//  HelperController.swift
//  KWatchOS2 Extension
//
//  Created by BOJ on 10/09/18.
//

// import Foundation


// MARK: - Declarations

var menuToken = 0
var menuListToken = 0
var listScreen = 1
var listDetailsScreen = 2
var launchScreen = 3
var loanAndDepositDetailsScreen = 4

var accounts = NSArray()
var deposits = NSArray()
var loans = NSArray()
var cards = NSArray()

var kSession = "session"
var selectedLoanCT = "";
var selectedDepositeCT = "";

var selectedLoanamount = "";
var selectedDepositeamount = "";

enum Menu: Int {
    case accounts, cards, loans, deposits
    var title: String {
        switch self {
        case .accounts:
            return "Accounts"
        case .cards:
            return "Cards"
        case .loans:
            return "Loans"
        case .deposits:
            return "Deposits"
        }
    }
}

// MARK: - Helper methods

func showAlert(message: String) {
  let action = WKAlertAction(title: "OK", style: WKAlertActionStyle.default ) {
  let a = "Please try again.";
  let b = "No internet connection. Please check your network settings and try again.";
  let c = "Please try again later.";
    
    if(message == b || message == c ){
      moveToMenuScreen();
    }else if(message == a){
       moveToSplashScreen();
    }
    else{
      print("Ok")
    }
  }
  WKExtension.shared().rootInterfaceController?.presentAlert(withTitle: "BOJ", message: message, preferredStyle: WKAlertControllerStyle.alert, actions:[action])
}

func checkUserLoggedIn(response: [String : Any]) -> Bool {

  if let isNetworkAvailable = response["isNetworkAvailable"] as? Bool, false == isNetworkAvailable {
    showAlert(message: "No internet connection. Please check your network settings and try again.")
    return false
  }

  if let isRetry = response["isRetry"] as? Bool, false == isRetry {
    showAlert(message: "Please try again.")
    return false
  }

  if let isLoggedIn = response["isUserLoggedIn"] as? Bool, false == isLoggedIn {
    moveToSplashScreen()
    showAlert(message: "Please register your device to continue.")
    return false
  }
  return true
}

func moveToMenuScreen() {
    WKInterfaceController.reloadRootControllers(
        withNames: ["frmWatMenu"], contexts: []
    )
    let context = ["token": ""]
    WKExtension.shared().rootInterfaceController?.pushController(withName: "frmWatMenu", context: context);
}

func moveToSplashScreen() {
    WKInterfaceController.reloadRootControllers(
        withNames: ["frmWatLaunch"], contexts: []
    )
    let context = ["token": ""]
    WKExtension.shared().rootInterfaceController?.pushController(withName: "frmWatLaunch", context: context);
}

func getDateString(dateString: String) -> String {
    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = "yyyy-MM-dd"
    if let date = inputFormatter.date(from: dateString) {
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = "dd MMM yyyy"
        return outputFormatter.string(from: date)
    }
    return dateString
}

// MARK: - Data controller

func fetchData(form: AnyObject, param: [String: AnyObject], screen : Int) {
    
    func phoneCommunicationSuccess(fetchResults:[String : Any]) {
      print("result of iwatch  :: " , fetchResults)
        DispatchQueue.main.async {
            
            if screen != launchScreen && false == checkUserLoggedIn(response: fetchResults) {
                return
            }
            
            if screen == listScreen {
                if let aForm = form as? frmWatMenuItemListController {
                    bindListScreenData(form: aForm, response: fetchResults)
                }
            }
            else if screen == listDetailsScreen {
                if let aForm = form as? frmWatMenuListItemDetailsController {
                    if let menu = Menu(rawValue: menuToken) {
                        if menu == .accounts || menu == .cards {
                            bindListDetailsScreenData(form: aForm, response: fetchResults)
                        }
                    }
                }
            }
            else if screen == loanAndDepositDetailsScreen {
                if let aLDForm = form as? frmWatMenuLAndDDetailsController {
                    if let menu = Menu(rawValue: menuToken) {
                        if menu == .loans || menu == .deposits {
                            bindLoanAndDepositsDetailsScreenData(form: aLDForm, response: fetchResults)
                        }
                    }
                }
            }
            else if screen == launchScreen {
                if let aForm = form as? frmWatLaunchController {
                    bindLaunchScreenData(form: aForm, response: fetchResults)
                }
            }
        }
    }
    
    func phoneCommunicationFailure(error:NSError) {
        showAlert(message: "Error while connecting apple watch.")
        
        DispatchQueue.main.async {
            if screen == listScreen {
                if let aForm = form as? frmWatMenuItemListController {
                    aForm.lblLoading.setHidden(true)
                    aForm.lblSummery.setHidden(true)
                }
            }
            else if screen == listDetailsScreen {
                if let aForm = form as? frmWatMenuListItemDetailsController {
                    aForm.lblLoading.setHidden(true)
                    aForm.lblSummery.setHidden(true)
                }
            }
            else if screen == loanAndDepositDetailsScreen {
                if let aLDForm = form as? frmWatMenuLAndDDetailsController {
                    aLDForm.lblLoading.setHidden(true)
                    aLDForm.lblSummery.setHidden(true)
                }
            }else{
              showAlert(message: "Please try again later.")
            }
        }
    }
    
    let phoneCommunicator = PhoneCommunicator.getSharedInstance();
    if phoneCommunicator.isActivate() {
        phoneCommunicator.requestData(message: param, replyHandler: phoneCommunicationSuccess, errorHandler: phoneCommunicationFailure as? ((Error) -> Void));
    }
    else {
        phoneCommunicator.activate(completionHandler: { (status) in
            if status {
                phoneCommunicator.requestData(message: param, replyHandler: phoneCommunicationSuccess, errorHandler: phoneCommunicationFailure as? ((Error) -> Void));
            }else{
                showAlert(message: "Please try again later.")
            }
        });
    }
    
}



