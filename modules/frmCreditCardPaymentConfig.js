var frmCreditCardPaymentConfig= {
  "formid": "frmCreditCardPayment",
  "frmCreditCardPayment": {
    "entity": "Cards",
    "objectServiceName": "RBObjects",
    "objectServiceOptions" : {"access":"online"}
  },

  "hiddenLblAccountNum":{
    "fieldprops":{
      "entity": "Cards",
      "widgettype": "Label",
      "field": "fromAccountNumber"
    }
  },
  "lblBranchNum":{
    "fieldprops":{
      "entity": "Cards",
      "widgettype": "Label",
      "field": "SourceBranchCode"
    }
  },

  "hiddenCardNumber":{
    "fieldprops":{
      "entity": "Cards",
      "widgettype": "Label",
      "field": "CardNum"
    }
  },

  "lblHiddenAmount":{
    "fieldprops":{
      "entity": "Cards",
      "widgettype":"Label",
      "field":"amount"
    }
  },
  
  "lblPaymentFlag":{
    "fieldprops":{
      "entity": "Cards",
      "widgettype":"Label",
      "field":"PaymentFlag"
    }
  },
  
  "lblDum":{
    "fieldprops":{
      "entity": "Cards",
      "widgettype":"Label",
      "field":"p_fcdb_ref_no"
    }
  }
  
  
 
};

