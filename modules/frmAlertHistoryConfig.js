var frmAlertHistoryConfig= {
  "formid": "frmAlertHistory",
  "frmAlertHistory": {
    "entity": "Messages",
    "objectServiceName": "RBObjects",
    "objectServiceOptions" : {"access":"online"},
  },
  "segDetails": {
    "fieldprops": {
      "widgettype": "Segment",
      "entity": "Messages",
      "additionalFields": ["title","sendDate","Description"],
      "field": {
        "lblDate": {
          "widgettype": "Label",
          "field": "sendDate"
        },
        "lblAlertDetails":{
          "widgettype": "Label",
          "field": "Description"
        }
      }
    }
  }
};