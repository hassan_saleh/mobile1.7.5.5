var tAndCversion = "0";

function getTermsandConditions() {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var langSelected = kony.store.getItem("langPrefObj");
  kony.print("BOJ langSelected  " + langSelected + " tncVersion ::" + tncVersion + " custid ::" + custid);
  var langSelected1 = langSelected.toUpperCase();
  var lan = "eng";
  if (langSelected1 == "EN") {
    lan = "eng";
  } else {
    lan = "ara";
  }

  var scopeObj = this;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {
    "access": "online",
    "objectName": "RBObjects"
  };
  var headers = {
    "session_token": kony.retailBanking.globalData.session_token
  };
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("Informationcontent", serviceName, options);
  var dataObject = new kony.sdk.dto.DataObject("Informationcontent");

  var serviceOptions = {
    "dataObject": dataObject,
    "headers": headers,
    "queryParams": {
      "ver": tncVersion,
      "lang": langSelected1,
      "language": lan,
      "custId": custid,
      "Loginchannel": channel,
      "userId" : user_id
    }
  };
  kony.print("Input params1-->" + JSON.stringify(serviceOptions));
  if (kony.sdk.isNetworkAvailable()) {
    modelObj.fetch(serviceOptions, tnCSuccess, tnCErrorCallBack);
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function tnCSuccess(response) {
  if (response !== undefined) {
    kony.print("tncSuccesss response--> " + JSON.stringify(response));
    gblTnCFlow = false;
    if (customerAccountDetails.profileDetails !== null) {
      customerAccountDetails.profileDetails = null;
    }
    if (response !== undefined && response[0].custData !== undefined && response[0].custData[0] !== null) {
      response[0].custData[0].maskedSMSno = mask_MobileNumber(response[0].custData[0].smsno);
      customerAccountDetails.profileDetails = response[0].custData[0];
      gblProfileCustmerName =	response[0].custData[0].title + response[0].custData[0].first_name;
    }
    kony.print("TnCFlag === " + TnCFlag + "profileDetails" + JSON.stringify(customerAccountDetails.profileDetails));
    if (TnCFlag == "F") {
      if ((response[0].statusType === "F" || response[0].status_code === "S0018")) {
        tAndCversion = response[0].version;
        var tAndCData = response[0].tncData;
        gblTnCDataObj = response[0].tncData;
        kony.print("Version Change: upper::" + tAndCversion);
        var htmlStrng = "<html><body bgcolor=\'#052c49\' text = \'#FFFFFF\' > </body></html>";
        frmTermsAndConditionsKA.richTexttermsandconditions.htmlString = isEmpty(response[0].tncData) ? htmlStrng : response[0].tncData;
        //frmTermsAndConditionsKA.richTexttermsandconditionsOld.text = response[0].tncData;
        frmTermsAndConditionsKA.FlexContainer0a0f9bf90d35346.isVisible =true;
        frmTermsAndConditionsKA.flxAccepteddate.isVisible =false;
        frmTermsAndConditionsKA.show();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      }else if(response !== undefined && (response[0].status_code === "S0017")){
        gblTnCDataObj = response[0].tncData;
        kony.print("Time to S0017");
        showWhich();
      }
      else {
        //should never come here
        kony.print("Time to leave earth");
        showWhich();
      }
    } else {
      if (response !== undefined && (response[0].statusType === "F" || response[0].status_code === "S0018")) {
        kony.print("Version Change:::" + tAndCversion);
        tAndCversion = response[0].version;
        kony.print("Version Change: niche::" + tAndCversion);
        var tAndCData = response[0].tncData;
        gblTnCDataObj = response[0].tncData;
        var htmlStrng =  "<html><body bgcolor='#052c49' text = '#FFFFFF' > </body></html>";
        frmTermsAndConditionsKA.richTexttermsandconditions.htmlString = isEmpty(response[0].tncData) ? htmlStrng : response[0].tncData;
        
        //frmTermsAndConditionsKA.richTexttermsandconditionsOld.text = response[0].tncData;
        frmTermsAndConditionsKA.FlexContainer0a0f9bf90d35346.isVisible =true;
        frmTermsAndConditionsKA.flxAccepteddate.isVisible =false;
        frmTermsAndConditionsKA.show();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

      } else if(response !== undefined && (response[0].status_code === "S0017")){
        gblTnCDataObj = response[0].tncData;
        kony.print("Time to S0017");
        showWhich();
      }
      else {
        showWhich();
      }
    }
  } else {
    showWhich();
  }
}

function tnCErrorCallBack(error) {
  kony.print("Inside tnCErrorCallBack:::" + JSON.stringify(error));
  var errmsg = "";
  if (error.code != null && error.code != "" && error.code == "10007") {

    showWhich();
  } else {
    if (!isEmpty(error.status_code)) {
      errmsg = getErrorMessage(error.status_code);
      if (error.status_code === "E0001") {
        errmsg = error.statusMessage;
      }
    } else if (!isEmpty(error.code))
      errmsg = getErrorMessage(error.code);

    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup(geti18Value("i18n.Bene.Failed"), errmsg, popupCommonAlertDimiss, "");
  }
  kony.print("Exit of tnCErrorCallBack:::");
}

function acceptTermsAndConditions() {
  kony.print("--- Inside acceptTermsAndConditions---");
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {
    "access": "online",
    "objectName": "RBObjects"
  };
  var headers = {};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
  var langSelected = kony.store.getItem("langPrefObj");
  var Language = langSelected.toUpperCase();
  var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
  dataObject.addField("custId", custid);
  dataObject.addField("Language", Language);
  dataObject.addField("userId", user_id);
  dataObject.addField("tncVersion", tAndCversion);
  var serviceOptions = {
    "dataObject": dataObject,
    "headers": headers
  };
  kony.print("Input params1-->" + JSON.stringify(serviceOptions));
  if (kony.sdk.isNetworkAvailable()) {
    modelObj.customVerb("AcceptTnC", serviceOptions, callAcceptTnCSuccess, callAcceptTnCError);
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
  kony.print("--- Exit of acceptTermsAndConditions---");
}

function callAcceptTnCSuccess(response) {
  kony.print("---Inside callAcceptTnCSuccess---" + JSON.stringify(response));
  kony.application.dismissLoadingScreen();
  showWhich();
}

function callAcceptTnCError(error) {
  kony.print("--- Inside callAcceptTnCError---" + JSON.stringify(error));
  kony.print("--- Exit of callAcceptTnCError----");
  showWhich();

}
function forceCPwd(){

  gblforceflag = true;
  gblFromModule = "ChangePassword";
  gblToOTPFrom = "ChangePassword";
  popupCommonAlertDimiss();
  ShowLoadingScreen();
  initialiseAndCallEnroll();
}

function forceCUname(){
  gblforceflag = true;
  gblFromModule = "ChangeUsername";
  gblToOTPFrom = "ChangeUsername";
  popupCommonAlertDimiss();	
  ShowLoadingScreen();
  initialiseAndCallEnroll();
}

function showWhich() {
  try{
    if(forceChangePasswordflag == "Y"){
      //enter into change password flow...
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup(kony.i18n.getLocalizedString("i18n.settings.myprofile.ChangePassword"), kony.i18n.getLocalizedString("i18n.common.forcechangepassword"), forceCPwd, logoutpp, kony.i18n.getLocalizedString("i18n.common.Change"), kony.i18n.getLocalizedString("i18n.common.cancelC"));
    }else if (forceChangeUserNameflag == "Y"){
      //enter into change username flow..
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup(kony.i18n.getLocalizedString("i18n.settings.myProfile.ChangeUsername"), kony.i18n.getLocalizedString("i18n.common.forcechangeusername"), forceCUname, logoutpp, kony.i18n.getLocalizedString("i18n.common.Change"), kony.i18n.getLocalizedString("i18n.common.cancelC"));
    }else{
      var appVersion1 = appConfig.appVersion.split(".");
	  var version = "";
      for(var i in appVersion1){
      	if(i == 0)
        	version = appVersion1[i]+".";
        else
        	version = version+""+appVersion1[i];
      }
      var  currappversion =parseFloat(version);
      if (releaseNoteFlag !== "" && releaseNoteFlag == currappversion) {
        loginFlowRN = false;
        devshwnot();
      } else {
        if (kony.store.getItem("langPrefObj") == "ar" && ar_ReleaseNotes != "") {
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
          frmReleaseNotesKA.show();
          startChatbotAfterLogin(custid , false);
        } else if (kony.store.getItem("langPrefObj") == "en" && en_ReleaseNotes != "") {
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
          frmReleaseNotesKA.show();
          startChatbotAfterLogin(custid , false);
        } else {
          loginFlowRN = false;
          devshwnot();
        }
      }
    }
  }catch(err){
    alert("Error in show which :: "+ err);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    devshwnot();

  }
}

function devshwnot(){
  if(isDeviceRegistered == "T") {//isDeviceRegistered == "T"
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmDeviceRegistrationOptionsKA.show();
    startChatbotAfterLogin(custid , false);
    //diffFlowFlag = "LOGINFLOW";
    //registerPush();
    //getAccountDataforQuickPreviewAccounts();
//     callRegisterDeviceBOJ();
  } else {
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
    frmAccountsLandingKA.flxDeals.setVisibility(false);
    frmAccountsLandingKA.flxLoans.setVisibility(false);
    frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
    frmAccountsLandingKA.flxDeals.zIndex = 1;
    frmAccountsLandingKA.flxLoans.zIndex = 1;
    frmAccountsLandingKA.forceLayout();
    if(kony.store.getItem("isDeviceRegisteredMandatory") === true){
    	//#ifdef android
    		registerPush();
    	//#endif
    	
    	//#ifdef iphone
    		gblUnsubscribe = {"ksid":ksid,
                "appId":konyRef.mainRef.appId,
                "deviceId":getCurrentDeviceId()
                 };
            unsubscribeCall();
    	//#endif
    }
    accountDashboardDataCall();
  }
}

function BOJGetTNC()
{
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJRestServices");
   kony.print("hassan tncVersion "+tncVersion);
   kony.print("hassan language  "+kony.store.getItem("langPrefObj").toUpperCase());
   kony.print("hassan custid"+custid);

  appMFConfiguration.invokeOperation("bojGetTnc", {},{"ver":tncVersion ,
                                                      "lang":kony.store.getItem("langPrefObj").toUpperCase(),
                                                      "custId": custid,
                                                      "Loginchannel":"MOBILE",
                                                      "userId":"BOJMOB"},
                                     successBOJGetTNC,
                                     function(error){});
}

function successBOJGetTNC(res)
{
  kony.print("hassan response"+JSON.stringify(res));
  gblTnCDataObj=res.tncData;
}

function showTnCFormSettings(){
  kony.print("showTnCFormSettings"+gblTnCDataObj);

  frmTermsAndConditionsKA.FlexContainer0a0f9bf90d35346.isVisible =false;
  frmTermsAndConditionsKA.flxAccepteddate.isVisible =true;

  var tncText =geti18Value("i18n.common.TncAccepted"); 
  var tDate=tncAcceDate.substring(0,10);
  var frmaDate=tDate.substring(8,10)+ "/"+ tDate.substring(5,7) + "/"+tDate.substring(0,4);
  frmTermsAndConditionsKA.lblAcceptedText.text=  tncText+ " " +  frmaDate;

 var htmlStrng = "<html><body bgcolor='#052c49' text = '#FFFFFF' > </body></html>";
  BOJGetTNC();
  if(!isEmpty(gblTnCDataObj)){
   
        frmTermsAndConditionsKA.richTexttermsandconditions.htmlString = isEmpty(gblTnCDataObj) ? htmlStrng : gblTnCDataObj;
        
    //frmTermsAndConditionsKA.richTexttermsandconditionsOld.text = gblTnCDataObj;
    frmTermsAndConditionsKA.show();
  }else{
    kony.print("No Data");
     frmTermsAndConditionsKA.richTexttermsandconditions.htmlString = htmlStrng;
        
    frmTermsAndConditionsKA.show();
  }
}

function show_DEVICEREGISTRATIONOPTIONCONTENT(){
  try{
    //#ifdef android
    frmDeviceRegistrationOptionsKA.CopyLabel0d00f815bcc7c41.text = geti18Value("i18n.DeviceReg.MssgAndroid");
    frmDeviceRegistrationOptionsKA.lblRegistrationProfileMessage.setVisibility(false);
    //#endif

    //#ifdef iphone
    frmDeviceRegistrationOptionsKA.CopyLabel0d00f815bcc7c41.text = geti18Value("i18n.DeviceReg.Mssg");
    frmDeviceRegistrationOptionsKA.lblRegistrationProfileMessage.setVisibility(true);
    //#endif
  }catch(e){
    kony.print("Exception_show_DEVICEREGISTRATIONOPTIONCONTENT ::"+e);
  }
}