/***************************
*
*
*
*****************************/
kony = kony || null;
kony.newCARD = kony.newCARD || null;
kony.newCARD = {"lastSelectionFlag":null,"cardTYPE":null,"applyCardsOption":false,"fields":null,"requiredFields":null,"ddlType":"","mainCardSelection":null,"maincardTYPE":null};
kony.newCARD.fields = {"SuplementryCardType":["lblSuplementryCardTypeTitle","lblSuplementryCardType","flxSuplementryCardType","suplementry_card_type",""],
                      "CreditCardNumber":["lblPrimaryCardNumberTitle","lblPrimaryCardNumber","flxPrimaryCardNumber","primary_creditCard_number",""],
					  "UpgradeCard":["lblUpgradeCardTitle","lblUpgradeCard","flxUpgradeCard","",""],
                      "AccountNumber":["lblAccountNumberTitle","lblAccountNumber","flxAccountNumber","p_acc_no",""],
                      "CardHolder":["lblCardHolderTitle","txtCardHolder","flxCardHolder","p_name_on_card",/[^A-za-z\s]/g],
                      "RelationShip":["lblRelationshipTypeTitle","lblRelationshipType","flxRelationshipType","p_relationship",""],
                      "Age":["lblAgeTitle","txtAge","flxAge","",""],
                      "Reason":["lblReasonTitle","lblReason","flxReason","p_reason",""],
					  "Color":["lblColorTitle","lblColor","flxColor","p_card_color",""],
                      "LimitType":["flxLimitTypeTitle","lblLimitType","flxLimitType","limit_type",""],
                      "SuplementryCardLimit":["lblSuplementryCardLimitTitle","txtSuplementryCardLimit","flxSuplementryCardLimit","suplementry_card_limit",/[^0-9]/g],
                      "EmploymentStatus":["lblEmploymentStatusTitle","lblEmploymentStatus","flxEmploymentStatus","p_employment_status",""],
                      "Profession":["lblProfessionTitle","txtProfession","flxProfession","p_profession",/[^A-Za-z\s]/g],
                      "TransferedSalary":["lblTransferedSalaryTitle","txtTransferedSalary","flxTransferedSalary","p_transferred_salary",/[^0-9]/g],
                      "MonthlyIncome":["lblMonthlyIncomeTitle","txtMonthlyIncome","flxMonthlyIncome","p_monthly_income",/[^0-9]/g],
                      "RequestLimit":["lblRequestLimitTitle","txtRequestLimit","flxRequestLimit","p_requested_limit",/[^0-9]/g],
                      "DeliveryMode":["btnDeliveryModeBranch","btnDeliverModeMailAddress","flxDeliveryMode","p_delv_mod",""],
                      "Branch":["lblBranchNameTitle","lblBranchName","flxBranchName","delivery_branch",""],
                      "Address":["lblAddressTitle","txtAddress","flxAddress","delivery_address",""],
                      "Notes":["lblNotesTitle","txtNotes","flxNotes","p_notes",""]};
kony.newCARD.requiredFields = {"SuplementryCreditCard":["SuplementryCardType","CreditCardNumber","CardHolder","Reason","RelationShip","Age","LimitType","RequestLimit","DeliveryMode","Branch","Notes"],
                               "PrimaryDebitCard":["AccountNumber","CardHolder","Reason","DeliveryMode","Branch","Notes"],
                               "SuplementryDebitCard":["CreditCardNumber","CardHolder","RelationShip","Reason","DeliveryMode","Branch","Notes"],
                               "WebchargeCard":["AccountNumber","CardHolder","LimitType","Reason","DeliveryMode","Branch","Notes"],
                               "PrimaryCreditCardReplace":["CreditCardNumber","CardHolder","Reason","DeliveryMode","Branch","Notes"],
							   "ReplaceDebitCards":["CreditCardNumber","CardHolder","Reason","DeliveryMode","Branch","Notes"],
                               "PrimaryCreditCard":["AccountNumber","CardHolder","Reason","EmploymentStatus","Profession","TransferedSalary","MonthlyIncome","RequestLimit","DeliveryMode","Branch","Notes"],
							   "UpgradeCreditCard":["CreditCardNumber","SuplementryCardType","RequestLimit","DeliveryMode","Branch","Notes"],
                               "Sticker":["CreditCardNumber","CardHolder","RelationShip","Reason","DeliveryMode","Branch","Notes"],
                               "Wearable":["AccountNumber","CreditCardNumber","CardHolder","Age","LimitType","Reason","Color","RequestLimit","DeliveryMode","Branch","Notes"],
                               "ReplacePrepaidCards":["CreditCardNumber","CardHolder","Reason","DeliveryMode","Branch","Notes"],
                          	   "ALL":["SuplementryCardType","CreditCardNumber","UpgradeCard","AccountNumber","CardHolder","RelationShip","Age","Reason","Color","LimitType","SuplementryCardLimit","EmploymentStatus","Profession","TransferedSalary","MonthlyIncome","RequestLimit","DeliveryMode","Branch","Address","Notes"]};
kony.newCARD.mainCardSelection = {"Credit":["flxPrimaryCreditCardReplace","flxSuplementryCreditCard","flxUpgradeCreditCard","flxWearables"],
                                  "Debit":["flxPrimaryDebitCard","flxSuplementryDebitCard","flxReplaceDebitCards","flxSticker"],
                                  "Prepaid":["flxWearables","flxWebChargeCard","flxReplacePrepaidCards"],
                                  "ALL":["PrimaryDebitCard","SuplementryDebitCard","Sticker","ReplaceDebitCards","PrimaryCreditCard","PrimaryCreditCardReplace","SuplementryCreditCard","UpgradeCreditCard","ChequeBook","Wearables","WebChargeCard","ReplacePrepaidCards"]};
/**********************************/
function navigate_TO_INSTANTCASHFORM(cardDATA){
	try{
   		/******Assigning Values to Widgets********/
    	frmInstantCash.lblTotalRedeemPoints.text = cardDATA.card_point+" "+geti18Value("i18n.instantcash.points");
    	frmInstantCash.lblAllowedRewardPoints.text = "";
    	frmInstantCash.lblCalculatedAmount.text = ((parseFloat(cardDATA.card_point)/400)*32)+" "+geti18Value("i18n.common.currencySymbol");
    	frmInstantCash.txtFieldAmount.text = "";
//     	frmInstantCash.lblRedeemMinPointsHint.text = geti18Value("i18n.instantcash.minpoints");
    	frmInstantCash.txtRedeemAmount.text = "";
//     	frmInstantCash.lblRedeemPointsValue.text = geti18Value("i18n.instantcash.400points");
    	frmInstantCash.btnNext.skin = "jomopaynextDisabled";
    	frmInstantCash.flxBorderAmount.skin = "skntextFieldDivider";
    	frmInstantCash.flxBorderAmountRedeemed.skin = "skntextFieldDivider";
    	/**************/
    	
    	/******UI Widgets Manage********/
    	frmInstantCash.flxInstantCashBody.setVisibility(true);
    	frmInstantCash.flxInstantCashPreConfirm.setVisibility(false);
    	if(parseFloat(cardDATA.card_point) < 400){
//         	frmInstantCash.lblminpntsnotreachedmsg.setVisibility(true);
        	frmInstantCash.txtFieldAmount.setEnabled(false);
        	frmInstantCash.btnNext.setVisibility(false);
        }else{
        	frmInstantCash.lblminpntsnotreachedmsg.setVisibility(false);
            frmInstantCash.txtFieldAmount.setEnabled(true);
            frmInstantCash.btnNext.setVisibility(true);
        }
    	/**************/
    	frmInstantCash.show();
    }catch(e){
    	kony.print("Exception_navigate_TO_INSTANTCASHFORM ::"+e);
    }
}

function onTextChange_REDEEMPOINTS_INSTANTCASH(fieldFLAG){
	try{
    	var redeemPTS = null;
    	if(fieldFLAG === "points"){
          frmInstantCash.txtFieldAmount.text = frmInstantCash.txtFieldAmount.text.replace(/\./g,"");
          if(frmInstantCash.txtFieldAmount.text !== ""){
            redeemPTS = frmInstantCash.txtFieldAmount.text;
            frmInstantCash.txtRedeemAmount.text = ((parseFloat(redeemPTS)/400)*32);
          }else{
          	frmInstantCash.txtRedeemAmount.text = "";
          }
        }
        if(fieldFLAG === "amount" && frmInstantCash.txtRedeemAmount.text !== ""){
          redeemPTS = frmInstantCash.txtRedeemAmount.text;
          frmInstantCash.txtFieldAmount.text = ""+Math.round(((parseInt(redeemPTS)/32)*400));
        }else if(fieldFLAG === "amount"){
          frmInstantCash.txtFieldAmount.text = "";
        }
    	redeemPTS = frmInstantCash.txtFieldAmount.text;
    	kony.print("redeem Points ::"+redeemPTS);
    	if(parseInt(redeemPTS) > 31250){
        	kony.print("inside IF");
        	frmInstantCash.lblExceedPointLimit.setVisibility(true);
        	frmInstantCash.flxBorderAmount.skin = "skntextFieldDividerOrange";
        	frmInstantCash.btnNext.skin = "jomopaynextDisabled";
        	return;
        }else{
        	kony.print("inside ELSE");
        	frmInstantCash.lblExceedPointLimit.setVisibility(false);
        }
    	if(parseFloat(redeemPTS) >= 400){
          frmInstantCash.btnNext.skin = "jomopaynextEnabled";
          frmInstantCash.flxBorderAmount.skin = "skntextFieldDividerGreen";
          frmInstantCash.flxBorderAmountRedeemed.skin = "skntextFieldDividerGreen";
        }else{
          frmInstantCash.btnNext.skin = "jomopaynextDisabled";
          frmInstantCash.flxBorderAmount.skin = "skntextFieldDividerOrange";
          frmInstantCash.flxBorderAmountRedeemed.skin = "skntextFieldDividerOrange";
        }
    }catch(e){
    	kony.print("Exception_onTextChange_REDEEMPOINTS_INSTANTCASH ::"+e);
    }
}

function onClick_NEXT_INSTANTCASHFORM(){
	try{
    	if(frmInstantCash.btnNext.skin === "jomopaynextEnabled"){
          var total_PTS = frmInstantCash.lblTotalRedeemPoints.text;
          total_PTS = total_PTS.replace(/\,/g,"");
          kony.print("total points ::"+total_PTS);
          if(parseFloat(frmInstantCash.txtFieldAmount.text) < 400){
          	customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.cards.minimumpointstoredeem"), popupCommonAlertDimiss, "");
          }else if(parseFloat(frmInstantCash.txtFieldAmount.text) > parseFloat(total_PTS)){
          	customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.instantcash.availablepointstoredeem")+" "+total_PTS, popupCommonAlertDimiss, "");
          }else if(parseFloat(frmInstantCash.txtFieldAmount.text) <= parseFloat(total_PTS)){
            frmInstantCash.lblNoOfRewardPointsAvailable.text = frmInstantCash.lblTotalRedeemPoints.text;
            frmInstantCash.lblPointsToBeRedeemed.text = frmInstantCash.txtFieldAmount.text;
            frmInstantCash.lblRedeemedAmountValue.text = frmInstantCash.txtRedeemAmount.text;
            frmInstantCash.btnNext.setVisibility(false);
            frmInstantCash.flxInstantCashBody.setVisibility(false);
            frmInstantCash.flxInstantCashPreConfirm.setVisibility(true);
          }
        }
    }catch(e){
    	kony.print("Exception_onClick_NEXT_INSTANTCASHFORM ::"+e);
    }
}

function onClick_BACK_INSTANTCASHFORM(){
	try{
    	if(frmInstantCash.flxInstantCashBody.isVisible === false){
        	frmInstantCash.btnNext.setVisibility(true);
            frmInstantCash.flxInstantCashBody.setVisibility(true);
            frmInstantCash.flxInstantCashPreConfirm.setVisibility(false);
        }else{
        	frmManageCardsKA.show();
        }
    }catch(e){
    	kony.print("Exception_onClick_BACK_INSTANTCASHFORM ::"+e);
    }
}

function navigate_TO_ECOMMERCELIMIT(){
	try{
      	/*****************RESET UI***********************/
    	var limit_DATA = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	var validation_VALUE = (parseFloat(limit_DATA.card_limit) > parseFloat(limit_DATA.spending_limit))?limit_DATA.card_limit:limit_DATA.spending_limit;
    	kony.print("validation_VALUE ::"+validation_VALUE);
    	frmEnableInternetTransactionKA.sliderECommerceLmt.min = 0;
    	frmEnableInternetTransactionKA.sliderECommerceLmt.max = parseInt(validation_VALUE);
    	frmEnableInternetTransactionKA.sliderMailOrderLimit.min = 0;
    	frmEnableInternetTransactionKA.sliderMailOrderLimit.max = parseInt(validation_VALUE);
    	frmEnableInternetTransactionKA.lblNext.skin = "sknLblNextDisabled";
    	frmEnableInternetTransactionKA.lblEcomLimit.text = geti18Value("i18n.ecommerce.maxlimitis")+" "+parseInt(validation_VALUE);
    	frmEnableInternetTransactionKA.lblMaillmit.text = geti18Value("i18n.ecommerce.maxlimitis")+" "+parseInt(validation_VALUE);
    	reset_ECOMMERECELIMIT();
    	/****************************************/
    	enable_disable_LIMIT_TOGGLE();
    	onSelect_PERIOD_ECOMMERCELIMIT("daily");
    	frmEnableInternetTransactionKA.show();
    }catch(e){
    	kony.print("Exception_navigate_TO_ECOMMERCELIMIT ::"+e);
    }
}

function navigate_TO_ATMPOSLIMITS(){
	try{
    	var limit_DATA = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	var validation_VALUE = (parseFloat(limit_DATA.card_limit) > parseFloat(limit_DATA.spending_limit))?limit_DATA.card_limit:limit_DATA.spending_limit;
    	frmATMPOSLimit.sliderATMLimit.min = 0;
    	frmATMPOSLimit.sliderATMLimit.max = parseInt(validation_VALUE);
    	frmATMPOSLimit.sliderPOSLimit.min = 0;
    	frmATMPOSLimit.sliderPOSLimit.max = parseInt(validation_VALUE);
    	frmATMPOSLimit.lblWithdrawalLimitHint.text = geti18Value("i18n.ecommerce.maxlimitis")+" "+validation_VALUE;
    	frmATMPOSLimit.lblPOSLimitHint.text = geti18Value("i18n.ecommerce.maxlimitis")+" "+validation_VALUE;
    	reset_ATMPOSLIMIT();
    	onSelect_PERIOD_ATMPOSLIMIT("Daily");
    	frmATMPOSLimit.show();
    }catch(e){
    	kony.print("Exception_navigate_TO_ATMPOSLIMITS ::"+e);
    }
}

function switch_ONOFF_ECOMMERCE(widget, flag){
	try{
    	var validated_LIMIT = validate_ECOMMERCE_LIMIT();
    	if(widget.isVisible === true){
        	widget.setVisibility(false);
        	if(flag === "eCommerceLimit"){
              frmEnableInternetTransactionKA.flxSwitchOnET.setVisibility(true);
              frmEnableInternetTransactionKA.flxECommerceLimit.setVisibility(true);
            }else if(flag === "mailOrderLimit"){
            	frmEnableInternetTransactionKA.flxSwitchOnMO.setVisibility(true);
        		frmEnableInternetTransactionKA.flxMailOederLimit.setVisibility(true);
            }
        }else{
        	widget.setVisibility(true);
        	if(flag === "eCommerceLimit"){
                frmEnableInternetTransactionKA.flxSwitchOnET.setVisibility(false);
                frmEnableInternetTransactionKA.flxECommerceLimit.setVisibility(true);
            }else if(flag === "mailOrderLimit"){
            	frmEnableInternetTransactionKA.flxSwitchOnMO.setVisibility(false);
        		frmEnableInternetTransactionKA.flxMailOederLimit.setVisibility(true);
            }
        }

		if((frmEnableInternetTransactionKA.flxSwitchOnET.isVisible && validated_LIMIT.internet) && 
           (frmEnableInternetTransactionKA.flxSwitchOnMO.isVisible && validated_LIMIT.mailorder)){
        	frmEnableInternetTransactionKA.lblNext.skin = "sknLblNextDisabled";
        }else{
        	frmEnableInternetTransactionKA.lblNext.skin = "sknLblNextEnabled";
        }
/******************
    	if(frmEnableInternetTransactionKA.flxSwitchOnET.isVisible === true || frmEnableInternetTransactionKA.flxSwitchOnMO.isVisible === true){
        	frmEnableInternetTransactionKA.lblToggleOnmsg.setVisibility(false);
        }else{
        	frmEnableInternetTransactionKA.lblToggleOnmsg.setVisibility(true);
        }
******************/
    }catch(e){
    	kony.print("Exception_switch_ONOFF_ECOMMERCELIMIT ::"+e);
    }
}

function reset_ECOMMERECELIMIT(){
	try{
    	frmEnableInternetTransactionKA.flxSwitchOffET.setVisibility(true);
    	frmEnableInternetTransactionKA.flxSwitchOnET.setVisibility(false);
    	frmEnableInternetTransactionKA.txtEcomLimit.text = "";
        frmEnableInternetTransactionKA.sliderECommerceLmt.selectedValue = 0;
        frmEnableInternetTransactionKA.flxECommerceLimit.setVisibility(true);
    	frmEnableInternetTransactionKA.flxSwitchOffMO.setVisibility(true);
        frmEnableInternetTransactionKA.flxSwitchOnMO.setVisibility(false);
        frmEnableInternetTransactionKA.txtMailLimit.text = "";
        frmEnableInternetTransactionKA.sliderMailOrderLimit.selectedValue = 0;
        frmEnableInternetTransactionKA.flxMailOederLimit.setVisibility(true);
    	frmEnableInternetTransactionKA.flxLimit.setVisibility(true);
    	frmEnableInternetTransactionKA.flxWithdrawalPOSlimit.setVisibility(false);
    	reset_ECOMERCELIMIT_SKIN();
    }catch(e){
    	kony.print("Exception_reset_ECOMMERECELIMIT ::"+e);
    }
}

function reset_ATMPOSLIMIT(){
	try{
    	frmATMPOSLimit.txtWithdrawalLimit.text = "";
    	frmATMPOSLimit.txtPOSLimit.text = "";
    	frmATMPOSLimit.btnDaily.skin = "sknBtnBGBlueWhite105Rd10";
    	frmATMPOSLimit.btnWeekly.skin = "sknBtnBGBlueWhite105Rd10";
    	frmATMPOSLimit.btnMonthly.skin = "sknBtnBGBlueWhite105Rd10";
    	frmATMPOSLimit.WithdrawalLimitBorder.skin = "skntextFieldDivider";
    	frmATMPOSLimit.POSLimitBorder.skin = "skntextFieldDivider";
    }catch(e){
    	kony.print("Exception_reset_ATMPOSLIMIT ::"+e);
    }
}

function reset_ECOMERCELIMIT_SKIN(){
	try{
    	frmEnableInternetTransactionKA.btnDaily.skin = "sknBtnBGBlueWhite105Rd10";
        frmEnableInternetTransactionKA.btnWeekly.skin = "sknBtnBGBlueWhite105Rd10";
        frmEnableInternetTransactionKA.btnMonthly.skin = "sknBtnBGBlueWhite105Rd10";
    	frmEnableInternetTransactionKA.borderBottom1.skin = "skntextFieldDividerGreen";
    	frmEnableInternetTransactionKA.borderBottom2.skin = "skntextFieldDividerGreen";
    }catch(e){
    	kony.print("Exception_reset_ECOMERCELIMIT_SKIN ::"+e);
    }
}

function enable_disable_LIMIT_TOGGLE(){
	try{
    	var validated_LIMIT = validate_ECOMMERCE_LIMIT();
        frmEnableInternetTransactionKA.flxSwitchOffET.setVisibility(!validated_LIMIT.internet);
        frmEnableInternetTransactionKA.flxSwitchOnET.setVisibility(validated_LIMIT.internet);
        frmEnableInternetTransactionKA.flxSwitchOffMO.setVisibility(!validated_LIMIT.mailorder);
        frmEnableInternetTransactionKA.flxSwitchOnMO.setVisibility(validated_LIMIT.mailorder);
    }catch(e){
    	kony.print("Exception_enable_disable_LIMIT_TOGGLE ::"+e);
    }
}

function onSelect_PERIOD_ECOMMERCELIMIT(periodType){
	try{
    	reset_ECOMERCELIMIT_SKIN();
    	var limit_DATA = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	var mailPeriodType = "";
        var internetPeriodType = "";
    	if(periodType === "daily"){
        	mailPeriodType = "daily_mail_limit";
            internetPeriodType = "daily_internet_limit";
        	frmEnableInternetTransactionKA.btnDaily.skin = "sknBtnBGWhiteBlue105Rd10";
        }else if(periodType === "weekly"){
        	mailPeriodType = "weekly_mail_limit";
			internetPeriodType = "weekly_internet_limit";
        	frmEnableInternetTransactionKA.btnWeekly.skin = "sknBtnBGWhiteBlue105Rd10";
        }else if(periodType === "Monthly"){
        	mailPeriodType = "monthly_mail_limit";
			internetPeriodType = "monthly_internet_limit";
        	frmEnableInternetTransactionKA.btnMonthly.skin = "sknBtnBGWhiteBlue105Rd10";
        }
    	var validation_VALUE = (parseFloat(limit_DATA.card_limit) > parseFloat(limit_DATA.spending_limit))?limit_DATA.card_limit:limit_DATA.spending_limit;
    	var limit = limit_DATA[internetPeriodType];
    	if(!isEmpty(limit)){
        	frmEnableInternetTransactionKA.txtEcomLimit.text = parseFloat(limit)>=1?limit:0+"";
        	frmEnableInternetTransactionKA.sliderECommerceLmt.selectedValue =  parseFloat(limit)>=1?parseInt(limit):0;
        }else{
        	frmEnableInternetTransactionKA.txtEcomLimit.text = validation_VALUE;
        	frmEnableInternetTransactionKA.sliderECommerceLmt.selectedValue = parseInt(validation_VALUE);
        }
      	if(frmEnableInternetTransactionKA.flxSwitchOnET.isVisible === true){
//         	frmEnableInternetTransactionKA.txtEcomLimit.text = limit_DATA[internetPeriodType]!==""?limit_DATA[internetPeriodType]:validation_VALUE;
//         	frmEnableInternetTransactionKA.sliderECommerceLmt.selectedValue = limit_DATA[internetPeriodType]!==""?parseInt(limit_DATA[internetPeriodType]):parseInt(validation_VALUE);
        	frmEnableInternetTransactionKA.flxECommerceLimit.setVisibility(true);
        	if(parseInt(limit_DATA[internetPeriodType]) > 0){
            	frmEnableInternetTransactionKA.borderBottom1.skin = "skntextFieldDividerGreen";
            }else{
            	frmEnableInternetTransactionKA.borderBottom1.skin = "skntextFieldDivider";
            }
        }
    	limit = limit_DATA[mailPeriodType];
    	if(!isEmpty(limit)){
        	frmEnableInternetTransactionKA.txtMailLimit.text = parseFloat(limit)>=1?limit:0+"";
       		frmEnableInternetTransactionKA.sliderMailOrderLimit.selectedValue = parseFloat(limit)>=1?parseInt(limit):0;
        }else{
        	frmEnableInternetTransactionKA.txtMailLimit.text = validation_VALUE;
       		frmEnableInternetTransactionKA.sliderMailOrderLimit.selectedValue = parseInt(validation_VALUE);
        }
		if(frmEnableInternetTransactionKA.flxSwitchOnMO.isVisible === true){
//         	frmEnableInternetTransactionKA.txtMailLimit.text = limit_DATA[mailPeriodType]!==""?limit_DATA[mailPeriodType]:validation_VALUE;
//         	frmEnableInternetTransactionKA.sliderMailOrderLimit.selectedValue = limit_DATA[mailPeriodType]!==""?parseInt(limit_DATA[mailPeriodType]):parseInt(validation_VALUE);
        	frmEnableInternetTransactionKA.flxMailOederLimit.setVisibility(true);
        	if(parseInt(limit_DATA[mailPeriodType]) > 0){
            	frmEnableInternetTransactionKA.borderBottom2.skin = "skntextFieldDividerGreen";
            }else{
            	frmEnableInternetTransactionKA.borderBottom2.skin = "skntextFieldDivider";
            }
        }
    }catch(e){
    	kony.print("Exception_onSelect_PERIOD_ECOMMERCELIMIT ::"+e);
    }
}

function onSelect_PERIOD_ATMPOSLIMIT(periodType){
	try{
    	reset_ATMPOSLIMIT();
    	var limit_DATA = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	var atmLimit = "", pocLimit = "";
    	if(periodType === "Daily"){
        	atmLimit = "daily_atm_limit";
        	pocLimit = "daily_pos_limit";
        	frmATMPOSLimit.btnDaily.skin = "sknBtnBGWhiteBlue105Rd10";
        }else if(periodType === "Weekly"){
        	atmLimit = "weekly_atm_limit";
        	pocLimit = "weekly_pos_limit";
        	frmATMPOSLimit.btnWeekly.skin = "sknBtnBGWhiteBlue105Rd10";
        }else if(periodType === "Monthly"){
        	atmLimit = "monthly_atm_limit";
        	pocLimit = "monthly_pos_limit";
        	frmATMPOSLimit.btnMonthly.skin = "sknBtnBGWhiteBlue105Rd10";
        }
    	
    	var validation_VALUE = (parseFloat(limit_DATA.card_limit) > parseFloat(limit_DATA.spending_limit))?limit_DATA.card_limit:limit_DATA.spending_limit;
    	if(!isEmpty(limit_DATA[atmLimit]) && parseFloat(limit_DATA[atmLimit]) > 0){
          frmATMPOSLimit.txtWithdrawalLimit.text = limit_DATA[atmLimit];
          frmATMPOSLimit.sliderATMLimit.selectedValue = parseInt(limit_DATA[atmLimit]);
        }else{
          frmATMPOSLimit.txtWithdrawalLimit.text = validation_VALUE;
          frmATMPOSLimit.sliderATMLimit.selectedValue = parseInt(validation_VALUE);
        }
      
      	if(!isEmpty(limit_DATA[pocLimit]) && parseFloat(limit_DATA[pocLimit]) > 0){
          frmATMPOSLimit.txtPOSLimit.text = limit_DATA[pocLimit];
          frmATMPOSLimit.sliderPOSLimit.selectedValue = parseInt(limit_DATA[pocLimit]);
        }else{
          frmATMPOSLimit.txtPOSLimit.text = validation_VALUE;
          frmATMPOSLimit.sliderPOSLimit.selectedValue = parseInt(validation_VALUE);
        }
    	/**frmATMPOSLimit.txtWithdrawalLimit.text = (limit_DATA[atmLimit] === "0")?limit_DATA[atmLimit]:kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_limit;
    	frmATMPOSLimit.sliderATMLimit.selectedValue = (limit_DATA[atmLimit] === "0")?parseInt(limit_DATA[atmLimit]):parseInt(kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_limit);
		frmATMPOSLimit.txtPOSLimit.text = (limit_DATA[pocLimit] === "0")?limit_DATA[pocLimit]:kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_limit;
		frmATMPOSLimit.sliderPOSLimit.selectedValue = (limit_DATA[pocLimit] === "0")?parseInt(limit_DATA[pocLimit]):parseInt(kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_limit);**/
    	if(parseInt(limit_DATA[atmLimit]) > 0){
        	frmATMPOSLimit.WithdrawalLimitBorder.skin = "skntextFieldDividerGreen";
        }
    	if(parseInt(limit_DATA[pocLimit]) > 0){
        	frmATMPOSLimit.POSLimitBorder.skin = "skntextFieldDividerGreen";
        }
    	
    }catch(e){
    	kony.print("Exception_onSelect_PERIOD_ATMPOSLIMIT ::"+e);
    }
}

function onTextChange_ECOMMERCELIMIT(widget, sliderREF, flag, flowflag){
	try{
    	var cardDATA = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	kony.print("cardDATA.card_limit ::"+cardDATA.card_limit+" :: cardDATA.spending_limit :: "+cardDATA.spending_limit);
    	var validation_VALUE = (parseFloat(cardDATA.card_limit) > parseFloat(cardDATA.spending_limit))?cardDATA.card_limit:cardDATA.spending_limit;
    	kony.print("validation_VALUE ::"+validation_VALUE);
    	if(widget.text !== "" && widget.text !== null && flag === "onTextChange"){
        	if(parseFloat(widget.text) <= parseFloat(validation_VALUE)){
       			sliderREF.selectedValue = parseInt(widget.text);
            	if(flowflag === "internetlimit"){
                	frmEnableInternetTransactionKA.borderBottom1.skin = "skntextFieldDividerGreen";
                }else if(flowflag === "mailorderlimit"){
                	frmEnableInternetTransactionKA.borderBottom2.skin = "skntextFieldDividerGreen";
                }
            }else{
              if(flowflag === "internetlimit"){
                frmEnableInternetTransactionKA.borderBottom1.skin = "skntextFieldDividerOrange";
              }else if(flowflag === "mailorderlimit"){
                frmEnableInternetTransactionKA.borderBottom2.skin = "skntextFieldDividerOrange";
              }
            }
        }else if(flag === "onSlide"){
        	kony.print("selected Value ::"+sliderREF.selectedValue);
        	if(flowflag === "internetlimit")
        		frmEnableInternetTransactionKA.txtEcomLimit.text = parseInt(sliderREF.selectedValue).toString();
        	else if(flowflag === "mailorderlimit")
            	frmEnableInternetTransactionKA.txtMailLimit.text = parseInt(sliderREF.selectedValue).toString();
        }
      
    	if(flowflag === "internetlimit"){
          animateLabel("UP","lblEcom",frmEnableInternetTransactionKA.txtEcomLimit.text);
          animateLabel("DOWN","lblMailOrder",frmEnableInternetTransactionKA.txtMailLimit.text);
        }else if(flowflag === "mailorderlimit"){
          animateLabel("DOWN","lblEcom",frmEnableInternetTransactionKA.txtEcomLimit.text);
          animateLabel("UP","lblMailOrder",frmEnableInternetTransactionKA.txtMailLimit.text);
        }
      
//     	validate_ECOMMERCELIMIT(widget.text, flowflag);
    }catch(e){
    	kony.print("Exception_onTextChange_ECOMMERCELIMIT ::"+e);
    }
}

function onTextChange_ATMPOSLIMIT(widget, sliderRef, flag, flowflag){
	try{
    	var cardDATA = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	var validation_VALUE = (parseFloat(cardDATA.card_limit) > parseFloat(cardDATA.spending_limit))?cardDATA.card_limit:cardDATA.spending_limit;
    	if(flag === "onSlide"){
        	widget.text = parseInt(sliderRef.selectedValue).toString();
        }else if(widget.text !== "" && widget.text !== null && flag === "onTextChange"){
          sliderRef.selectedValue = parseInt(widget.text);
          if(parseFloat(widget.text) <= parseFloat(validation_VALUE)){
            if(flowflag === "ATM"){
            	frmATMPOSLimit.WithdrawalLimitBorder.skin = "skntextFieldDividerGreen";
            }else if(flowflag === "POS"){
            	frmATMPOSLimit.POSLimitBorder.skin = "skntextFieldDividerGreen";
            }
          }else{
            if(flowflag === "ATM"){
            	frmATMPOSLimit.WithdrawalLimitBorder.skin = "skntextFieldDividerOrange";
            }else if(flowflag === "POS"){
            	frmATMPOSLimit.POSLimitBorder.skin = "skntextFieldDividerOrange";
            }
          }
        }
    }catch(e){
    	kony.print("Exception_onTextChange_ATMPOSLIMIT ::"+e);
    }
}

function validate_ECOMMERCELIMIT(cardDATA){//value, flowflag){
	try{
    	kony.print("card data ::"+JSON.stringify(cardDATA));
    	var validation_VALUE = (parseFloat(cardDATA.card_limit) > parseFloat(cardDATA.spending_limit))?cardDATA.card_limit:cardDATA.spending_limit;
      	if((parseFloat(cardDATA.validation_VALUE) < parseFloat(frmEnableInternetTransactionKA.txtEcomLimit.text)) || 
           (parseFloat(cardDATA.validation_VALUE) < parseFloat(frmEnableInternetTransactionKA.txtMailLimit.text))){
        	return false;
        }else{
        	return true;
        }
        
    }catch(e){
    	kony.print("Exception_validate_ECOMMERCELIMIT ::"+e);
    }
}

function validate_ECOMMERCE_LIMIT(){
	try{
    	var returnVALUE = {"internet":false,"mailorder":false};
    	var limit_DATA = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	if((limit_DATA.daily_internet_limit !== null && parseFloat(limit_DATA.daily_internet_limit) > 0.0) ||
        	(limit_DATA.weekly_internet_limit !== null && parseFloat(limit_DATA.weekly_internet_limit) > 0.0) ||
        	(limit_DATA.monthly_internet_limit !== null && parseFloat(limit_DATA.monthly_internet_limit) > 0.0)){
        	returnVALUE.internet = true;
        }
    	
    	if((limit_DATA.daily_mail_limit !== null && parseFloat(limit_DATA.daily_mail_limit) > 0.0) ||
        	(limit_DATA.weekly_mail_limit !== null && parseFloat(limit_DATA.weekly_mail_limit) > 0.0) ||
        	(limit_DATA.monthly_mail_limit !== null && parseFloat(limit_DATA.monthly_mail_limit) > 0.0)){
        	returnVALUE.mailorder = true;
        }
    	return returnVALUE;
    }catch(e){
    	kony.print("Exception_validate_ECOMMERCE_LIMIT ::"+e);
    }
}

function onClick_APPLYNEWCARDSOPTIONS(selectedOption){
	try{
    	var field = "",flex = "";
    	for(var i in kony.newCARD.requiredFields[selectedOption]){
        	field = kony.newCARD.requiredFields[selectedOption][i];
        	flex = kony.newCARD.fields[field][2];
        	kony.print("field ::"+field+" :: flex :: "+flex);
        	frmApplyNewCardsKA[flex].setVisibility(true);
        }
    	customize_VIEW_APPLYNEWCARDSOPTION(selectedOption);
    	frmApplyNewCardsKA.flxApplynewCardsFirst.setVisibility(false);
    	frmApplyNewCardsKA.flxApplyNewCardsSecond.setVisibility(true);
    	//animate_NEWCARDSOPTIOS("DOWN",kony.newCARD.fields[field][0],frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text,frmApplyNewCardsKA);
    }catch(e){
    	kony.print("Exception_onClick_APPLYNEWCARDSOPTIONS ::"+e);
    }
}

function customize_VIEW_APPLYNEWCARDSOPTION(option){
	try{
      	if(kony.newCARD.maincardTYPE === "Credit"){
        	frmApplyNewCardsKA.flxDeliverModeMailAddress.setVisibility(true);
        	if(option === "Wearable"){
            	frmApplyNewCardsKA.lblAgeTitle.text = geti18Value("i18n.cards.agecredit");
        		frmApplyNewCardsKA.flxAccountNumber.setVisibility(false);
            	frmApplyNewCardsKA.flxAddress.setVisibility(false);
        		frmApplyNewCardsKA.flxDeliverModeMailAddress.setVisibility(true);
        	}
        }else{
        	frmApplyNewCardsKA.flxAddress.setVisibility(false);
        	frmApplyNewCardsKA.flxDeliverModeMailAddress.setVisibility(false);
        }
      	frmApplyNewCardsKA.lblAccountNumberTitle.text = geti18Value("i18n.common.accountNumber");
      	//
    	if(kony.newCARD.maincardTYPE === "Prepaid"){
        	frmApplyNewCardsKA.lblAccountNumberTitle.text = geti18Value("i18n.applycards.comissionaccount");
        	frmApplyNewCardsKA.flxLimitType.setVisibility(false);
        	if(option === "Wearable"){
            frmApplyNewCardsKA.flxPrimaryCardNumber.setVisibility(false);
        	frmApplyNewCardsKA.flxRequestLimit.setVisibility(false);
        	frmApplyNewCardsKA.txtRequestLimit.text = "0";
        	frmApplyNewCardsKA.lblAgeTitle.text = geti18Value("i18n.cards.ageprepaid");
            }
        }
    	frmApplyNewCardsKA.flxReason.setVisibility(false);
		//
		if(option === "PrimaryCreditCardReplace" || option === "ReplaceDebitCards" || option === "ReplacePrepaidCards"){
        	frmApplyNewCardsKA.flxReason.setVisibility(true);
        	frmApplyNewCardsKA.btnReason.setEnabled(true);
        	if(option === "PrimaryCreditCardReplace"){
            	frmApplyNewCardsKA.flxDeliverModeMailAddress.setVisibility(true);
            }
		}else if(option === "UpgradeCreditCard"){
        	frmApplyNewCardsKA.flxRequestLimit.setVisibility(true);
            frmApplyNewCardsKA.flxSuplementryCardType.setVisibility(false);
        	frmApplyNewCardsKA.lblSuplementryCardTypeTitle.text = geti18Value("i18n.cards.cardtype");
        	set_SERVCARDFLAG(geti18Value("i18n.cards.newcard"),"UpgradeCreditCard");
        }else{
        	if(option === "SuplementryCreditCard" || (option === "Wearable" && kony.newCARD.maincardTYPE === "Credit")){
            	selected_OPTIONS_APPLYNEWCARDS([{"lblJP":geti18Value("i18n.cards.jointlimit")}],"LimitType");
            	frmApplyNewCardsKA.flxLimitType.setVisibility(false);
            	if(option === "SuplementryCreditCard")
            		frmApplyNewCardsKA.lblPrimaryCardNumberTitle.text = geti18Value("i18n.applycards.primarycardnumber");
            }else{
            	frmApplyNewCardsKA.lblLimitType.text = "";
            	frmApplyNewCardsKA.lblPrimaryCardNumberTitle.text = geti18Value("i18n.newCard.primarycreditcardnumber");
            }
			selected_OPTIONS_APPLYNEWCARDS([{"lblJP":geti18Value("i18n.cards.newcard")}],"Reason");
        	frmApplyNewCardsKA.btnReason.setEnabled(false);
		}
    }catch(e){
    	kony.print("Exception_customize_VIEW_APPLYNEWCARDSOPTION ::"+e);
    }
}

function switch_CARDSLANDING_APPLYCARDS(flag){
    try{
        if(flag === "flxApplyNewCards" && frmCardsLandingKA[flag].isVisible === false){
            frmCardsLandingKA.btnAllCards.skin = "btnLoans";
            frmCardsLandingKA.btnApplyCards.skin = "sknTransparentBGRNDFontBOJ";
        	frmCardsLandingKA.lblBack.text = geti18Value("i18n.billsPay.Cards");
        	frmCardsLandingKA.btnApplyCards.setVisibility(false);
        	frmCardsLandingKA.btnAllCards.setVisibility(false);
            frmCardsLandingKA.flxScrollMain.setVisibility(false);
			frmCardsLandingKA.flxapplyDebit.setVisibility(true);
            frmCardsLandingKA.flxapplyCredit.setVisibility(true);
            if(!kony.retailBanking.globalData.isDebitCardAvailable){
                frmCardsLandingKA.flxapplyDebit.setVisibility(false);
            }
            if(!kony.retailBanking.globalData.isCreditCardAvailable){
                frmCardsLandingKA.flxapplyCredit.setVisibility(false);
            }
            frmCardsLandingKA.flxApplyNewCards.setVisibility(true);
        }else if(flag === "flxScrollMain" && frmCardsLandingKA[flag].isVisible === false){
            frmCardsLandingKA.btnAllCards.skin = "btnAccounts";
            frmCardsLandingKA.btnApplyCards.skin = "sknTransparentBGRNDFontBOJ";
        	frmCardsLandingKA.lblBack.text = geti18Value("i18n.accounts.accounts");
        	frmCardsLandingKA.btnApplyCards.setVisibility(true);
        	frmCardsLandingKA.flxApplyNewCards.setVisibility(false);
        	frmCardsLandingKA.btnAllCards.setVisibility(true);
            frmCardsLandingKA.flxScrollMain.setVisibility(true);
        }
    }catch(e){
        kony.print("Exception_animate_CARDSLANDING_APPLYCARDS ::"+e);
    }
}

function reset_APPLYNEWCARDS(){
	try{
    	frmApplyNewCardsKA.lblApplyCardTitle.text = geti18Value("i18n.cards.applynewcard");
    	frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
    	frmApplyNewCardsKA.flxApplynewCardsFirst.setVisibility(true);
    	frmApplyNewCardsKA.flxApplyNewCardsSecond.setVisibility(false);
    	kony.newCARD.ddlType = "";
    	reset_APPLYNEWCARDS1STPAGE();
    	reset_APPLYNEWCARDS2NDPAGE();
    	frmApplyNewCardsKA.show();
    }catch(e){
    	kony.print("Exception_reset_APPLYNEWCARDS ::"+e);
    }
}

function reset_APPLYNEWCARDS1STPAGE(){
	try{
    	kony.newCARD.applyCardsOption = false;
    	for(var i in kony.newCARD.mainCardSelection.ALL){
        	frmApplyNewCardsKA["btn"+kony.newCARD.mainCardSelection.ALL[i]].text = "s";
        	frmApplyNewCardsKA["btn"+kony.newCARD.mainCardSelection.ALL[i]].skin = "CopyslButtonGlossBlue0cec99ae0f6784a";
        	frmApplyNewCardsKA["lbl"+kony.newCARD.mainCardSelection.ALL[i]+"Title"].skin = "sknWhite50OPC150SIZE";
        	frmApplyNewCardsKA["flx"+kony.newCARD.mainCardSelection.ALL[i]].setVisibility(false);
        }
    	//
    	for(var i in kony.newCARD.mainCardSelection[kony.newCARD.maincardTYPE]){
        	frmApplyNewCardsKA[kony.newCARD.mainCardSelection[kony.newCARD.maincardTYPE][i]].setVisibility(true);
        }
    	
    	if(kony.newCARD.maincardTYPE === "Credit"){
          frmApplyNewCardsKA.flxSuplementryCreditCard.setVisibility(kony.retailBanking.globalData.isCreditCardAvailable);
          frmApplyNewCardsKA.flxPrimaryCreditCardReplace.setVisibility(kony.retailBanking.globalData.isCreditCardAvailable);
          frmApplyNewCardsKA.flxWearables.setVisibility(kony.retailBanking.globalData.isCreditCardAvailable);
          frmApplyNewCardsKA.lblWearablesTitle.text = geti18Value("i18n.cards.Wearable");
          frmApplyNewCardsKA.lblWebChargeCardTitle.text = geti18Value("i18n.cards.WebchargeCard");
          frmApplyNewCardsKA.flxUpgradeCreditCard.setVisibility(kony.retailBanking.globalData.isCreditCardAvailable);
        }
    	if(kony.newCARD.maincardTYPE === "Debit"){
    		frmApplyNewCardsKA.flxSuplementryDebitCard.setVisibility(kony.retailBanking.globalData.isDebitCardAvailable);
 			frmApplyNewCardsKA.flxPrimaryDebitCard.setVisibility(!kony.retailBanking.globalData.isDebitCardAvailable);
//         	frmApplyNewCardsKA.flxSticker.setVisibility(kony.retailBanking.globalData.isDebitCardAvailable);
        	frmApplyNewCardsKA.flxReplaceDebitCards.setVisibility(kony.retailBanking.globalData.isDebitCardAvailable);
        }
    	if(kony.newCARD.maincardTYPE === "Prepaid"){
        	frmApplyNewCardsKA.lblWebChargeCardTitle.text = geti18Value("i18n.cards.prepaidcard");
        	frmApplyNewCardsKA.lblWearablesTitle.text = geti18Value("i18n.cards.WearableZeroLimit");
        	frmApplyNewCardsKA.flxReplacePrepaidCards.setVisibility(kony.retailBanking.globalData.isWebchargeCardAvailable);
        }
    }catch(e){
    	kony.print("Exception_reset_APPLYNEWCARDS1STPAGE ::"+e);
    }
}
function reset_APPLYNEWCARDS2NDPAGE(){
	try{
    	/***************RESETTING UI ELEMENTS***************/
    	var field = "";
        frmApplyNewCardsKA.lblSuplementryCardTypeTitle.text = geti18Value("i18n.newCard.supplementrycardtype");
    	frmApplyNewCardsKA.lblAgeTitle.text = geti18Value("i18n.common.age");
    	frmApplyNewCardsKA.lblMobileNumberHint.setVisibility(false);
   		for(var i in kony.newCARD.requiredFields.ALL){
        	field = kony.newCARD.requiredFields.ALL[i];
        	kony.print("filed ::"+field);
        	if(field !== "DeliveryMode"){
            	frmApplyNewCardsKA["flxBorder"+field].skin = "skntextFieldDivider";
            	frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text = "";
            	animate_NEWCARDSOPTIOS("DOWN",kony.newCARD.fields[field][0],frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text,frmApplyNewCardsKA);
            	frmApplyNewCardsKA[kony.newCARD.fields[field][0]].width = "80%";
            	frmApplyNewCardsKA[kony.newCARD.fields[field][0]].height = "40%";
            	frmApplyNewCardsKA[kony.newCARD.fields[field][0]].skin = "sknLblNextDisabled";
        		frmApplyNewCardsKA[kony.newCARD.fields[field][0]].top = "35%";
            }else{
            	frmApplyNewCardsKA[kony.newCARD.fields[field][0]].text = "t";
                frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text = "s";
            }
        	if(kony.newCARD.fields[field][2] !== "")
        		frmApplyNewCardsKA[kony.newCARD.fields[field][2]].setVisibility(false);
        }
    	frmApplyNewCardsKA.lblRequestLimitHint.setVisibility(false);
    	frmApplyNewCardsKA.lblReasonHint.setVisibility(false);
    	frmApplyNewCardsKA.lblReasonHint.text = "";
    	/******************************/
    }catch(e){
    	kony.print("Exception_reset_APPLYNEWCARDS2NDPAGE ::"+e);
    }
}

function onClick_BACK_APPLYNEWCARDS(){
	try{
    	if(frmApplyNewCardsKA.flxApplyNewCardsSecond.isVisible === true){
        	frmApplyNewCardsKA.flxApplyNewCardsSecond.setVisibility(false);
        	frmApplyNewCardsKA.flxApplynewCardsFirst.setVisibility(true);
        	frmApplyNewCardsKA.btnNext.skin = "jomopaynextEnabled";
        	frmApplyNewCardsKA.lblApplyCardTitle.text = geti18Value("i18n.cards.applynewcard");
        	reset_APPLYNEWCARDS2NDPAGE();
        }else if(frmApplyNewCardsKA.flxApplynewCardsFirst.isVisible === true){
        	kony.newCARD.maincardTYPE = "";
        	kony.newCARD.applyCardsOption = false;
        	frmCardsLandingKA.show();
        }
    }catch(e){
    	kony.print("Exception_onClick_BACK_APPLYNEWCARDS ::"+e);
    }
}

function onClick_RADIOBUTTON_APPLYNEWCARDSTYPE(widget, label, type){
	try{
    	//reset_APPLYNEWCARDS1STPAGE();
    	for(var i in kony.newCARD.mainCardSelection.ALL){
          frmApplyNewCardsKA["btn"+kony.newCARD.mainCardSelection.ALL[i]].text = "s";
          frmApplyNewCardsKA["btn"+kony.newCARD.mainCardSelection.ALL[i]].skin = "CopyslButtonGlossBlue0cec99ae0f6784a";
          frmApplyNewCardsKA["lbl"+kony.newCARD.mainCardSelection.ALL[i]+"Title"].skin = "sknWhite50OPC150SIZE";
        }
    	widget.text = "t";
    	widget.skin = "sknRDOWhiteBRDBOJFont";
    	label.skin = "sknLblWhike150";
    	frmApplyNewCardsKA.btnNext.skin = "jomopaynextEnabled";
    	kony.newCARD.cardTYPE = type;
    }catch(e){
    	kony.print("Exception_onClick_RADIOBUTTON_APPLYNEWCARDSTYPE ::"+e);
    }
}

function onClick_NEXT_APPLYNEWCARDS(){
	try{
    	if(frmApplyNewCardsKA.btnNext.skin === "jomopaynextEnabled"){
          if(frmApplyNewCardsKA.flxApplynewCardsFirst.isVisible === true){
            kony.print(kony.newCARD.cardTYPE);
          	if(kony.newCARD.cardTYPE === "ChequeBook"){
            	frmCardsChequeBook.show();
            }else{
              frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
          	  if(kony.newCARD.cardTYPE === "Wearable" && kony.newCARD.maincardTYPE === "Prepaid"){
              	frmApplyNewCardsKA.lblApplyCardTitle.text = geti18Value("i18n.cards.WearableZeroLimit");
              }else{
          	  	frmApplyNewCardsKA.lblApplyCardTitle.text = geti18Value("i18n.cards."+kony.newCARD.cardTYPE);
              }

          	  if(kony.newCARD.cardTYPE === "WebchargeCard" && kony.newCARD.maincardTYPE === "Prepaid"){
                frmApplyNewCardsKA.lblApplyCardTitle.text = geti18Value("i18n.cards.prepaidcard");
              }
          	  kony.newCARD.ddlType = "";
              onClick_APPLYNEWCARDSOPTIONS(kony.newCARD.cardTYPE);
            }
          }else if(frmApplyNewCardsKA.flxApplyNewCardsSecond.isVisible === true){
//               serv_APPLYCARDS();
          	  navigate_APPLYCARDS_CONFIRMATION(kony.newCARD.cardTYPE);
          }
        }
    }catch(e){
    	kony.print("Exception_onClick_NEXT_APPLYNEWCARDS ::"+e);
    }
}

function onChoose_OPTIONS_APPLYNEWCARDS(options){
	try{
    	var inputtype = null;
    	kony.newCARD.applyCardsOption = true;
    	if(options === "SuplementryCardType"){
        	if(frmApplyNewCardsKA.lblPrimaryCardNumber.text !== ""){
              inputtype = set_SUPPLEMENTRYCARDTYPE(kony.store.getItem("frmAccount").card_type, kony.store.getItem("frmAccount").card_type_desc);
              kony.boj.JomoPaytransferType(inputtype,options);
            }else{
            	frmApplyNewCardsKA.flxBorderCreditCardNumber.skin = "sknFlxOrangeLine";
            }
        }else if(options === "CreditCardNumber"){
        	frmCreditCardNumber.show();
        }else if(options === "AccountNumber"){
        	getAllAccounts();
        }else if(options === "UpgradeCard"){
        	inputtype = [geti18Value("i18n.applycards.cardtype"),geti18Value("i18n.applycards.cardlimit")];
        	kony.boj.JomoPaytransferType(inputtype,options);
        }else if(options === "RelationShip"){
        	inputtype = [geti18Value("i18n.relations.father"),geti18Value("i18n.relations.mother"),geti18Value("i18n.relations.husband"),geti18Value("i18n.relations.wife"),geti18Value("i18n.relations.brother"),geti18Value("i18n.relations.sister"),geti18Value("i18n.relations.son"),geti18Value("i18n.relations.daughter"),geti18Value("i18n.relations.self")];
        	kony.boj.JomoPaytransferType(inputtype,options);
        }else if(options === "Reason"){
        	if(kony.newCARD.cardTYPE === "PrimaryCreditCardReplace" || kony.newCARD.cardTYPE === "ReplaceDebitCards" || kony.newCARD.cardTYPE === "ReplacePrepaidCards"){
            	inputtype = [geti18Value("i18n.card.lost"),geti18Value("i18n.card.damaged"),geti18Value("i18n.applycards.changename")];// Mai 10/3/2021 remove Others in replacement  ,geti18Value("i18n.FilterTransaction.Others")];
            }
          else{
        		inputtype = [geti18Value("i18n.cards.newcard"),geti18Value("i18n.cards.ReplaceCard")];
            }
        	kony.boj.JomoPaytransferType(inputtype,options);
        }else if(options === "Color"){
        	inputtype = [geti18Value("i18n.color.pink"),geti18Value("i18n.color.black"),geti18Value("i18n.color.purple"),geti18Value("i18n.color.blue")];
        	kony.boj.JomoPaytransferType(inputtype,options);
        }else if(options === "LimitType"){
  			inputtype = [geti18Value("i18n.cards.jointlimit"),geti18Value("i18n.cards.seperatedlimit")];
        	kony.boj.JomoPaytransferType(inputtype,options);
        }else if(options === "DeliveryMode"){
        	frmApplyNewCardsKA.lblBranchName.text = "";
        	frmApplyNewCardsKA.txtAddress.text = "";
        	if(frmApplyNewCardsKA.btnDeliveryModeBranch.text === "t"){
            	frmApplyNewCardsKA.flxAddress.setVisibility(true);
            	frmApplyNewCardsKA.flxBranchName.setVisibility(false);
            	frmApplyNewCardsKA.btnDeliverModeMailAddress.text = "t";
            	frmApplyNewCardsKA.btnDeliveryModeBranch.text = "s";
            }else if(frmApplyNewCardsKA.btnDeliverModeMailAddress.text === "t"){
            	frmApplyNewCardsKA.flxBranchName.setVisibility(true);
            	frmApplyNewCardsKA.flxAddress.setVisibility(false);
            	frmApplyNewCardsKA.btnDeliverModeMailAddress.text = "s";
            	frmApplyNewCardsKA.btnDeliveryModeBranch.text = "t";
            }
        	frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
        }else if(options === "Branch"){
        	kony.boj.showSelectedDetailsFromBene("Branch", "lblBranchName");
        }else if(options === "EmploymentStatus"){
        	inputtype = [geti18Value("i18n.newcards.business"),geti18Value("i18n.newcards.employed"),geti18Value("i18n.newcards.unemployed"),geti18Value("i18n.newcards.others")];
        	kony.boj.JomoPaytransferType(inputtype,options);
        }
    }catch(e){
    	kony.newCARD.applyCardsOption = false;
    	kony.print("Exception_onSelect_OPTIONS_APPLYNEWCARDS ::"+e);
    }
}

function selected_OPTIONS_APPLYNEWCARDS(selectedItems, type){
	try{
    	kony.newCARD.applyCardsOption = false;
    	kony.newCARD.ddlType = type;
    	/**********************
    	animate_NEWCARDSOPTIOS("DOWN",kony.newCARD.fields.CardHolder[0],frmApplyNewCardsKA[kony.newCARD.fields.CardHolder[1]].text,frmApplyNewCardsKA);
    	animate_NEWCARDSOPTIOS("DOWN",kony.newCARD.fields.SuplementryCardLimit[0],frmApplyNewCardsKA[kony.newCARD.fields.SuplementryCardLimit[1]].text,frmApplyNewCardsKA);
    	animate_NEWCARDSOPTIOS("DOWN",kony.newCARD.fields.Address[0],frmApplyNewCardsKA[kony.newCARD.fields.Address[1]].text,frmApplyNewCardsKA);
    	animate_NEWCARDSOPTIOS("DOWN",kony.newCARD.fields.Notes[0],frmApplyNewCardsKA[kony.newCARD.fields.Notes[1]].text,frmApplyNewCardsKA);
    	**********************/
    	if(type === "Branch"){
        	kony.print("Selected Items ::"+JSON.stringify(selectedItems));
        	frmApplyNewCardsKA[kony.newCARD.fields[type][1]].text = selectedItems.BRANCH_NAME;
        }else{
        	if(type === "AccountNumber"){
              var storeData = isEmpty(selectedItems) ? "" : selectedItems;
            	kony.store.setItem("frmAccount",storeData);
            }else if(type === "CreditCardNumber"){
            	if(kony.newCARD.cardTYPE === "SuplementryCreditCard" || kony.newCARD.cardTYPE === "PrimaryCreditCard" || kony.newCARD.cardTYPE === "UpgradeCreditCard" || kony.newCARD.cardTYPE === "Wearable"){
                  frmApplyNewCardsKA.lblRequestLimitHint.text = geti18Value("i18n.ecommerce.maxlimitis")+kony.store.getItem("frmAccount").card_limit;
                  frmApplyNewCardsKA.lblRequestLimitHint.setVisibility(true);
                }else if(kony.newCARD.cardTYPE === "PrimaryCreditCardReplace" || kony.newCARD.cardTYPE === "ReplaceDebitCards" || kony.newCARD.cardTYPE === "ReplacePrepaidCards"){
                	frmApplyNewCardsKA.txtCardHolder.text = kony.store.getItem("frmAccount").name_on_card;
                	frmApplyNewCardsKA.lblCardHolderTitle.skin = "sknlblanimated75";
                    frmApplyNewCardsKA.lblCardHolderTitle.top = "12%";
                    frmApplyNewCardsKA.flxBorderCardHolder.skin = "skntextFieldDividerGreen";
                	frmApplyNewCardsKA.lblReasonHint.setVisibility(false);
                	if(kony.newCARD.cardTYPE === "PrimaryCreditCardReplace"){
                    	if(kony.store.getItem("frmAccount").card_type_desc === "Classic" || (kony.store.getItem("frmAccount").card_name.indexOf("Classic") > -1))
                            frmApplyNewCardsKA.lblReasonHint.text = "*"+geti18Value("i18n.newCard.replaceCardFee"); 
                        else
                            frmApplyNewCardsKA.lblReasonHint.text = "*"+geti18Value("i18n.newCard.replaceCCFee"); 
                    	frmApplyNewCardsKA.lblReasonHint.setVisibility(true);
                    }
                }
            }
            frmApplyNewCardsKA[kony.newCARD.fields[type][1]].text = selectedItems[0].lblJP;
        }
    	
    	if(type === "UpgradeCard"){
        	if(selectedItems[0].lblJP === "Card Limit"){
            	frmApplyNewCardsKA.flxSuplementryCardType.setVisibility(false);
            	frmApplyNewCardsKA.lblSuplementryCardType.text = "";
            	frmApplyNewCardsKA.flxRequestLimit.setVisibility(true);
            }else{
//             	frmApplyNewCardsKA.flxRequestLimit.setVisibility(false);
                frmApplyNewCardsKA.lblSuplementryCardTypeTitle.text = geti18Value("i18n.applycards.cardtype");
            	frmApplyNewCardsKA.flxSuplementryCardType.setVisibility(true);
            }
        }
       
    	if((type === "Reason")){
        	frmApplyNewCardsKA.lblReasonHint.setVisibility(false);
        	set_SERVCARDFLAG(selectedItems[0].lblJP, kony.newCARD.cardTYPE);
        	if(kony.newCARD.lastSelectionFlag === "RWC" || kony.newCARD.lastSelectionFlag === "RSD" || kony.newCARD.lastSelectionFlag === "RDC"){
            	frmApplyNewCardsKA.lblReasonHint.text = "*"+geti18Value("i18n.newCard.replaceCardFee");
            	frmApplyNewCardsKA.lblReasonHint.setVisibility(true);
            }else if(kony.newCARD.lastSelectionFlag === "NWC"){
            	frmApplyNewCardsKA.lblReasonHint.text = "*"+geti18Value("i18n.newCard.newWebChargeCardFee");
            	frmApplyNewCardsKA.lblReasonHint.setVisibility(true);
            }else if(kony.newCARD.lastSelectionFlag === "RPC"){
            	if(!isEmpty(kony.store.getItem("frmAccount"))){
                	if(kony.store.getItem("frmAccount").card_type_desc === "Classic" || (kony.store.getItem("frmAccount").card_name.indexOf("Classic") > -1))
                		frmApplyNewCardsKA.lblReasonHint.text = "*"+geti18Value("i18n.newCard.replaceCardFee"); 
                	else
                    	frmApplyNewCardsKA.lblReasonHint.text = "*"+geti18Value("i18n.newCard.replaceCCFee"); 
                }
            	frmApplyNewCardsKA.lblReasonHint.setVisibility(true);
            }
        }
    	/************************/
    	validate_APPLYCARDSDETAILS();
    	frmApplyNewCardsKA.forceLayout();
    	frmApplyNewCardsKA[kony.newCARD.fields[type][0]].skin = "sknlblanimated75";
        frmApplyNewCardsKA[kony.newCARD.fields[type][0]].top = "12%";
    	frmApplyNewCardsKA["flxBorder"+type].skin = "skntextFieldDividerGreen";
    	frmApplyNewCardsKA.show();

    }catch(e){
    	kony.print("Exception_selected_OPTIONS_APPLYNEWCARDS ::"+e);
    }
}

function set_SERVCARDFLAG(type, option){
	try{
    	switch(option){
          case "SuplementryCreditCard":
            kony.newCARD.lastSelectionFlag = "SC";
            break;
          case "PrimaryDebitCard":
            kony.newCARD.lastSelectionFlag = "DC";
            break;
          case "SuplementryDebitCard":
            kony.newCARD.lastSelectionFlag = "SD";
            break;
          case "WebchargeCard":
            kony.newCARD.lastSelectionFlag = "WC";
            break;
          case "PrimaryCreditCard":
            kony.newCARD.lastSelectionFlag = "C";
            break;
          case "PrimaryCreditCardReplace":
            kony.newCARD.lastSelectionFlag = "PC";
            break;
          case "Wearable":
            if(kony.newCARD.maincardTYPE === "Prepaid"){
            	kony.newCARD.lastSelectionFlag = "C";
            }else{
            	kony.newCARD.lastSelectionFlag = "SC";
            }
            break;
          case "Sticker":
            kony.newCARD.lastSelectionFlag = "SD";
            break;
          case "ReplacePrepaidCards":
          	kony.newCARD.lastSelectionFlag = "WC";
            break;
          case "ReplaceDebitCards":
          	kony.newCARD.lastSelectionFlag = "DC";
            break;
          case "UpgradeCreditCard":
          	kony.newCARD.lastSelectionFlag = "C";
            break;
        }
    	if(type === geti18Value("i18n.cards.newcard")){
        	kony.newCARD.lastSelectionFlag = "N"+kony.newCARD.lastSelectionFlag;
        }else{
        	kony.newCARD.lastSelectionFlag = "R"+kony.newCARD.lastSelectionFlag;
        }
    }catch(e){
    	kony.print("Exception_set_SERVCARDFLAG ::"+e);
    }
}

function validate_APPLYCARDSDETAILS(){
	try{
    	var field = "",valid = true;
    	for(var i in kony.newCARD.requiredFields[kony.newCARD.cardTYPE]){
        	field = kony.newCARD.requiredFields[kony.newCARD.cardTYPE][i];
        	if(kony.newCARD.fields[field][4] !== ""){
            	if(frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text !== "" && field !== "DeliveryMode"){
                  if(new RegExp(kony.newCARD.fields[field][4]).test(frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text)){
                      frmApplyNewCardsKA["flxBorder"+field].skin = "skntextFieldDividerOrange";
                      valid = false;
                  }else{
                      frmApplyNewCardsKA["flxBorder"+field].skin = "skntextFieldDividerGreen";
                  }
                }else if(frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text === "" && field !== "DeliveryMode"){
                  if(frmApplyNewCardsKA[kony.newCARD.fields[field][2]].isVisible === true){
                    frmApplyNewCardsKA["flxBorder"+field].skin = "skntextFieldDivider";
                    valid = false;
                  }
                }
            }
        	if((field !== "DeliveryMode" && field !== "Branch" && field !== "Notes") && 
               (frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text === null || frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text === "")){
            	if(frmApplyNewCardsKA[kony.newCARD.fields[field][2]].isVisible === true){
                  frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
                  return;
                }
            }
        	switch(field){
              case "DeliveryMode":
            		if(frmApplyNewCardsKA[kony.newCARD.fields[field][0]].text === "t"){
                		if(frmApplyNewCardsKA[kony.newCARD.fields.Branch[1]].text === null || frmApplyNewCardsKA[kony.newCARD.fields.Branch[1]].text === ""){
                    		frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
            				return;
                    	}
                	}else{
                		if(frmApplyNewCardsKA[kony.newCARD.fields.Address[1]].text === null || frmApplyNewCardsKA[kony.newCARD.fields.Address[1]].text === ""){
                    		frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
            				return;
                    	}
                	}
              		break;
              case "RequestLimit":
              		if(kony.newCARD.cardTYPE === "SuplementryCreditCard" || (kony.newCARD.maincardTYPE !== "Prepaid" && kony.newCARD.cardTYPE === "Wearable"))
              		if(frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text !== ""){
                      if(parseFloat(kony.store.getItem("frmAccount").card_limit) < parseFloat(frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text)){
                         frmApplyNewCardsKA["flxBorder"+field].skin = "skntextFieldDividerOrange";
                         frmApplyNewCardsKA.lblMobileNumberHint.setVisibility(true);
                         frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
                         valid = false;
                         return;
                      }
                      if(parseFloat(frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text) > 0){
                      }else{
                        frmApplyNewCardsKA["flxBorder"+field].skin = "skntextFieldDividerOrange";
                        frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
                        valid = false;
                      }
                    }else{
                       frmApplyNewCardsKA.lblMobileNumberHint.setVisibility(false);
                                             
                    }
              		break;
              case "Age":
                    if(parseFloat(frmApplyNewCardsKA[kony.newCARD.fields[field][1]].text) > 0){
                    }else{
                      frmApplyNewCardsKA["flxBorder"+field].skin = "skntextFieldDividerOrange";
                      frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
                      valid = false;
                    }
              		break;
            }
        }
      
    	if(valid){
    		frmApplyNewCardsKA.btnNext.skin = "jomopaynextEnabled";
        }
    }catch(e){
    	kony.print("Exception_validate_APPLYCARDSDETAILS ::"+e);
    }
}

function update_CARDDATA(flag){
	try{
    	if(flag === "ecommerce"){
        	if(frmEnableInternetTransactionKA.btnDaily.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].daily_internet_limit = frmEnableInternetTransactionKA.txtEcomLimit.text;
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].daily_mail_limit = frmEnableInternetTransactionKA.txtMailLimit.text;
            }else if(frmEnableInternetTransactionKA.btnWeekly.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].weekly_internet_limit = frmEnableInternetTransactionKA.txtEcomLimit.text;
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].weekly_mail_limit = frmEnableInternetTransactionKA.txtMailLimit.text;
            }else if(frmEnableInternetTransactionKA.btnMonthly.skin === "sknBtnBGWhiteBlue105Rd10"){
           		kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].monthly_internet_limit = frmEnableInternetTransactionKA.txtEcomLimit.text;
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].monthly_mail_limit = frmEnableInternetTransactionKA.txtMailLimit.text;
            }
        }else if(flag === "instantCash"){
        	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_point = parseInt(kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_point) - parseInt(frmInstantCash.txtFieldAmount.text)
        }else if(flag === "ATMPOS"){
        	if(frmATMPOSLimit.btnDaily.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].daily_atm_limit = frmATMPOSLimit.txtWithdrawalLimit.text;
                kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].daily_pos_limit = frmATMPOSLimit.txtPOSLimit.text;
            }else if(frmATMPOSLimit.btnWeekly.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].weekly_atm_limit = frmATMPOSLimit.txtWithdrawalLimit.text;
                kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].weekly_pos_limit = frmATMPOSLimit.txtPOSLimit.text;
            }else if(frmATMPOSLimit.btnMonthly.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].monthly_atm_limit = frmATMPOSLimit.txtWithdrawalLimit.text;
                kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].monthly_pos_limit = frmATMPOSLimit.txtPOSLimit.text;
            }
        }else if(flag === "ATM"){
        	kony.print("ATM LIMIT ::"+frmATMPOSLimit.txtWithdrawalLimit.text);
        	if(frmATMPOSLimit.btnDaily.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].daily_atm_limit = frmATMPOSLimit.txtWithdrawalLimit.text.toString();
            }else if(frmATMPOSLimit.btnWeekly.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].weekly_atm_limit = frmATMPOSLimit.txtWithdrawalLimit.text.toString();
            }else if(frmATMPOSLimit.btnMonthly.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].monthly_atm_limit = frmATMPOSLimit.txtWithdrawalLimit.text.toString();
            }
        }else if(flag === "POS"){
        	kony.print("POS LIMIT ::"+frmATMPOSLimit.txtPOSLimit.text);
        	if(frmATMPOSLimit.btnDaily.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].daily_pos_limit = frmATMPOSLimit.txtPOSLimit.text;
            }else if(frmATMPOSLimit.btnWeekly.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].weekly_pos_limit = frmATMPOSLimit.txtPOSLimit.text;
            }else if(frmATMPOSLimit.btnMonthly.skin === "sknBtnBGWhiteBlue105Rd10"){
            	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].monthly_pos_limit = frmATMPOSLimit.txtPOSLimit.text;
            }
        }
    }catch(e){
    	kony.print("Exception_update_CARDDATA ::"+e);
    }
}

function set_SUPPLEMENTRYCARDTYPE(cardType, cardTypeDes){
	try{
    	kony.print("card type ::"+cardType);
    	var list = [["classic","gold","platinum"],
                    [geti18Value("i18n.cards.classic"),geti18Value("i18n.cards.gold"),geti18Value("i18n.cards.platinum"),geti18Value("i18n.cards.mastercardworld")]];
    	var i = 0;
    	var temp = [], j = 0;
    	if(kony.newCARD.maincardTYPE === "Credit" && kony.newCARD.cardTYPE === "UpgradeCreditCard"){
          var limit_CARD_REQ = frmApplyNewCardsKA.txtRequestLimit.text;
          if((parseInt(MIN_CARDLIMIT_CLASSIC) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_CLASSIC))){
            i = 1;
          }
          if((parseInt(MIN_CARDLIMIT_GOLD) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_GOLD))){
            i = 2;
          }
          if((parseInt(MIN_CARDLIMIT_PLATINUM) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MIN_CARDLIMIT_WORLD))){
            i = 3;
          }
          if((parseInt(MAX_CARDLIMIT_PLATINUM) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_WORLD))){
            i = 0;
          }
          
          if((parseInt(MIN_CARDLIMIT_WORLD) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_PLATINUM))){
          	return [geti18Value("i18n.cards.platinum"),geti18Value("i18n.cards.mastercardworld")];
          }
//         	j = i;
//         	i = 4;
        }else{
          for(i in list[0]){
              if((cardType.toLowerCase().indexOf(list[0][i]) > -1) || (cardTypeDes.toLowerCase().indexOf(list[0][i]) > -1)){
                  i = parseInt(i)+1;
                  break;
              }
          }
        }
    	if(i > 0){
        	for(;j<i;j++){
            	temp.push(list[1][j]);
            }
        	kony.print("temp data ::"+JSON.stringify(temp));
        	return temp;
        }
    	return list[1];
    }catch(e){
    	kony.print("Exception_set_SUPPLEMENTRYCARDTYPE ::"+e);
    }
}

function reset_APPLYCARDS_CONFIRMATION(){
	try{
      for(var a in kony.newCARD.requiredFields.ALL){
        if(kony.newCARD.requiredFields.ALL[a] !== "Branch" && kony.newCARD.requiredFields.ALL[a] !== "Address"){
          frmApplyNewCardsConfirmKA["lbl"+kony.newCARD.requiredFields.ALL[a]].text = "";
          frmApplyNewCardsConfirmKA["flx"+kony.newCARD.requiredFields.ALL[a]].setVisibility(false);
        }
      }
      frmApplyNewCardsConfirmKA.lblAgeTitle.text = geti18Value("i18n.common.age");
      frmApplyNewCardsConfirmKA.lblAccountNumberTitle.text = geti18Value("i18n.common.accountNumber");
      frmApplyNewCardsConfirmKA.lblTermsandConditionsCheckBox.text = "q";
      frmApplyNewCardsConfirmKA.richtxtTermsandConditions.text = "<U>"+geti18Value("i18n.tnc.cardrequest")+"</U>";
    }catch(e){
    	kony.print("Exception_reset_APPLYCARDS_CONFIRMATION ::"+e);
    }
}

function navigate_APPLYCARDS_CONFIRMATION(field){
	try{
    	kony.print("selected field ::"+field);
    	reset_APPLYCARDS_CONFIRMATION();
    	var delMode = "", frmField = "";
    	for(var a in kony.newCARD.requiredFields[field]){
      	  kony.print("Field :: "+kony.newCARD.requiredFields[field][a]+""+kony.newCARD.fields[kony.newCARD.requiredFields[field][a]][1]);
          if(kony.newCARD.requiredFields[field][a] === "DeliveryMode"){
          	delMode = (frmApplyNewCardsKA.btnDeliveryModeBranch.text === "t")?frmApplyNewCardsKA.lblBranchName.text:frmApplyNewCardsKA.txtAddress.text;
          	frmApplyNewCardsConfirmKA["lbl"+kony.newCARD.requiredFields[field][a]].text = delMode;
          	frmApplyNewCardsConfirmKA["flx"+kony.newCARD.requiredFields[field][a]].setVisibility(true);
          }else if(kony.newCARD.requiredFields[field][a] !== "Branch" && kony.newCARD.requiredFields[field][a] !== "Address"){
          	frmField = kony.newCARD.fields[kony.newCARD.requiredFields[field][a]][1];
            frmApplyNewCardsConfirmKA["lbl"+kony.newCARD.requiredFields[field][a]].text = frmApplyNewCardsKA[frmField].text;
          	if(kony.newCARD.requiredFields[field][a] !== "Reason" && frmApplyNewCardsKA[frmField].text !== "" && frmApplyNewCardsKA[frmField].text !== null)
            	frmApplyNewCardsConfirmKA["flx"+kony.newCARD.requiredFields[field][a]].setVisibility(true);
            else if(kony.newCARD.requiredFields[field][a] === "Reason"){
            	if(kony.newCARD.cardTYPE === "PrimaryCreditCardReplace" || kony.newCARD.cardTYPE === "ReplaceDebitCards" || kony.newCARD.cardTYPE === "ReplacePrepaidCards"){
                  frmApplyNewCardsConfirmKA["flx"+kony.newCARD.requiredFields[field][a]].setVisibility(true);
                }
            }
          }
          if(kony.newCARD.cardTYPE === "UpgradeCreditCard" && kony.newCARD.requiredFields[field][a] === "SuplementryCardType"){
          	frmApplyNewCardsConfirmKA.lblSuplementryCardTypeTitle.text = geti18Value("i18n.applycards.cardtype");
          }else if(kony.newCARD.requiredFields[field][a] === "SuplementryCardType"){
          	frmApplyNewCardsConfirmKA.lblSuplementryCardTypeTitle.text = geti18Value("i18n.newCard.supplementrycardtype");
          }
          if(field === "SuplementryCreditCard"){
          	frmApplyNewCardsConfirmKA.lblCreditCardNumberTitle.text = geti18Value("i18n.applycards.primarycardnumber");
          }else{
            frmApplyNewCardsConfirmKA.lblCreditCardNumberTitle.text = geti18Value("i18n.newCard.primarycreditcardnumber");
          }
        }
    	if(kony.newCARD.maincardTYPE === "Prepaid"){
        	if(field === "Wearable"){
              frmApplyNewCardsConfirmKA.flxCreditCardNumber.setVisibility(false);
              frmApplyNewCardsConfirmKA.flxLimitType.setVisibility(false);
              frmApplyNewCardsConfirmKA.flxRequestLimit.setVisibility(false);
            }
        	frmApplyNewCardsConfirmKA.lblAgeTitle.text = geti18Value("i18n.cards.ageprepaid");
        	frmApplyNewCardsConfirmKA.lblAccountNumberTitle.text = geti18Value("i18n.applycards.comissionaccount");
        }else if(kony.newCARD.maincardTYPE === "Credit" && field === "Wearable"){
        	frmApplyNewCardsConfirmKA.flxAccountNumber.setVisibility(false);
        	frmApplyNewCardsConfirmKA.lblAgeTitle.text = geti18Value("i18n.cards.agecredit");
        }
    	frmApplyNewCardsConfirmKA.show();
    }catch(e){
    	kony.print("Exception_navigate_APPLYCARDS_CONFIRMATION ::"+e);
    }
}

function onClick_BACK_APPLYCARDS_CONFIRMATION(){
	frmApplyNewCardsKA.show();
	reset_APPLYCARDS_CONFIRMATION();
}

function navigate_DEBIT_LINKED_ACCOUNTS(){
	try{
//     	process_LINKED_ACCOUNTS_DATA(kony.retailBanking.globalData.accountsDashboardData.accountsData);
    	frmCardLinkedAccounts.btnNext.skin = "jomopaynextDisabled";
    	serv_GET_DEBIT_CARD_LINKED_ACCOUNTS(kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex]);
    }catch(e){
    	kony.print("Exception_navigate_DEBIT_LINKED_ACCOUNTS ::"+e);
    }
}

function process_LINKED_ACCOUNTS_DATA(data, resData){
	try{
    	var temp = [];
    	card_LINKED_ACCOUNTS = resData;
    	card_LINKED_ACC_COUNT = resData.length;
    	var card_acc_number = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_acc_num;
        for(var i in data){
        	if(data[i].currencyCode === "JOD"){
              for(var j in resData){
                var nick_name=data[i].AccNickName;
                  if(data[i].accountID === resData[j].acc_num){
                      kony.print("accountID"+data[i].accountID +"acc_num ::"+ resData[j].acc_num);
                      data[i].flxLinkedAccountsDisabled = {"isVisible":false};
                      data[i].flxLinkedAccountsEnable = {"isVisible":true};
                    //fix for 1862;
                    if(nick_name !== undefined && nick_name !== null && nick_name !== "")
                    {
                        data[i].lblAccountNickName={"text":nick_name,"isVisible":true};
                      data[i].lblAccountName={"text":data[i].accountName,"isVisible":true};
                      data[i].lblAccountNumber={"text":data[i].accountID,"isVisible":false};
                    }
                    else {
                          data[i].lblAccountNickName={"text":nick_name,"isVisible":false};
                      data[i].lblAccountName={"text":data[i].accountName,"isVisible":true};
                      data[i].lblAccountNumber={"text":data[i].accountID,"isVisible":true};
                      }
      				//end	
                      if(data[i].accountID === card_acc_number ){
                          frmCardLinkedAccounts.lblDefaultAccount.text = card_acc_number;
                      }
                      break;
                  }else{
                      data[i].flxLinkedAccountsDisabled = {"isVisible":true};
                      data[i].flxLinkedAccountsEnable = {"isVisible":false};
                    //fix for 1862;
                    if(nick_name !== undefined && nick_name !== null && nick_name !== "")
                    {
                        data[i].lblAccountNickName={"text":nick_name,"isVisible":true};
                      data[i].lblAccountName={"text":data[i].accountName,"isVisible":true};
                      data[i].lblAccountNumber={"text":data[i].accountID,"isVisible":false};
                    }
                    else {
                          data[i].lblAccountNickName={"text":nick_name,"isVisible":false};
                      data[i].lblAccountName={"text":data[i].accountName,"isVisible":true};
                      data[i].lblAccountNumber={"text":data[i].accountID,"isVisible":true};
                      }
      				//end	
                  }
              }
              temp.push(data[i]);
            }
        }
    	frmCardLinkedAccounts.segCardLinkedAccounts.removeAll();
      
      //fix for 1862
//     	frmCardLinkedAccounts.segCardLinkedAccounts.widgetDataMap = {"lblAccountNickName":"AccNickName",
//                                                                     "lblAccountName":"accountName",
//                                                                     "lblAccountNumber":"accountID",
//                                                                     "flxLinkedAccountsDisabled":"flxLinkedAccountsDisabled",
//                                                                     "flxLinkedAccountsEnable":"flxLinkedAccountsEnable"};
    	frmCardLinkedAccounts.segCardLinkedAccounts.setData(temp);
    	frmCardLinkedAccounts.show();
    }catch(e){
    	kony.print("Exception_process_LINKED_ACCOUNTS_DATA ::"+e);
    }
}

function onSelection_LINKED_ACCOUNTS(eventObj,context){
	try{
    	kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    	var data = context.widgetInfo.selectedRowItems[0];
    	if(data.flxLinkedAccountsEnable.isVisible === true && data.lblAccountNumber.text !== frmCardLinkedAccounts.lblDefaultAccount.text){
        	data.flxLinkedAccountsEnable.isVisible = false;
        	data.flxLinkedAccountsDisabled.isVisible = true;
        	card_LINKED_ACC_COUNT = parseInt(card_LINKED_ACC_COUNT)-1;
        }else if(data.flxLinkedAccountsEnable.isVisible === true && data.lblAccountNumber.text == frmCardLinkedAccounts.lblDefaultAccount.text){/*hassan BMA-1844 */
            customAlertPopup(geti18Value("i18n.opening_account.btnInfo"), geti18Value("i18n.linked.defaultCard"), popupCommonAlertDimiss, "");
          	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        	return;
            /*hassan BMA-1844 */
        }else if(data.flxLinkedAccountsEnable.isVisible === false){
        	if(card_LINKED_ACC_COUNT >= 8 && data.lblAccountNumber.text !== frmCardLinkedAccounts.lblDefaultAccount.text){
            	customAlertPopup(geti18Value("i18n.opening_account.btnInfo"), geti18Value("i18n.linked.maximumaccounts"), popupCommonAlertDimiss, "");
              kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();	
              return;
            }else{
        		data.flxLinkedAccountsDisabled.isVisible = false;
        		data.flxLinkedAccountsEnable.isVisible = true;
            	card_LINKED_ACC_COUNT = parseInt(card_LINKED_ACC_COUNT)+1;
            }
        }
    	kony.print("Total Linked Accouns ::"+card_LINKED_ACC_COUNT);
        frmCardLinkedAccounts.segCardLinkedAccounts.setDataAt(data,parseInt(context.rowIndex));
    	validate_CARD_LINK_ACCOUNT();
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }catch(e){
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	kony.print("Exception_onSelection_LINKED_ACCOUNTS ::"+e);
    }
}

function onSelection_DEFAULT_ACCOUNTS(defaultAccID){
	try{
    	var data = frmCardLinkedAccounts.segCardLinkedAccounts.data;
    	for(var i in data){
        	if(data[i].lblAccountNumber === defaultAccID){
            	data[i].flxLinkedAccountsDisabled.isVisible = false;
        		data[i].flxLinkedAccountsEnable.isVisible = true;
            }
        }
    	frmCardLinkedAccounts.lblDefaultAccount.text = defaultAccID;
    	frmCardLinkedAccounts.segCardLinkedAccounts.removeAll();
    	frmCardLinkedAccounts.segCardLinkedAccounts.setData(data);
    	validate_CARD_LINK_ACCOUNT();
    	frmCardLinkedAccounts.forceLayout();
    	frmCardLinkedAccounts.show();
    }catch(e){
    	kony.print("Exception_onSelection_DEFAULT_ACCOUNTS ::"+e);
    }
}

function onSelection_TNC_APPLYCARDS_CONFIRMATION(){
	try{
    	var tncMsg = "";
    	if(frmApplyNewCardsConfirmKA.lblTermsandConditionsCheckBox.text === "p"){
          frmApplyNewCardsConfirmKA.lblTermsandConditionsCheckBox.text = "q";
        }else{
          frmApplyNewCardsConfirmKA.lblTermsandConditionsCheckBox.text = "p";
        }
    }catch(e){
    	kony.print("Exception_onSelection_TNC_APPLYCARDS_CONFIRMATION ::"+e);
    }
}

function navigate_INTERNAL_TERMSANDCONDITION_SCREEN(currentForm, type){
	try{
    	navigation_STORE_PREVIOUS_FORM(currentForm);
    	switch(type){
          case "UpgradeCreditCard":
            tncMsg = geti18Value("i18n.tnc.upgradecards")+"  "+geti18Value("i18n.tnc.upgradecardsFees");
            break;
          case "SuplementryCreditCard" || "SuplementryDebitCard" || "Wearable":
            if(type === "Wearable" && kony.newCARD.maincardTYPE === "Prepaid")
            	//tncMsg = geti18Value("i18n.applycards.prepaidTNC");// hassan BMA-1460
                tncMsg = geti18Value("i18n.applycards.prepaidZeroTNC");// hassan BMA-1460
            else
            	tncMsg = geti18Value("i18n.tnc.supplementarycards");
            break;
          default:
            if(kony.newCARD.maincardTYPE === "Prepaid")
             	tncMsg = geti18Value("i18n.applycards.prepaidTNC");
            else if(kony.newCARD.maincardTYPE === "Credit")
            	tncMsg = geti18Value("i18n.applycards.creditTNC");
            else
            	tncMsg = geti18Value("i18n.account.subCheck1");
            break;
        }
        frmTermsAndCondtions.lblTnc.text = tncMsg;
    	frmTermsAndCondtions.show();
    }catch(e){
    	kony.print("Exception_navigate_INTERNAL_TERMSANDCONDITION_SCREEN ::"+e);
    }
}

function show_CARD_TYPE(limit, cardLimit){
	try{
    	if((parseInt(MIN_CARDLIMIT_CLASSIC) <= parseInt(limit))){//(parseInt(limit) < parseInt(cardLimit)) && 
        	frmApplyNewCardsKA.lblRequestLimitHint.text = geti18Value("i18n.ecommerce.maxlimitis")+cardLimit;
        	frmApplyNewCardsKA.btnSuplementryType.setVisibility(false);
            frmApplyNewCardsKA.lblSuplementryCardTypeTitle.skin = "sknlblanimated75";
        	frmApplyNewCardsKA.lblSuplementryCardTypeTitle.top = "12%";
    		frmApplyNewCardsKA.flxBorderSuplementryCardType.skin = "skntextFieldDividerGreen";
        	var limit_CARD_REQ = frmApplyNewCardsKA.txtRequestLimit.text;
            if((parseInt(MIN_CARDLIMIT_CLASSIC) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_CLASSIC))){
              frmApplyNewCardsKA.lblSuplementryCardType.text = geti18Value("i18n.cards.classic");
            }
            if((parseInt(MIN_CARDLIMIT_GOLD) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_GOLD))){
              frmApplyNewCardsKA.lblSuplementryCardType.text = geti18Value("i18n.cards.gold");
            }
            if((parseInt(MIN_CARDLIMIT_PLATINUM) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MIN_CARDLIMIT_WORLD))){
              frmApplyNewCardsKA.lblSuplementryCardType.text = geti18Value("i18n.cards.platinum");
            }
            if((parseInt(MAX_CARDLIMIT_PLATINUM) <= parseInt(limit_CARD_REQ))){//&& (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_WORLD))
              frmApplyNewCardsKA.lblSuplementryCardType.text = geti18Value("i18n.cards.mastercardworld");
            }
          	if((parseInt(MIN_CARDLIMIT_WORLD) <= parseInt(limit_CARD_REQ)) && (parseInt(limit_CARD_REQ) <= parseInt(MAX_CARDLIMIT_PLATINUM))){
            	frmApplyNewCardsKA.lblSuplementryCardType.text = "";
            	frmApplyNewCardsKA.btnSuplementryType.setVisibility(true);
            	frmApplyNewCardsKA.flxSuplementryCardType.setVisibility(true);
            }
        	if(frmApplyNewCardsKA.lblSuplementryCardType.text !== ""){
            	frmApplyNewCardsKA.flxSuplementryCardType.setVisibility(true);
            }
        }else if(parseInt(limit) < parseInt(MIN_CARDLIMIT_CLASSIC)){
        	frmApplyNewCardsKA.flxSuplementryCardType.setVisibility(false);
            frmApplyNewCardsKA.flxBorderRequestLimit.skin = "sknFlxOrangeLine";
            frmApplyNewCardsKA.lblRequestLimitHint.text = geti18Value("i18n.update.minlimithint")+MIN_CARDLIMIT_CLASSIC;
        	frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
        }else{
        	frmApplyNewCardsKA.lblSuplementryCardType.text = "";
        	frmApplyNewCardsKA.btnSuplementryType.setVisibility(true);
        	frmApplyNewCardsKA.lblSuplementryCardTypeTitle.width = "80%";
            frmApplyNewCardsKA.lblSuplementryCardTypeTitle.height = "40%";
            frmApplyNewCardsKA.lblSuplementryCardTypeTitle.skin = "sknLblNextDisabled";
            frmApplyNewCardsKA.lblSuplementryCardTypeTitle.top = "35%";
    		frmApplyNewCardsKA.flxBorderSuplementryCardType.skin = "skntextFieldDivider";
        	frmApplyNewCardsKA.flxSuplementryCardType.setVisibility(false);
        	frmApplyNewCardsKA.btnNext.skin = "jomopaynextDisabled";
        }
    }catch(e){
      kony.print("Exception_show_CARD_TYPE ::"+e);
    }
}

function navigate_CARD_ACCOUNT_LINK_CONFIRM(){
	try{
    	frmCardLinkedAccountsConfirm.lblDefaultAccountTitle.setVisibility(false);
    	frmCardLinkedAccountsConfirm.lblDefaultAccount.setVisibility(false);
    	frmCardLinkedAccountsConfirm.lblDefaultAccount.text = "";
    	var card_acc_num_def = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_acc_num;
    	var segData = frmCardLinkedAccounts.segCardLinkedAccounts.data, tempLink = [], tempUnLink = [], defaultAcc = [],isLinked = false, data = [];
    	kony.print("segment Data ::"+JSON.stringify(segData));
    	for(var i in segData){
        	isLinked = false;
    		for(var j in card_LINKED_ACCOUNTS){
              if(card_LINKED_ACCOUNTS[j].acc_num === segData[i].lblAccountNumber){
                if(segData[i].flxLinkedAccountsDisabled.isVisible === true){
                	kony.print("Disabled");
              		tempUnLink.push(segData[i]);
                }else if(segData[i].flxLinkedAccountsEnable.isVisible === true){
                	isLinked = true;
                }
                break;
              }
        	}
        	if(segData[i].flxLinkedAccountsEnable.isVisible === true){// && !isLinked
            	kony.print("Link");
              	tempLink.push(segData[i]);
            }
        	if((card_acc_num_def !== frmCardLinkedAccounts.lblDefaultAccount.text) &&
               (frmCardLinkedAccounts.lblDefaultAccount.text === segData[i].lblAccountNumber)){
              defaultAcc.push(segData[i]);
            }
        }
    	if(!isEmpty(defaultAcc))
        	data.push([{"lblTitle":geti18Value("i18n.linkcard.defaultaccounts")},defaultAcc]);
      
    	if(!isEmpty(tempLink))
        	data.push([{"lblTitle":geti18Value("i18n.debitcardlinkedacc.linkedacc")},tempLink]);
      
    	if(!isEmpty(tempUnLink))
        	data.push([{"lblTitle":geti18Value("i18n.cards.unlinkaccounts")},tempUnLink]);
      
    	frmCardLinkedAccountsConfirm.segLinkedAccounts.widgetDataMap = {"lblAccountNickName":"AccNickName",
                                                                    "lblAccountName":"accountName",
                                                                    "lblAccountNumber":"accountID",
                                                                     "lblTitle":"lblTitle"};
    	frmCardLinkedAccountsConfirm.flxBack.setVisibility(true);
    	frmCardLinkedAccountsConfirm.btnConfirm.text = geti18Value("i18n.transfers.CONFIRM");
    	frmCardLinkedAccountsConfirm.segLinkedAccounts.removeAll();
    	if(data.length > 0){
        	frmCardLinkedAccountsConfirm.lblLinkedAccountsTitle.setVisibility(false);
    		frmCardLinkedAccountsConfirm.segLinkedAccounts.setData(data);
        }else
        	frmCardLinkedAccountsConfirm.lblLinkedAccountsTitle.setVisibility(false);
    	frmCardLinkedAccountsConfirm.show();
    }catch(e){
    	kony.print("Exception_navigate_CARD_ACCOUNT_LINK_CONFIRM ::"+e);
    }
}

function get_ACCOUNT_LINK_STATUS(data){
	try{
    	var tmpLink = [], tmpUnLink = [], defaultAccount = [], isLinked = false;
    	var card_acc_num = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_acc_num;
    	kony.print("Linked Accounts ::"+JSON.stringify(card_LINKED_ACCOUNTS));
        for(var j in data){
          isLinked = false;
          for(var i in card_LINKED_ACCOUNTS){
            if(card_LINKED_ACCOUNTS[i].acc_num === data[j].lblAccountNumber.text){
              if(data[j].flxLinkedAccountsDisabled.isVisible){
                tmpUnLink.push(data[j]);
              }else if(data[j].flxLinkedAccountsEnable.isVisible){
                isLinked = true;
              }
              break;
            }
          }
          if(data[j].flxLinkedAccountsEnable.isVisible && !isLinked){
            tmpLink.push(data[j]);
          }
          if((frmCardLinkedAccounts.lblDefaultAccount.text === data[j].lblAccountNumber.text) &&
             (card_acc_num !== data[j].lblAccountNumber.text)){
            defaultAccount.push(data[j]);
          }
        }
    	kony.print("tmpLink :: "+JSON.stringify(tmpLink)+" tmpUnLink :: "+JSON.stringify(tmpUnLink)+" defaultAccount :: "+JSON.stringify(defaultAccount));
    	serv_DATA_CARD_LINK = {"link":tmpLink,"unlink":tmpUnLink,"defaultAccount":defaultAccount};
    	return [tmpLink,tmpUnLink,defaultAccount];
    }catch(e){
    	kony.print("Exception_get_ACCOUNT_LINK_STATUS ::"+e);
    }
}

function validate_CARD_LINK_ACCOUNT(){
	try{
    	var isChecked = false;
    	var data = frmCardLinkedAccounts.segCardLinkedAccounts.data;
    	var count = 0;
    	for(var i in data){
        	isChecked = false;
        	for(var j in card_LINKED_ACCOUNTS){
            	if(card_LINKED_ACCOUNTS[j].acc_num === data[i].lblAccountNumber){
                   if(data[i].flxLinkedAccountsDisabled.isVisible === true){
                     kony.print(JSON.stringify(data[i]));
                   	  count++;
                      //frmCardLinkedAccounts.btnNext.skin = "jomopaynextEnabled";
                  }
                	isChecked = true;
                	break;
                }
            }
        	if(isChecked === false){
            	if(data[i].flxLinkedAccountsEnable.isVisible === true){
                	count++;
                	//frmCardLinkedAccounts.btnNext.skin = "jomopaynextEnabled";
                }
            }
        }
    	if(frmCardLinkedAccounts.lblDefaultAccount.text !== kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_acc_num){
        	count++;
         	//frmCardLinkedAccounts.btnNext.skin = "jomopaynextEnabled";
        }
    	if(count > 0)
          frmCardLinkedAccounts.btnNext.skin = "jomopaynextEnabled";
    	else
          frmCardLinkedAccounts.btnNext.skin = "jomopaynextDisabled";
    }catch(e){
    	kony.print("Exception_validate_CARD_LINK_ACCOUNT ::"+e);
    }
}

function data_CARD_LINK_SUCCESS_SCREEN(data, flag){
	try{
    	kony.print("data ::"+JSON.stringify(data)+" :: flag ::"+flag)
    	if(!isEmpty(data)){
          if(flag !== "defaultAccount"){
            for(var i in data){
                if(data[i].errorCode === "00000")
                    serv_DATA_CARD_LINK[flag][i].isSuccess = true;
                else
                    serv_DATA_CARD_LINK[flag][i].isSuccess = false;
            }
          }else if(flag === "defaultAccount"){
          	if(data === "00000")
              serv_DATA_CARD_LINK.defaultAccount[0].isSuccess = true;
            else
              serv_DATA_CARD_LINK.defaultAccount[0].isSuccess = false;
          }
        }else{
        	for(var i in serv_DATA_CARD_LINK[flag]){
            	serv_DATA_CARD_LINK[flag][i].isSuccess = false;
            }
        }
    }catch(e){
    	kony.print("Exception_data_CARD_LINK_SUCCESS_SCREEN ::"+e);
    }
}

function navigate_CARD_LINK_ACCOUNT_CONFIRMATION(){
	try{
    	var temp = [], segData = [], nickname = "", flag = ["defaultAccount","link", "unlink"],title = [geti18Value("i18n.linkcard.defaultaccounts"),geti18Value("i18n.debitcardlinkedacc.linkedacc"),geti18Value("i18n.cards.unlinkaccounts")];
    	for(var check in flag){
          kony.print("check ::"+check);
          temp = [];
          if(!isEmpty(serv_DATA_CARD_LINK[flag[check]])){
              for(var i in serv_DATA_CARD_LINK[flag[check]]){
                nickname = serv_DATA_CARD_LINK[flag[check]][i].accountName;
                if(!isEmpty(serv_DATA_CARD_LINK[flag[check]][i].AccNickName))
                  nickname = serv_DATA_CARD_LINK[flag[check]][i].AccNickName;
                
				temp.push({"AccNickName":nickname,
                           "accountName":serv_DATA_CARD_LINK[flag[check]][i].lblAccountNumber,
                           "lblAccountNumber":serv_DATA_CARD_LINK[flag[check]][i].isSuccess?{text:geti18Value("i18n.common.success"),isVisible:true,top:"-2dp"}:{text:geti18Value("i18n.Bene.Failed"),isVisible:true,top:"-2dp"},
                           "flxIcon1":serv_DATA_CARD_LINK[flag[check]][i].isSuccess?{backgroundColor:"10BF00",isVisible:true}:{backgroundColor:"FF0000",isVisible:true},
                           "lblTick":serv_DATA_CARD_LINK[flag[check]][i].isSuccess?"r":"O",
                           "flxLinkedAccountsDisabled":{isVisible:false},
                           "flxLinkedAccountsEnable":{isVisible:false}});
              }
          	  segData.push([{"lblTitle":title[check]},temp]);
          	  if(flag[check] === "defaultAccount")
                kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_acc_num = serv_DATA_CARD_LINK.defaultAccount[0].lblAccountNumber;
          }
        }
    	kony.print("segData ::"+JSON.stringify(segData));
    	frmCardLinkedAccountsConfirm.segLinkedAccounts.widgetDataMap = {"lblAccountNickName":"AccNickName",
                                                                    "lblAccountName":"accountName",
                                                                    "lblAccountNumber":"lblAccountNumber",
                                                                    "flxIcon1":"flxIcon1",
                                                                    "lblTick":"lblTick",
                                                                    "flxLinkedAccountsDisabled":"flxLinkedAccountsDisabled",
                                                                    "flxLinkedAccountsEnable":"flxLinkedAccountsEnable",
                                                                    "lblTitle":"lblTitle"};
    	frmCardLinkedAccountsConfirm.flxBack.setVisibility(false);
    	frmCardLinkedAccountsConfirm.btnConfirm.text = geti18Value("i18n.cards.gotoCards");
    	frmCardLinkedAccountsConfirm.segLinkedAccounts.removeAll();
    	frmCardLinkedAccountsConfirm.segLinkedAccounts.setData(segData);
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }catch(e){
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	kony.print("Exception_navigate_CARD_LINK_ACCOUNT_CONFIRMATION ::"+e);
    }
}