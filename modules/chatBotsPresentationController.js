kony = kony || {};
kony.chatbotPresentation = function(){
  kony.print("in chat bot presentation");
};

kony.chatbotPresentation.isFirstTimeOpened = true;
kony.chatbotPresentation.botOffline = false;
kony.chatbotPresentation.lastTyped = null;
kony.chatbotPresentation.nearByAtmsData = null;
kony.chatbotPresentation.fromChatBot = false;

kony.chatbotPresentation.prototype.firstTimeOpen = function(){
  if(kony.chatbotPresentation.isFirstTimeOpened){
    kony.chatbots.bot.clearChatBox();
    kony.chatbotPresentation.isFirstTimeOpened = false;
    this.predictString(null,"introduction");
    var savedCredential = kony.sdk.mvvm.KonyApplicationContext.getRecentlyLoggedUserDetails();
    var username = kony.retailBanking.globalData.globals.userObj.userName || frmLoginKA.tbxusernameTextField.text || savedCredential["username"]  || frmLoginKA.lblChangeUserName.text || DecryptValue(kony.store.getItem("userName"));
    kony.chatbots.bot.addMessageWithIcon("Hello "+username+"!");
    kony.chatbotPresentation.lastTyped = "bot";
  }
};

kony.chatbotPresentation.prototype.clean = function(){
  try{
    kony.chatbotPresentation.isFirstTimeOpened = true;
    kony.chatbotPresentation.botOffline = false;
    kony.chatbotPresentation.lastTyped = null;
  }catch(err){

  }
};

kony.chatbotPresentation.prototype.predictString = function(type,str){
  try{
    (new kony.chatbot.command()).predict(type,str,this.predictReply.bind(this));
  }catch(err){
    kony.print("err "+err);
  }
};

kony.chatbotPresentation.prototype.predictReply = function(response){
  try{
    if(response){
      this.botMessage(response);
    }
  }catch(err){
    kony.print("err "+err);
  }
};
kony.chatbotPresentation.prototype.botMessage = function(msg){
  try{
    var message = msg.reply;
    var action = msg.action;
    var parameters = msg.parameters;

    if(action !== null && action !== undefined){
      switch(action){
        case 'showAccountBalance':
          var mesg = parameters.message;
          this.showAccountBalance(message,mesg);
          break;
        case 'showATM' :
          var type = parameters.type;
          this.showATMs(message,action,type);
          break;
        case 'displayATMS' :
          var data = parameters.data;
          this.displayATMS(message,data);
          break;
        case 'payCreditCard' :
          var amount = parameters.amount;
          this.payCreditCard(message,action,amount);
          break;
        case 'callCustomerService' :
          this.callCustomerService(message);
          break;
        case 'showTransactions':
          var accountType = parameters.accountType;
          this.showTransactions(message,accountType);
          break;
        case 'payPerson' :
          var fromAccount = parameters.fromAccount;
          var amountPay = parameters.amount;
          this.payPerson(message,fromAccount,amountPay);
          break;
        case 'exit' : 
          kony.print("exit bot");
          break;
      }
    }
    else{
      this.displayBotText(message);
    }
  }catch(err){
    kony.print(err);
  }
};

kony.chatbotPresentation.prototype.displayBotText = function(data){
  try{
    kony.chatbots.bot.dismissLoading();
    if(data !== null && data !== undefined){
      var length = data.length;
      if(length > 0){
        if(kony.chatbotPresentation.lastTyped === "bot"){
          kony.chatbots.bot.addMessage(false,data[0]);
        }
        else{
          kony.chatbots.bot.addMessageWithIcon(data[0]);
          kony.chatbotPresentation.lastTyped = "bot";
        }
        for(var i = 1;i<length;i++){
          kony.chatbots.bot.addMessage(false,data[i]);
        }
        kony.chatbots.bot.addTime();
      }
    }
  }catch(err){
    kony.print(err);
  }
};

kony.chatbotPresentation.prototype.showAccountBalance = function(text,message){
  try{
    this.displayBotText(text);
    var noOfAccounts = message.length;
    var noOfRecordsToDisplay = 1;
    for(var i=0;i<noOfRecordsToDisplay;i++){
      var accountName = message[i].accountType;
      var accountBalance = message[i].accountBalance;
      var displayBalance = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountBalance);
      var msg = accountName+" \n\n"+"Available Balance : "+displayBalance;
      if(i === 0){
        kony.chatbots.bot.addMessageWithIcon(msg);
      }
      else{
        kony.chatbots.bot.addMessage(false,msg);
      }
    }
    if(noOfAccounts > noOfRecordsToDisplay){
      var displayText = "More ("+(noOfAccounts-noOfRecordsToDisplay)+")";
      kony.chatbots.bot.addMoreItemsForAccountBalance(message,displayText,noOfRecordsToDisplay,null);
    }
    kony.chatbots.bot.addTime();
    kony.chatbotPresentation.lastTyped = "bot";
  }catch(err){
    kony.print(err);
  }
};

kony.chatbotPresentation.prototype.onClickMoreAccountBalanceHandler = function(data,noOfRecordsDisplayed,index){
  try{
    frmChatBots.flexScrollChatBox.removeAt(index);
    index = index - 1;
    var length = data.length;
    var currentLength = 1;
    
    for(var i=0;i<currentLength;i++){
      index = index +1;
       var accountName = data[noOfRecordsDisplayed+i].accountType;
      var accountBalance = data[noOfRecordsDisplayed+i].accountBalance;
      var displayBalance = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(accountBalance);
      var msg = accountName+" \n\n"+"Available Balance : "+displayBalance;
        kony.chatbots.bot.addMessageAt(false,msg,index);
    }
    index = index + 1;
    var remaining = length - noOfRecordsDisplayed - currentLength;
    if(remaining > 0){
      var displayText = "More ("+remaining+")";
      kony.chatbots.bot.addMoreItemsForAccountBalance(data,displayText,(noOfRecordsDisplayed+currentLength),index);
    }
  }catch(err){
    
  }
};

kony.chatbotPresentation.prototype.showOptions = function(text,action,options){
  kony.print("Removed");
//   try{
//     this.displayBotText(text);
//     var data = [];
//     for(var i=0;i<options.length;i++){
//       var temp = {};
//       temp.text = options[i];
//       temp.onClickCallback = this.onClickOfOption.bind(this);
//       temp.parameter = options[i];
//       data.push(temp);
//     }
//     kony.chatbots.bot.addButtons(data);
//     var currentForm = kony.application.getCurrentForm().id;
//     if(currentForm === 'frmBotSplash'||currentForm === 'frmChatBots')
//         frmChatBots.show();
//   }catch(err){
//     kony.print(err);
//   }
};

kony.chatbotPresentation.prototype.showATMs = function(text,action,type){
  try{
    var self = this;
    kony.location.getCurrentPosition(function(response){
      gblIsCurrentLocation = true;
      if(response && response.coords && response.coords.latitude && response.coords.longitude){
        gblLatitude =response.coords.latitude;
        gbLlongitude =response.coords.longitude;
        var str = {};
        str.type = type;
        str.lat = String(gblLatitude);
        str.lon = String(gbLlongitude);
        str.str = "showATMS";
        self.predictString("showATMS",str);
      }
    },function(err){
      gblIsCurrentLocation = false;
      var str = {};
      str.type = type;
      str.lat = null;
      str.lon = null;
      str.str = "showATMS";
      self.predictString("showATMS",str);
      var errorMsg = "Error code: "+err.code;
      errorMsg = errorMsg+" message: "+err.message;
      //#ifdef android
      if(err.code == 2){
        kony.ui.Alert(i18n_turnOnLocationAlert,androidgeoCallBack,constants.ALERT_TYPE_CONFIRMATION,"Cancel","Settings","");
      }
      //#endif
    },{timeout:32000,fastestInterval:0,minimumTime : 0});
  }catch(err){

  }
};

kony.chatbotPresentation.prototype.displayATMS = function(text,data){
  try{
    this.displayBotText(text);
    var length = data.length;
    var noOfRecordsToDispaly = 3;
    if(length < 3){
      noOfRecordsToDispaly = length;
    }
    for(var i=0;i<noOfRecordsToDispaly;i++){
      kony.chatbots.bot.addAtmDetailFlex(data[i],null);
    }
    var temp = {};
    temp.callback = this.onClickOfMap.bind(this);
    temp.locationData = data;
    temp.noOfRecordsDisplayed = 3;
    var remaining = length - noOfRecordsToDispaly;
    if(noOfRecordsToDispaly === 3 && remaining > 0){
      var displayText = "More ("+remaining+")";
      kony.chatbots.bot.addMoreItemsFlexForMaps(temp,displayText,null);
    }
    //kony.chatbots.bot.addClickableMapFlex(temp,null);
  }catch(err){
    kony.print(err);
  }
};

kony.chatbotPresentation.prototype.onClickOfMap = function(locationData,noOfRecordsDisplayed,index){
  try{
    if(noOfRecordsDisplayed >= locationData.length){
      alert("done");
      return;
    }
    frmChatBots.flexScrollChatBox.removeAt(index);
    index = index-1;
    var length = locationData.length;
    var currentLength = 3;
    if(length-noOfRecordsDisplayed < 3){
      currentLength = length-noOfRecordsDisplayed;
    }
    for(var i=0;i<currentLength;i++){
      index = index+1;
      kony.chatbots.bot.addAtmDetailFlex(locationData[noOfRecordsDisplayed+i],index);
    }
    index = index+1;
    var temp = {};
    temp.callback = this.onClickOfMap.bind(this);
    temp.locationData = locationData;
    temp.noOfRecordsDisplayed = noOfRecordsDisplayed +currentLength;
    
    var remaining = length - noOfRecordsDisplayed - currentLength;
    if(noOfRecordsDisplayed+currentLength < length && remaining > 0){
      var displayText = "More ("+remaining+")";
      kony.chatbots.bot.addMoreItemsFlexForMaps(temp,displayText,index);
    }
    //kony.chatbots.bot.addClickableMapFlex(temp,Number(index));
    
    
    /*gblFormName = "frmChatBots";
    globalZoomLevel = 15;
    //frmLocatorKA.imgFilter.src = "filterunselected.png";
    //frmLocatorKA.imgFilter.isVisible = true;
    kony.retailbanking.AdvancedAtmFilter.isFilterApplied = false;
    var res = {};
    res.records = locationData;
    if(userAgent == "iPhone"){
      //frmLocatorKA.backButton.setVisibility(true);
    }
    else{
      //frmLocatorKA.hamburgerButton.setVisibility(false);
      //frmLocatorKA.backButton.setVisibility(true);
    }
    setDataMapCallBack(res);
*/
    
    
  }catch(err){
    kony.print(err);
  }
};


kony.chatbotPresentation.prototype.navigateToATMDetailsScreen = function(data){
  try{
    data.name = data.addressLine1;
    data.desc = data.addressLine2;
    data.lat = data.latitude;
    data.lon = data.longitude;
    kony.chatbotPresentation.fromChatBot = true;
    getDetailsData(data);
  }catch(err){
    
  }
};

kony.chatbotPresentation.prototype.navigateToMapWithOneRecord = function(data){
  try{
    gblFormName = "frmChatBots";
    globalZoomLevel = 15;
    //frmLocatorKA.imgFilter.src = "filterunselected.png";
    //frmLocatorKA.imgFilter.isVisible = true;
    kony.retailbanking.AdvancedAtmFilter.isFilterApplied = false;
    var res = {};
    var locationData = [];
    locationData.push(data);
    res.Locations = locationData;
    if(userAgent == "iPhone"){
      //frmLocatorKA.backButton.setVisibility(true);
    }
    else{
      //frmLocatorKA.hamburgerButton.setVisibility(false);
      //frmLocatorKA.backButton.setVisibility(true);
    }
    setDataMapCallBack(res);

  }catch(err){
    
  }
};

kony.chatbotPresentation.prototype.backToChatBotsFromMaps = function(){
  try{
    kony.chatbots.bot.backToChatBot();
  }catch(err){

  }
};

kony.chatbotPresentation.prototype.payCreditCard = function(text,action,amount){
  try{
    this.displayBotText(text);
    (new kony.chatbot.command()).payCreditCard(amount);
  }catch(err){

  }
};

kony.chatbotPresentation.prototype.callCustomerService = function(text){
  try{
    this.displayBotText(text);
  }catch(err){

  }
};

kony.chatbotPresentation.prototype.showTransactions = function(text,accountType){
  try{
    this.displayBotText(text);
    (new kony.chatbot.command()).showTransactions(accountType);
  }catch(err){
    kony.print(err);
  }
};

kony.chatbotPresentation.prototype.payPerson = function(message,fromAccount,amountPay){
  try{
    this.displayBotText(message);
    (new kony.chatbots.command()).payPerson(message,fromAccount,amountPay);
  }catch(err){
    alert(err);
  }
};

kony.chatbotPresentation.prototype.onExitChatBot = function(){
  try{
    kony.chatbots.user.onClickClose();
    this.predictString(null,'exit');
  }catch(err){

  }
};

kony.chatbotPresentation.prototype.onClickOfOption = function(option,text){
  try{
    var userText = text;
    kony.chatbots.user.addMessage(userText);
    kony.chatbots.user.addTime();
    kony.chatbots.bot.showLoading();
    kony.chatbotPresentation.lastTyped = "user";
    this.predictString(null,text);
  }catch(err){
    kony.print(err);
  }
};

kony.chatbotPresentation.prototype.handleBotServerConncetionFailure = function(callback){
  try{
    if(kony.application.getCurrentForm().id === "frmBotSplash"){
      kony.chatbotPresentation.isFirstTimeOpened = true;
      kony.chatbotPresentation.botOffline = false;
      kony.chatbotPresentation.lastTyped = null;
      kony.chatbotPresentation.nearByAtmsData = null;
      var previousForm = kony.chatbots.bot.previousForm;
      previousForm.show();
      toastMsg.showToastMsg("Unable to connect to the Server please try again later",5000);
    }
    else{
      var msg = {"reply" : ["unable to connect to server please try again later!"]};
      callback(msg); 
    }
  }catch(err){

  }
};