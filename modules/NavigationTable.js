kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.navigationTable =  function(){
    this.items ={};
};

kony.rb.navigationTable.prototype.setContextItem = function(key, value){
    var previous = undefined;
    if (this.hasContextItem(key)) {
        previous = this.items[key];
    }
    this.items[key] = value;
    return previous;
};

kony.rb.navigationTable.prototype.getContextItem = function(key){
    return this.hasContextItem(key) ? this.items[key] : undefined;
};

kony.rb.navigationTable.prototype.hasContextItem = function(key){
    return this.items.hasOwnProperty(key);
};
   
kony.rb.navigationTable.prototype.removeContextItem = function(key){
    if (this.hasContextItem(key)){
        previous = this.items[key];
        delete this.items[key];
        return previous;
    }
    else {
        return undefined;
    }
};
    
kony.rb.navigationTable.prototype.clear = function(){
    this.items = {};
};

