var frmInformationKAConfig = {
    "formid": "frmInformationKA",
    "frmInformationKA": {
        "entity": "Informationcontent",
        "objectServiceName": "RBObjects",
        "objectServiceOptions" : {"access":"online"},
    },
    "segOffersKA": {
        "fieldprops": {
            "widgettype": "Segment",
            "entity": "Informationcontent",
            "additionalFields": ["categoryId","categoryImageId"],
            "field": {
                "lblHeader": {
                    "widgettype": "Label",
                    "field" : "categoryTitle"
                },
              "lblDescription": {
                "widgettype": "Label",
                "field" : "categoryDescription"
              },
              "imgNews" : {
                 "widgettype": "Image",
                 "field" : "categoryImage"
              }
            }
        }
    } ,
    "segFaq": {
        "fieldprops": {
          "widgettype": "Segment",
            "entity": "Informationcontent",
            "field": {
                "lblQuestion": {
                    "widgettype": "Label",
                    "field" : "question"
                },
              "lblAnswer": {
                "widgettype": "Label",
                "field" : "answer"
              }
              
            }
        }
    }
};