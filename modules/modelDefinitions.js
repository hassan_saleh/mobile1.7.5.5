function Account(aId, aName, aType, aBal, aPoints, bName, ccNo, cCode, cBal, dID, dDate, eMsg, iRate, iPFM, lsBal, mDate, mDue,
                         nName, oDate, oBal, pTerm, pVal, succ, sBPay,sCWdraw, sDep, sTFrom, sTTo, transLimit, xferLimit, uName,rNo) {
         kony.print("Account: Inside constructor");                
         this.accountID=aId;
         this.accountName=aName;
         this.accountType=aType;
         this.availableBalance=aBal;
         this.availablePoints=aPoints;
         this.bankName=bName;
         this.creditCardNumber=ccNo;
         this.currencyCode=cCode;
         this.currentBalance=cBal; 
         this.deviceID=dID;
         this.dueDate=dDate;
         this.errmsg=eMsg;
         this.interestRate=iRate;
         this.isPFM=iPFM; 
         this.lastStatementBalance=lsBal;
         this.maturityDate=mDate;
         this.minimumDue=mDue;
         this.nickName=nName;
         this.openingDate=oDate;
         this.outstandingBalance=oBal; 
         this.paymentTerm=pTerm;
         this.principalValue=pVal;
         this.success=succ;
         this.supportBillPay=sBPay; 
         this.supportCardlessCash=sCWdraw;
         this.supportDeposit=sDep;    
         this.supportTransferFrom=sTFrom;
         this.supportTransferTo=sTTo;
         this.transactionLimit=transLimit;
         this.transferLimit=xferLimit; 
         this.userName=uName;  
         this.bsbNum=rNo;
}

function Transaction(aId, accNo, amt, cImg, cImgBack, cno, desc, emsg, eAccNo, fRecNum, fEndDate, fStartDate, fType, fromAccBal, fromAccName,
                        fromAccNo, fromAcctype, formCheckNo, fNickName, hDepImg, iScheduled, lRecordNum, numRecur, payeeId, pNickName, pPemail, pPName, 
                        pPPhone, pId, rId, sDate, sAmt, sDateRange, sDesc, sEndDate, sMaxAmt, sMinAmt, sStartDate, sTransType, sType,
                        sStatusDesc, succ, tAccName, tAccNo, tAccType, tChkNo, tComments, tDate, tId, tNotes, tType, otp, validDate, cMode, cPersonName, cStatus, sCode, Email, Phone, cashlessOTP, OTPValidDate) {
         kony.print("Transaction: Inside constructor");                
         this.accountID=aId;
         this.accountNumber=accNo;
         this.amount=amt;
         this.checkImage=cImg;
         this.checkImageBack=cImgBack;
         this.checkNumber=cno;
         this.description=desc;
         this.errmsg=emsg;
         this.ExternalAccountNumber=eAccNo; 
         this.firstRecordNumber=fRecNum;
         this.frequencyEndDate=fEndDate;
         this.frequencyStartDate=fStartDate;
         this.frequencyType=fType; 
         this.fromAccountBalance=fromAccBal;
         this.fromAccountName=fromAccName;
         this.fromAccountNumber=fromAccNo;
         this.fromAccountType=fromAcctype;
         this.fromCheckNumber=formCheckNo;
         this.fromNickName=fNickName;
         this.hasDepositImage=hDepImg; 
         this.isScheduled=iScheduled;
         this.lastRecordNumber=lRecordNum;
         this.numberOfRecurrences=numRecur;
         this.payeeId=payeeId; 
         this.payeeNickName=pNickName;    
         this.payPersonEmail=pPemail;
         this.payPersonName=pPName;
         this.payPersonPhone=pPPhone;
         this.personId=pId; 
         this.referenceId=rId;         
         this.scheduledDate=sDate;
         this.searchAmount=sAmt;
         this.searchDateRange=sDateRange;
         this.searchDescription=sDesc; 
         this.searchEndDate=sEndDate; 
         this.searchMaxAmount=sMaxAmt;
         this.searchMinAmount=sMinAmt;
         this.searchStartDate=sStartDate;
         this.searchTransactionType=sTransType; 
         this.searchType=sType; 
         this.statusDescription=sStatusDesc; 
         this.success=succ; 
         this.toAccountName=tAccName; 
         this.toAccountNumber=tAccNo;
         this.toAccountType=tAccType;
         this.toCheckNumber=tChkNo;
         this.transactionComments=tComments; 
         this.transactionDate=tDate; 
         this.transactionId=tId; 
         this.transactionsNotes=tNotes;     
         this.transactionType=tType;
  		 this.otp=otp;
  		 this.validDate=validDate;
  		 this.cashlessMode=cMode;
  		 this.cashlessPersonName=cPersonName;
  		 this.cashWithdrawalTransactionStatus=cStatus;
  		 this.cashlessSecurityCode=sCode;
 		 this.cashlessEmail=Email;
  		 this.cashlessPhone=Phone;
   		 this.cashlessOTP=cashlessOTP;
  		 this.cashlessOTPValidDate=OTPValidDate;
}

function Ads(adId, adType, adDescription, adImageURL, adAction1, adAction2, adTitle, adImageURL2, adActionType) {
         kony.print("Advertisements: Inside constructor");                
         this.adId=adId;
         this.adType=adType;
         this.adDescription=adDescription;
         this.adImageURL=adImageURL;
         this.adAction1=adAction1;
         this.adAction2=adAction2;
         this.adTitle=adTitle;
		 this.adImageURL2 = adImageURL2;
  		 this.adActionType = adActionType;
}

function QrCode(AtmId,id,timestamp){
  		 kony.print("QrCode: Inside constructor");                
         this.AtmId=AtmId;
         this.id=id;
         this.timestamp=timestamp;
  
}

function p2pRegistration(accountId,displayName,email,errmsg,isNpp,isZell,p2pRegId,phone,success){
  		 kony.print("p2pRegistration: Inside constructor");                
         this.accountId=accountId;
         this.displayName=displayName;
         this.email=email;
  		 this.errmsg=errmsg;
  		 this.isNpp=isNpp;
  		 this.isZell=isZell;
  		 this.p2pRegId=p2pRegId;
  		 this.phone=phone;
  		 this.success=success;
}

function NewUserProducts(features,productId,rates,productDescription,productType,info,termsAndConditions,productImageURL,productName)
{
    kony.print("NewUserProducts: Inside constructor");
    this.features=features;
    this.productId=productId;
    this.rates=rates;
    this.productDescription=productDescription;
    this.info=info;
    this.termsAndConditions=termsAndConditions;
    this.productImageURL=productImageURL;
    this.productName=productName;
    this.productType=productType;

}
function NewUserSecurityQuestions(question,QuestionId)
{
  kony.print("NewUserSecurityQuestions: Inside constructor");
  this.question=question;
  this.QuestionId=QuestionId; 
}