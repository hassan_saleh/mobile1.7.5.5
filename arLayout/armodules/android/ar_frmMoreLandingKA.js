//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:36 EET 2020
function addWidgetsfrmMoreLandingKAAr() {
    frmMoreLandingKA.setDefaultUnit(kony.flex.DP);
    var moreScrollContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "moreScrollContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "onScrolling": AS_FlexScrollContainer_5744197d64f64bf4a8a994cfc51b1885,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknslFSbox",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    moreScrollContainer.setDefaultUnit(kony.flex.DP);
    var bottomContainerr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "65%",
        "id": "bottomContainerr",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknCopyslFbox019f84c8a6c5541",
        "top": "37%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    bottomContainerr.setDefaultUnit(kony.flex.DP);
    bottomContainerr.add();
    var flxOuterBottonContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75%",
        "id": "flxOuterBottonContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOuterBottonContainerKA.setDefaultUnit(kony.flex.DP);
    var bottomContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "bottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknCopyslFSbox013d08d6060bd4b",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    bottomContainer.setDefaultUnit(kony.flex.DP);
    var moreResourcesSegment = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 0,
        "data": [{
            "HiddenLbl": "",
            "imgicontick": "",
            "lblPageNameKA": ""
        }],
        "groupCells": false,
        "id": "moreResourcesSegment",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_9032cf7905794b79a7870c2ca9f51c2d,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": container,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "f7f7f700",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "container": "container",
            "imgicontick": "imgicontick",
            "lblPageNameKA": "lblPageNameKA"
        },
        "width": "100%"
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var segmentBorderBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "segmentBorderBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "-1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    segmentBorderBottom.setDefaultUnit(kony.flex.DP);
    segmentBorderBottom.add();
    bottomContainer.add(moreResourcesSegment, segmentBorderBottom);
    flxOuterBottonContainerKA.add(bottomContainer);
    moreScrollContainer.add(bottomContainerr, flxOuterBottonContainerKA);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 12
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.more.resources"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var hamburgerButton = new kony.ui.Button({
        "focusSkin": "sknhamburgerButtonFocus",
        "height": "50dp",
        "id": "hamburgerButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_b161b6a6ea274cb4b67494c8010b702a,
        "skin": "sknhamburgerButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, hamburgerButton);
    frmMoreLandingKA.add(moreScrollContainer, androidTitleBar);
};
function frmMoreLandingKAGlobalsAr() {
    frmMoreLandingKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMoreLandingKAAr,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "footers": [footerBack],
        "id": "frmMoreLandingKA",
        "init": AS_Form_27c15e07f1dd42489132334199d90467,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_156ebd28571f47dfa876957728039d2a,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_5db481a4da8d4c21b1cbb01e57abbd24,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
