//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmChequeStatusAr() {
frmChequeStatus.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "2%",
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"right": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"right": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add( lblBack,lblBackIcon);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": "Cheque Status Title",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeader.add(flxBack, lblTitle);
var flxBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "25%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxTextBox = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60%",
"id": "flxTextBox",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTextBox.setDefaultUnit(kony.flex.DP);
var txtChequeBox = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "txtChequeBox",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"secureTextEntry": false,
"skin": "sknTxtBox90",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblSiriTransLimit = new kony.ui.Label({
"id": "lblSiriTransLimit",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "Enter cheque number",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var borderBottom1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "borderBottom1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "95%",
"zIndex": 2
}, {}, {});
borderBottom1.setDefaultUnit(kony.flex.DP);
borderBottom1.add();
flxTextBox.add(txtChequeBox, lblSiriTransLimit, borderBottom1);
var btnsearch = new kony.ui.Button({
"bottom": "2%",
"focusSkin": "slButtonWhite",
"height": "30%",
"id": "btnsearch",
"isVisible": true,
"right": "25%",
"skin": "slButtonWhite",
"text": "Check",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyborderBottom0c03ba52725ee46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "CopyborderBottom0c03ba52725ee46",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0c03ba52725ee46.setDefaultUnit(kony.flex.DP);
CopyborderBottom0c03ba52725ee46.add();
flxTop.add(flxTextBox, btnsearch, CopyborderBottom0c03ba52725ee46);
var flxBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "74%",
"id": "flxBottom",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "26%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBottom.setDefaultUnit(kony.flex.DP);
var CopyborderBottom0f8d45f82ebf84d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "1%",
"id": "CopyborderBottom0f8d45f82ebf84d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"skin": "skntextFieldDivider",
"top": "4%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0f8d45f82ebf84d.setDefaultUnit(kony.flex.DP);
CopyborderBottom0f8d45f82ebf84d.add();
var lblDeatils = new kony.ui.Label({
"id": "lblDeatils",
"isVisible": true,
"right": "0%",
"skin": "sknCaiRegWhite50Op",
"text": "Details",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyborderBottom0f5b8d8a6822941 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "1%",
"id": "CopyborderBottom0f5b8d8a6822941",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"skin": "skntextFieldDivider",
"top": "1%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0f5b8d8a6822941.setDefaultUnit(kony.flex.DP);
CopyborderBottom0f5b8d8a6822941.add();
var FlexContainer0e856e0cc884740 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "FlexContainer0e856e0cc884740",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "1%",
"skin": "slFbox",
"top": "10%",
"width": "98%",
"zIndex": 1
}, {}, {});
FlexContainer0e856e0cc884740.setDefaultUnit(kony.flex.DP);
var lbl1 = new kony.ui.Label({
"height": "100%",
"id": "lbl1",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "Check Status : ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copylbl0ifbb4108097b4f = new kony.ui.Label({
"height": "100%",
"id": "Copylbl0ifbb4108097b4f",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "Active / Inactive",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer0e856e0cc884740.add( Copylbl0ifbb4108097b4f,lbl1);
var CopyFlexContainer0a3a32680281c43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "CopyFlexContainer0a3a32680281c43",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "1%",
"skin": "slFbox",
"top": "2%",
"width": "98%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a3a32680281c43.setDefaultUnit(kony.flex.DP);
var Copylbl0ad09625def7444 = new kony.ui.Label({
"height": "100%",
"id": "Copylbl0ad09625def7444",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "Cheque Date : ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copylbl0g3c4dac98edc4b = new kony.ui.Label({
"height": "100%",
"id": "Copylbl0g3c4dac98edc4b",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "10/10/2019",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyFlexContainer0a3a32680281c43.add( Copylbl0g3c4dac98edc4b,Copylbl0ad09625def7444);
var CopyFlexContainer0f04408b4954648 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "CopyFlexContainer0f04408b4954648",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "1%",
"skin": "slFbox",
"top": "2%",
"width": "98%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0f04408b4954648.setDefaultUnit(kony.flex.DP);
var Copylbl0f336cf44dba94e = new kony.ui.Label({
"height": "100%",
"id": "Copylbl0f336cf44dba94e",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "Cheque Amount : ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copylbl0eca32b5ec35d47 = new kony.ui.Label({
"height": "100%",
"id": "Copylbl0eca32b5ec35d47",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "20,090.909 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyFlexContainer0f04408b4954648.add( Copylbl0eca32b5ec35d47,Copylbl0f336cf44dba94e);
var CopyFlexContainer0ie41a05dfa6947 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "CopyFlexContainer0ie41a05dfa6947",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "1%",
"skin": "slFbox",
"top": "2%",
"width": "98%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0ie41a05dfa6947.setDefaultUnit(kony.flex.DP);
var Copylbl0ee75b554d8ba4c = new kony.ui.Label({
"height": "100%",
"id": "Copylbl0ee75b554d8ba4c",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "Cheque Image  : ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copylbl0c7c936c0895749 = new kony.ui.Label({
"height": "100%",
"id": "Copylbl0c7c936c0895749",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "Not available",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyFlexContainer0ie41a05dfa6947.add( Copylbl0c7c936c0895749,Copylbl0ee75b554d8ba4c);
flxBottom.add(CopyborderBottom0f8d45f82ebf84d, lblDeatils, CopyborderBottom0f5b8d8a6822941, FlexContainer0e856e0cc884740, CopyFlexContainer0a3a32680281c43, CopyFlexContainer0f04408b4954648, CopyFlexContainer0ie41a05dfa6947);
flxBody.add(flxTop, flxBottom);
frmChequeStatus.add(flxHeader, flxBody);
};
function frmChequeStatusGlobalsAr() {
frmChequeStatusAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmChequeStatusAr,
"bounces": false,
"enabledForIdleTimeout": false,
"id": "frmChequeStatus",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "slFormCommon",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
