//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmUserSettingsSIRILIMITAr() {
frmUserSettingsSIRILIMIT.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var lblTouchIdHeader = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblTouchIdHeader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": "SIRI Transaction limit",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_c55ace37fb2244ea9d4e1dfa203295ef,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBacktext = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "lblBacktext",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBacktext);
flxHeader.add(lblTouchIdHeader, flxBack);
var flxBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var FlxEmail1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "FlxEmail1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "6%",
"width": "90%",
"zIndex": 1
}, {}, {});
FlxEmail1.setDefaultUnit(kony.flex.PERCENTAGE);
var txtEmail1 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtEmail1",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"maxTextLength": 9,
"onDone": AS_TextField_ab7969a1ba47470c83b2dc29cd982d32,
"onTextChange": AS_TextField_f634d333d2024495867c4278bf47174e,
"onTouchEnd": AS_TextField_acd5740226ed4651a9fbd9eeb3b817cf,
"onTouchStart": AS_TextField_cfd0f0b445de4583b335c4cf08dde1a7,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 10
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": false,
"showCloseButton": true,
"showProgressIndicator": false,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var borderBottom1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "borderBottom1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "95%",
"zIndex": 2
}, {}, {});
borderBottom1.setDefaultUnit(kony.flex.DP);
borderBottom1.add();
var lblEmail1 = new kony.ui.Label({
"id": "lblEmail1",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": "SIRI Transaction limit",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblClose1 = new kony.ui.Label({
"height": "100%",
"id": "lblClose1",
"isVisible": false,
"onTouchStart": AS_Label_hd1f0f2fbe2e4abba0ea8751f502c345,
"left": "2%",
"skin": "sknClose",
"text": "O",
"top": "10%",
"width": "10%",
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxEmail1.add(txtEmail1, borderBottom1, lblEmail1, lblClose1);
var btnConfirmEstmt = new kony.ui.Button({
"bottom": "5%",
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "9%",
"id": "btnConfirmEstmt",
"isVisible": true,
"onClick": AS_Button_a05545a85af8487780ae0ef2d020a2c7,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Estmt.Confirm"),
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxBody.add(FlxEmail1, btnConfirmEstmt);
frmUserSettingsSIRILIMIT.add(flxHeader, flxBody);
};
function frmUserSettingsSIRILIMITGlobalsAr() {
frmUserSettingsSIRILIMITAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmUserSettingsSIRILIMITAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmUserSettingsSIRILIMIT",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_i702cc9893124605b7e3f53f4fb55058,
"preShow": AS_Form_g8eafbd494224811b1dd897d98fd2558,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"bouncesZoom": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
