//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:02 EET 2020
function addWidgetsfrmConfirmCashWithDrawQRCodeKAAr() {
    frmConfirmCashWithDrawQRCodeKA.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var confirmWithdrawTitleLabel = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "confirmWithdrawTitleLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.confirmWithdraw"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    iosTitleBar.add(confirmWithdrawTitleLabel);
    var backButton = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "skntitleBarTextButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "minWidth": "50dp",
        "onClick": AS_Button_ae96c22cf6314c8ea1ab4e3f2a148976,
        "skin": "skntitleBarTextButton",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.cancel"),
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,2, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    titleBarWrapper.add(iosTitleBar, backButton);
    var FlexScrollContainerConfirmWithdraw = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": false,
        "id": "FlexScrollContainerConfirmWithdraw",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexScrollContainerConfirmWithdraw.setDefaultUnit(kony.flex.DP);
    var FlexContainerWithdrawDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "115dp",
        "id": "FlexContainerWithdrawDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainerWithdrawDetails.setDefaultUnit(kony.flex.DP);
    var innerFlexContainerWithdrawDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "innerFlexContainerWithdrawDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    innerFlexContainerWithdrawDetails.setDefaultUnit(kony.flex.DP);
    var withdrawAmount = new kony.ui.Label({
        "centerX": "50%",
        "id": "withdrawAmount",
        "isVisible": true,
        "skin": "skndetailPageNumber",
        "text": "$ 5006.00",
        "top": "10dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var forCollectionLabel = new kony.ui.Label({
        "centerX": "50%",
        "id": "forCollectionLabel",
        "isVisible": true,
        "skin": "skndetailPageDate",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.cashWithdrawalby"),
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var collectorName = new kony.ui.Label({
        "centerX": "50%",
        "id": "collectorName",
        "isVisible": true,
        "skin": "skn30363f110KA",
        "text": "Self",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var divider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider1.setDefaultUnit(kony.flex.DP);
    divider1.add();
    innerFlexContainerWithdrawDetails.add(withdrawAmount, forCollectionLabel, collectorName, divider1);
    FlexContainerWithdrawDetails.add(innerFlexContainerWithdrawDetails);
    var fromFlxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "fromFlxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    fromFlxDetails.setDefaultUnit(kony.flex.DP);
    var fromLabel = new kony.ui.Label({
        "id": "fromLabel",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var withdrawFrom = new kony.ui.Label({
        "id": "withdrawFrom",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": "Savings 2453",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var divider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider2.setDefaultUnit(kony.flex.DP);
    divider2.add();
    fromFlxDetails.add(fromLabel, withdrawFrom, divider2);
    var flexNotesDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexNotesDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexNotesDetails.setDefaultUnit(kony.flex.DP);
    var lblNotes = new kony.ui.Label({
        "id": "lblNotes",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.notes"),
        "top": "13dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionNotes = new kony.ui.Label({
        "bottom": "15dp",
        "id": "transactionNotes",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": "Note details appear here. This can be a muliple line descripsion",
        "top": "5dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var divider3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider3.setDefaultUnit(kony.flex.DP);
    divider3.add();
    flexNotesDetails.add(lblNotes, transactionNotes, divider3);
    var FlexContainerConfirmandEditBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10%",
        "clipBounds": true,
        "height": "260dp",
        "id": "FlexContainerConfirmandEditBtns",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainerConfirmandEditBtns.setDefaultUnit(kony.flex.DP);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_i301a708423249a09abd211a8a72cac0,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.confirm"),
        "top": "15%",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnEditTranscation = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "35dp",
        "id": "btnEditTranscation",
        "isVisible": true,
        "onClick": AS_Button_d211ec7638604dd8ac10babfc6a22381,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.edit"),
        "top": "5dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    FlexContainerConfirmandEditBtns.add(btnConfirm, btnEditTranscation);
    FlexScrollContainerConfirmWithdraw.add(FlexContainerWithdrawDetails, fromFlxDetails, flexNotesDetails, FlexContainerConfirmandEditBtns);
    frmConfirmCashWithDrawQRCodeKA.add(titleBarWrapper, FlexScrollContainerConfirmWithdraw);
};
function frmConfirmCashWithDrawQRCodeKAGlobalsAr() {
    frmConfirmCashWithDrawQRCodeKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmConfirmCashWithDrawQRCodeKAAr,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmConfirmCashWithDrawQRCodeKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "preShow": AS_Form_a1b411b9737e417f8bdfcacd3d97ba1b,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false
    });
};
