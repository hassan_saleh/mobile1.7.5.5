//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializesegBulkPaymentConfirmationAr() {
    flxBulkPaymentConfirmAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "22%",
        "id": "flxBulkPaymentConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxBulkPaymentConfirmAr.setDefaultUnit(kony.flex.DP);
    var flxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDetails.setDefaultUnit(kony.flex.DP);
    var payeename = new kony.ui.Label({
        "centerY": "11%",
        "id": "payeename",
        "isVisible": true,
        "right": "4%",
        "skin": "sknLblNextDisabled",
        "text": "wertyui",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var payeenickname = new kony.ui.Label({
        "centerY": "56.35%",
        "height": "20dp",
        "id": "payeenickname",
        "isVisible": false,
        "right": "7%",
        "skin": "sknRegisterMobileBank",
        "text": "iuytrs",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var accountnumber = new kony.ui.Label({
        "id": "accountnumber",
        "isVisible": true,
        "right": "4%",
        "skin": "sknLblAccNumBiller",
        "text": "09876543234567",
        "top": "15%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var dueAmount = new kony.ui.Label({
        "id": "dueAmount",
        "isVisible": true,
        "right": "4%",
        "skin": "sknLblAccNumBiller",
        "text": "asdfghj98765432",
        "top": "30%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var hiddenDueAmount = new kony.ui.Label({
        "centerY": "75%",
        "height": "30%",
        "id": "hiddenDueAmount",
        "isVisible": false,
        "right": "4%",
        "skin": "sknLblAccNumBiller",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxPaidAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxPaidAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "4%",
        "skin": "slFbox",
        "top": "47%",
        "width": "92%",
        "zIndex": 1
    }, {}, {});
    flxPaidAmount.setDefaultUnit(kony.flex.DP);
    var paidAmount = new kony.ui.Label({
        "height": "100%",
        "id": "paidAmount",
        "isVisible": true,
        "right": "0%",
        "skin": "sknLblAccNumBiller",
        "text": "Refference Number:\najksdfhaklsjfhasdjklhfahfksdhfhasdkl",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblpaidAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblpaidAmount",
        "isVisible": false,
        "right": "0%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.paidamount"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var txtpaidAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "focusSkin": "sknTxtBox90",
        "id": "txtpaidAmount",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "right": "34%",
        "secureTextEntry": false,
        "skin": "sknTxtBox90",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxPaidAmount.add(paidAmount, lblpaidAmount, txtpaidAmount);
    flxDetails.add(payeename, payeenickname, accountnumber, dueAmount, hiddenDueAmount, flxPaidAmount);
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    flxBulkPaymentConfirmAr.add(flxDetails, contactListDivider);
}
