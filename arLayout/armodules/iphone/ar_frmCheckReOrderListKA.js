//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:02 EET 2020
function addWidgetsfrmCheckReOrderListKAAr() {
    frmCheckReOrderListKA.setDefaultUnit(kony.flex.DP);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "50dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var contactsegment = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "CopytransactionName0bfeb2626799d46": "Delivered",
            "chevron": "right_chevron_icon.png",
            "lblNumberOfLeafletsKA": "Leaflets: 20",
            "transactionDate": "Oct 10, 2015",
            "transactionName": "Payment to City of Austin"
        }, {
            "CopytransactionName0bfeb2626799d46": "Delivered",
            "chevron": "right_chevron_icon.png",
            "lblNumberOfLeafletsKA": "Leaflets: 20",
            "transactionDate": "Oct 10, 2015",
            "transactionName": "Payment to City of Austin"
        }, {
            "CopytransactionName0bfeb2626799d46": "Delivered",
            "chevron": "right_chevron_icon.png",
            "lblNumberOfLeafletsKA": "Leaflets: 20",
            "transactionDate": "Oct 10, 2015",
            "transactionName": "Payment to City of Austin"
        }, {
            "CopytransactionName0bfeb2626799d46": "Delivered",
            "chevron": "right_chevron_icon.png",
            "lblNumberOfLeafletsKA": "Leaflets: 20",
            "transactionDate": "Oct 10, 2015",
            "transactionName": "Payment to City of Austin"
        }, {
            "CopytransactionName0bfeb2626799d46": "Delivered",
            "chevron": "right_chevron_icon.png",
            "lblNumberOfLeafletsKA": "Leaflets: 20",
            "transactionDate": "Oct 10, 2015",
            "transactionName": "Payment to City of Austin"
        }, {
            "CopytransactionName0bfeb2626799d46": "Delivered",
            "chevron": "right_chevron_icon.png",
            "lblNumberOfLeafletsKA": "Leaflets: 20",
            "transactionDate": "Oct 10, 2015",
            "transactionName": "Payment to City of Austin"
        }],
        "groupCells": false,
        "id": "contactsegment",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_6246333d6f2b45ab98c2149c1526b216,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFlexContainer02f94a2abf98249,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0.00%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyFlexContainer02f94a2abf98249": "CopyFlexContainer02f94a2abf98249",
            "CopytransactionName0bfeb2626799d46": "CopytransactionName0bfeb2626799d46",
            "FlexContainer01ff055a4331e4f": "FlexContainer01ff055a4331e4f",
            "chevron": "chevron",
            "contactListDivider": "contactListDivider",
            "flxImageandNameKA": "flxImageandNameKA",
            "lblNumberOfLeafletsKA": "lblNumberOfLeafletsKA",
            "transactionDate": "transactionDate",
            "transactionName": "transactionName"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_SWIPE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var LabelNoRecordsKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "35.519999999999996%",
        "id": "LabelNoRecordsKA",
        "isVisible": false,
        "right": "17.52%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.checkReOrder.NoRecords"),
        "top": "30dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyFlexContainer0b2b1c26ffbf74f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "5dp",
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyFlexContainer0b2b1c26ffbf74f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0",
        "onTouchEnd": AS_FlexContainer_53011a6c48e6463d954800a8a9f43356,
        "skin": "sknCopyslFbox07d05709853a74d",
        "width": "100%"
    }, {}, {});
    CopyFlexContainer0b2b1c26ffbf74f.setDefaultUnit(kony.flex.DP);
    var lblNameKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNameKA",
        "isVisible": true,
        "right": "5%",
        "skin": "skn",
        "text": kony.i18n.getLocalizedString("i18n.checkReOrder.CallEnquires"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgicontick = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "imgicontick",
        "isVisible": true,
        "left": "5%",
        "skin": "sknslImage",
        "src": "phone_icon.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0b2b1c26ffbf74f.add(lblNameKA, imgicontick);
    var segmentBorderBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "segmentBorderBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    segmentBorderBottom.setDefaultUnit(kony.flex.DP);
    segmentBorderBottom.add();
    mainContent.add(contactsegment, LabelNoRecordsKA, CopyFlexContainer0b2b1c26ffbf74f, segmentBorderBottom);
    var titleBarAccountInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarAccountInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarAccountInfo.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var lblTitleIosKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTitleIosKA",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": "Check Re-Order",
        "top": "15dp",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_835c3f2362ef4449b24f24e27dcf4f1f,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    iosTitleBar.add(lblTitleIosKA, backButton);
    var btnNewKA = new kony.ui.Button({
        "focusSkin": "sknbtn",
        "height": "50dp",
        "id": "btnNewKA",
        "isVisible": true,
        "onClick": AS_Button_7b265671a73b49e8ab6edcc6c7a87b28,
        "left": "10dp",
        "skin": "sknbtn",
        "text": kony.i18n.getLocalizedString("i18n.checkReOrder.New"),
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    titleBarAccountInfo.add(iosTitleBar, btnNewKA);
    frmCheckReOrderListKA.add(mainContent, titleBarAccountInfo);
};
function frmCheckReOrderListKAGlobalsAr() {
    frmCheckReOrderListKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCheckReOrderListKAAr,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmCheckReOrderListKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false
    });
};
