function initializetmpAddressSearchResults() {
    flxAddressSearchResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxAddressSearchResults",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxAddressSearchResults.setDefaultUnit(kony.flex.DP);
    var lblSearchResults = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSearchResults",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblF9c9c9c",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lineFirstNameKA = new kony.ui.Label({
        "bottom": "1%",
        "centerX": "50%",
        "height": "1dp",
        "id": "lineFirstNameKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLineEDEDEDKA",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAddressSearchResults.add(lblSearchResults, lineFirstNameKA);
}