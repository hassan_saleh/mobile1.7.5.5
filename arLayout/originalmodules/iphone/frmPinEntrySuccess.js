function addWidgetsfrmPinEntrySuccess() {
    frmPinEntrySuccess.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxHeaderKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeaderKA.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.00%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "skin": "slFbox",
        "top": "0dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    flxHeaderContainerKA.setDefaultUnit(kony.flex.DP);
    var imgAuthMode1KA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgAuthMode1KA",
        "isVisible": true,
        "left": "0%",
        "skin": "slImage",
        "src": "touchiconactive.png",
        "width": "45dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxLine1KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "3.50%",
        "id": "flxLine1KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-2dp",
        "skin": "sknFlxLine",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    flxLine1KA.setDefaultUnit(kony.flex.DP);
    flxLine1KA.add();
    var imgAuthMode2KA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgAuthMode2KA",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "pin.png",
        "width": "45dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxLine2KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "3.50%",
        "id": "flxLine2KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-3dp",
        "skin": "sknFlxLine",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    flxLine2KA.setDefaultUnit(kony.flex.DP);
    flxLine2KA.add();
    var imgAuthMode3KA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgAuthMode3KA",
        "isVisible": true,
        "left": "-6dp",
        "skin": "slImage",
        "src": "face.png",
        "width": "45dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderContainerKA.add(imgAuthMode1KA, flxLine1KA, imgAuthMode2KA, flxLine2KA, imgAuthMode3KA);
    flxHeaderKA.add(flxHeaderContainerKA);
    var lblSetLoginPIN = new kony.ui.Label({
        "centerX": "50%",
        "height": "30dp",
        "id": "lblSetLoginPIN",
        "isVisible": true,
        "left": "0%",
        "skin": "sknonboardingHeader",
        "text": "Set Login PIN",
        "top": "50dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbldesc = new kony.ui.Label({
        "centerX": "50.00%",
        "height": "28dp",
        "id": "lbldesc",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknonboardingText",
        "text": "xyzBank supports PIN authentication for",
        "top": "3.00%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbldesc2 = new kony.ui.Label({
        "centerX": "50%",
        "height": "28dp",
        "id": "lbldesc2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknonboardingText",
        "text": "secure and conveinent sign in.",
        "top": "1%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgsetPIN = new kony.ui.Image2({
        "centerX": "50%",
        "height": "35%",
        "id": "imgsetPIN",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "pin.png",
        "top": "10dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(flxHeaderKA, lblSetLoginPIN, lbldesc, lbldesc2, imgsetPIN);
    var flxFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "flxFooter",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "55%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFooter.setDefaultUnit(kony.flex.DP);
    var flxPINSuccess = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPINSuccess",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPINSuccess.setDefaultUnit(kony.flex.DP);
    var flxPINCreated = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxPINCreated",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxPINCreated.setDefaultUnit(kony.flex.DP);
    var successIcon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "22dp",
        "id": "successIcon",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "29%",
        "skin": "sknsuccessIcon",
        "top": 7,
        "width": "22dp",
        "zIndex": 1
    }, {}, {});
    successIcon.setDefaultUnit(kony.flex.DP);
    var Image07596c1afc4b545 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "Image07596c1afc4b545",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    successIcon.add(Image07596c1afc4b545);
    var lblPINCreated = new kony.ui.Label({
        "id": "lblPINCreated",
        "isVisible": true,
        "left": "4dp",
        "skin": "sknonboardingHeader",
        "text": "PIN CREATED",
        "top": "7dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxPINCreated.add(successIcon, lblPINCreated);
    var lblFooterdesc3 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblFooterdesc3",
        "isVisible": true,
        "skin": "sknonboardingText",
        "text": "You can login with PIN next time you sign in.",
        "top": "36dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnContinueSetPin = new kony.ui.Button({
        "bottom": "19%",
        "centerX": "50%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "36dp",
        "id": "btnContinueSetPin",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_f4e2edffcb224b01ba9a256647cd6e77,
        "skin": "sknprimaryAction",
        "text": "CONTINUE...",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxPINSuccess.add(flxPINCreated, lblFooterdesc3, btnContinueSetPin);
    flxFooter.add(flxPINSuccess);
    frmPinEntrySuccess.add(flxHeader, flxFooter);
};

function frmPinEntrySuccessGlobals() {
    frmPinEntrySuccess = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPinEntrySuccess,
        "enabledForIdleTimeout": true,
        "id": "frmPinEntrySuccess",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_abf87c8358234b52acbab05cda891b10,
        "skin": "sknfrmbkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};