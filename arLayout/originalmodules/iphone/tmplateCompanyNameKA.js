function initializetmplateCompanyNameKA() {
    FlexContainer040c5584262324d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "FlexContainer040c5584262324d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknyourAccountCard"
    }, {}, {});
    FlexContainer040c5584262324d.setDefaultUnit(kony.flex.DP);
    var lblCompanyNameKA = new kony.ui.Label({
        "id": "lblCompanyNameKA",
        "isVisible": true,
        "left": "10dp",
        "skin": "skn",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": "Label",
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlexContainer040c5584262324d.add(lblCompanyNameKA, lblSepKA);
}