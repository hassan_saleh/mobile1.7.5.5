function initializetmpCurrency() {
    tmpFlxCurrency = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "tmpFlxCurrency",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    tmpFlxCurrency.setDefaultUnit(kony.flex.DP);
    var flxCountryIcon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "80%",
        "id": "flxCountryIcon",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "skin": "slFbox",
        "top": "0%",
        "width": "15%",
        "zIndex": 1
    }, {}, {});
    flxCountryIcon.setDefaultUnit(kony.flex.DP);
    var imgCountryIcon = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCountryIcon",
        "isVisible": true,
        "left": "0%",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCountryIcon.add(imgCountryIcon);
    var lblMobileCountry = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMobileCountry",
        "isVisible": false,
        "left": "23%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "72%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCurrencydetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCurrencydetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "23%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "54%",
        "zIndex": 1
    }, {}, {});
    flxCurrencydetails.setDefaultUnit(kony.flex.DP);
    var lblCurrency = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblCurrency",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblCurrency0c4373e11aa8641 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblCurrency0c4373e11aa8641",
        "isVisible": true,
        "left": "1%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.common.open"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDescriptionEn = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblDescriptionEn",
        "isVisible": true,
        "left": "1%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDescriptionAr = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblDescriptionAr",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblDescriptionAr0ca1638a7c3ee4b = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblDescriptionAr0ca1638a7c3ee4b",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.common.close"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDecimal = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblDecimal",
        "isVisible": false,
        "left": "5%",
        "skin": "sknLblWhike125",
        "text": "EUR",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "16dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrCode = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCurrCode",
        "isVisible": false,
        "left": "5%",
        "skin": "sknLblWhike125",
        "text": "EUR",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "16dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCurrencydetails.add(lblCurrency, CopylblCurrency0c4373e11aa8641, lblDescriptionEn, lblDescriptionAr, CopylblDescriptionAr0ca1638a7c3ee4b, lblDecimal, lblCurrCode);
    var imgTickIcon = new kony.ui.Label({
        "height": "100%",
        "id": "imgTickIcon",
        "isVisible": false,
        "left": "80%",
        "skin": "sknBOJttf200NOOPC",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    tmpFlxCurrency.add(flxCountryIcon, lblMobileCountry, flxCurrencydetails, imgTickIcon);
}