function addWidgetsfrmBotSplash() {
    frmBotSplash.setDefaultUnit(kony.flex.DP);
    var flxBotSplashContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBotSplashContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBotSplashContainer.setDefaultUnit(kony.flex.DP);
    var imgBotSplash = new kony.ui.Image2({
        "height": "100%",
        "id": "imgBotSplash",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "botsplash.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBotSplashContainer.add(imgBotSplash);
    frmBotSplash.add(flxBotSplashContainer);
};

function frmBotSplashGlobals() {
    frmBotSplash = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmBotSplash,
        "enabledForIdleTimeout": false,
        "id": "frmBotSplash",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "CopyslForm0g3e71a9fe6d247"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_j7820e0aa76443d08e41530a7eb09e9f,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};