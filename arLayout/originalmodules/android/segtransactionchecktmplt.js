function initializesegtransactionchecktmplt() {
    CopyFlexContainer071e4f27a130443 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "CopyFlexContainer071e4f27a130443",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer071e4f27a130443.setDefaultUnit(kony.flex.DP);
    var transactionDate = new kony.ui.Label({
        "centerY": "27.78%",
        "height": "20dp",
        "id": "transactionDate",
        "isVisible": true,
        "left": "5%",
        "skin": "sknRegisterMobileBank",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerY": "66.52%",
        "height": "20dp",
        "id": "transactionName",
        "isVisible": true,
        "left": "18.52%",
        "skin": "skn383838LatoRegular107KA",
        "top": "30dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "transactionAmount",
        "isVisible": true,
        "right": "26dp",
        "skin": "skn383838LatoRegular107KA",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLineEDEDEDKA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Image052c0a28a15ea4b = new kony.ui.Image2({
        "height": "30dp",
        "id": "Image052c0a28a15ea4b",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "checkf.png",
        "top": "27dp",
        "width": "37dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer071e4f27a130443.add(transactionDate, transactionName, transactionAmount, lblSepKA, Image052c0a28a15ea4b);
}