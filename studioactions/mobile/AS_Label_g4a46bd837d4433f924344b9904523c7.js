function AS_Label_g4a46bd837d4433f924344b9904523c7(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if ((currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") && currentForm.passwordTextField.text !== "") {
        if (frmLoginKA.lblShowPass.text === "B") {
            currentForm.passwordTextField.secureTextEntry = true;
            frmLoginKA.lblShowPass.text = "A";
        } else {
            currentForm.passwordTextField.secureTextEntry = false;
            frmLoginKA.lblShowPass.text = "B";
        }
    }
}