function AS_Button_j94d44de90284f1c912c7119c0400c14(eventobject) {
    function SHOW_ALERT_ide_onClick_ha4c08d9f6474aafb0462999ca6a1bad_True() {
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_ha4c08d9f6474aafb0462999ca6a1bad_False() {}

    function SHOW_ALERT_ide_onClick_ha4c08d9f6474aafb0462999ca6a1bad_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_ha4c08d9f6474aafb0462999ca6a1bad_True();
        } else {
            SHOW_ALERT_ide_onClick_ha4c08d9f6474aafb0462999ca6a1bad_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_ha4c08d9f6474aafb0462999ca6a1bad_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}