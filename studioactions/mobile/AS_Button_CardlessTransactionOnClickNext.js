function AS_Button_CardlessTransactionOnClickNext(eventobject) {
    return AS_Button_bc9c73dd782947a9a224ee682904a362(eventobject);
}

function AS_Button_bc9c73dd782947a9a224ee682904a362(eventobject) {
    if (frmCardlessTransaction.txtFieldAmount.text % 5 !== 0) customAlertPopup(geti18Value("i18n.Cardless.NewCardless"), geti18Value("i18n.Cardless.checkAmount"), popupCommonAlertDimiss, "");
    else toConfirmScreenCardless();
}