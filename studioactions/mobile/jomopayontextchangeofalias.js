function jomopayontextchangeofalias(eventobject, changedtext) {
    return AS_TextField_b98e1e414a9e45049fc396a621618634(eventobject, changedtext);
}

function AS_TextField_b98e1e414a9e45049fc396a621618634(eventobject, changedtext) {
    if (!/^[0-9a-zA-Z\s]+$/.test(frmJoMoPay.txtAliasDetail.text)) frmJoMoPay.txtAliasDetail.text = frmJoMoPay.txtAliasDetail.text.substring(0, frmJoMoPay.txtAliasDetail.text.length - 1);
    if (frmJoMoPay.txtAliasDetail !== null && frmJoMoPay.txtAliasDetail !== "" && frmJoMoPay.txtFieldAmount.text != null && frmJoMoPay.txtFieldAmount.text != "" && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtPhoneNo.text != null && frmJoMoPay.txtPhoneNo.text != "") {
        frmJoMoPay.btnNext.skin = "jomopaynextEnabled";
        frmJoMoPay.btnNext.focusSkin = "jomopaynextEnabled";
    } else {
        frmJoMoPay.btnNext.skin = "jomopaynextDisabled";
        frmJoMoPay.btnNext.focusSkin = "jomopaynextDisabled";
    }
}