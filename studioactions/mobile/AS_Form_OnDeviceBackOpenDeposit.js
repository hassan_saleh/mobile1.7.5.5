function AS_Form_OnDeviceBackOpenDeposit(eventobject) {
    return AS_Form_ee33e12f6fa44b0280c296b3e6273f85(eventobject);
}

function AS_Form_ee33e12f6fa44b0280c296b3e6273f85(eventobject) {
    if (frmOpenTermDeposit.flxConfirmDeposite.isVisible === true) {
        frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
        frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
        frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
    } else {
        resetForm();
        frmAccountsLandingKA.show();
        frmOpenTermDeposit.destroy();
    }
}