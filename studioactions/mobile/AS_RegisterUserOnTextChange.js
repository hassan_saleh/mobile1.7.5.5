function AS_RegisterUserOnTextChange(eventobject, changedtext) {
    return AS_TextField_g8c4558d61504f45929cdd7509164e76(eventobject, changedtext);
}

function AS_TextField_g8c4558d61504f45929cdd7509164e76(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    if (gblFromModule != "ForgotPassword") frmEnrolluserLandingKA.flxUsernameValidation.isVisible = true;
    frmEnrolluserLandingKA.flxPasswordValidation.isVisible = false;
    if (usernameFlag === true && passwordFlag === true && confrmPasswordFlag === true) {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextEnabled";
        frmEnrolluserLandingKA.flxProgress1.left = "80%";
    } else {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
        frmEnrolluserLandingKA.flxProgress1.left = "65%";
    }
    isClearButtonVisible(frmEnrolluserLandingKA.tbxUsernameKA, frmEnrolluserLandingKA.lblClose);
    frmEnrolluserLandingKA.lblInvalidCredentialsKA.isVisible = false;
}