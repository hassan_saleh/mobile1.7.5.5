function AS_onEndEditingAddr2AddBeneiOS(eventobject, changedtext) {
    return AS_TextField_g31cb93efbdd45bc84076fc8fea6029d(eventobject, changedtext);
}

function AS_TextField_g31cb93efbdd45bc84076fc8fea6029d(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress2.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress2", frmAddExternalAccountKA.lblAddress2.text);
}