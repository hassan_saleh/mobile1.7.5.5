function AS_Button_d2c26cf2e2c545d0ab59513d2702f924(eventobject) {
    var currForm = kony.application.getCurrentForm().id;
    if (currForm !== "frmAccountsLandingKA") {
        if (kony.sdk.isNetworkAvailable()) {
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.left = "0%"
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var controller = INSTANCE.getFormController("frmAccountsLandingKA");
            var navObject = new kony.sdk.mvvm.NavigationObject();
            navObject.setRequestOptions("segAccountsKA", {
                "headers": {
                    "session_token": kony.retailBanking.globalData.session_token
                }
            });
            controller.loadDataAndShowForm(navObject);
            kony.print("Nav accounts Dadhboard:::");
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.print("same form:::");
    }
}