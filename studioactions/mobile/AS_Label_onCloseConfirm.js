function AS_Label_onCloseConfirm(eventobject, x, y) {
    return AS_Label_c970c421cc744e6b89425534d59c9094(eventobject, x, y);
}

function AS_Label_c970c421cc744e6b89425534d59c9094(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.Transfer.cancelTra"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.common.cancel"));

    function onClickYesClose() {
        if (gblTModule == "web") {
            frmManageCardsKA.show();
            popupCommonAlertDimiss();
        } else {
            popupCommonAlertDimiss();
            removeDatafrmTransfersform();
            makeInlineErrorsTransfersInvisible();
            frmPaymentDashboard.show();;
        }
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}