function onSlideEcommerceLimit(eventobject, selectedvalue) {
    return AS_Slider_ba69a556cd064015a94144de7e64efaa(eventobject, selectedvalue);
}

function AS_Slider_ba69a556cd064015a94144de7e64efaa(eventobject, selectedvalue) {
    if (eventobject.id === "sliderECommerceLmt") {
        onTextChange_ECOMMERCELIMIT(frmEnableInternetTransactionKA.txtEcomLimit, frmEnableInternetTransactionKA.sliderECommerceLmt, "onSlide", "internetlimit");
    } else if (eventobject.id === "sliderMailOrderLimit") {
        onTextChange_ECOMMERCELIMIT(frmEnableInternetTransactionKA.txtMailLimit, frmEnableInternetTransactionKA.sliderMailOrderLimit, "onSlide", "mailorderlimit");
    }
}