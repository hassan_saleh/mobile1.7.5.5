function onClickAccountDetailsUnClearedTransactions(eventobject) {
    return AS_FlexContainer_fe50fb1ff5af40699315301599553287(eventobject);
}

function AS_FlexContainer_fe50fb1ff5af40699315301599553287(eventobject) {
    frmAccountDetailKA.transactionSegment.removeAll();
    if (customerAccountDetails.unClearedTransaction === false) {
        frmAccountDetailKA.transactionSegment.setData(kony.retailBanking.globalData.accountsTransactionList.unClearedTransaction);
        customerAccountDetails.unClearedTransaction = true;
        frmAccountDetailKA.CopylblFilter0f7770a92cff247.setEnabled(true);
        frmAccountDetailKA.lblExport.setEnabled(false);
    } else {
        frmAccountDetailKA.transactionSegment.setData(kony.retailBanking.globalData.accountsTransactionList.transaction[0]);
        customerAccountDetails.unClearedTransaction = false;
        frmAccountDetailKA.CopylblFilter0f7770a92cff247.setEnabled(true);
        frmAccountDetailKA.lblExport.setEnabled(true);
    }
}