function AS_Button_acbdf7fc201d43079d9c3a7adb387d06(eventobject) {
    frmEPS.flxDetailsIBAN.setVisibility(false);
    frmEPS.flxDetailsAlias.setVisibility(true);
    if (frmEPS.btnAlias.skin === slButtonBlueFocus) {
        frmEPS.btnAlias.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnIBAN.skin = slButtonBlueFocus;
    } else {
        frmEPS.btnAlias.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnIBAN.skin = slButtonBlueFocus;
    }
}