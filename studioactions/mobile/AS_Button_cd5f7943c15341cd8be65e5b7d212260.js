function AS_Button_cd5f7943c15341cd8be65e5b7d212260(eventobject) {
    function SHOW_ALERT_onClick_45875596f0074e8a8ed7487c91f990e7_True() {
        closeAndroidNav();
        deviceRegFrom = "logout";
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_onClick_45875596f0074e8a8ed7487c91f990e7_False() {}

    function SHOW_ALERT_onClick_45875596f0074e8a8ed7487c91f990e7_Callback(response) {
        if (response === true) {
            SHOW_ALERT_onClick_45875596f0074e8a8ed7487c91f990e7_True();
        } else {
            SHOW_ALERT_onClick_45875596f0074e8a8ed7487c91f990e7_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_onClick_45875596f0074e8a8ed7487c91f990e7_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}