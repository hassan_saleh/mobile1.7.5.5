function AS_TextField_NationalIDRegistration_ExpiryDate_onSelection(eventobject, isValidDateSelected) {
    return AS_Calendar_j2a48395348848b0973109fc575b0980(eventobject, isValidDateSelected);
}

function AS_Calendar_j2a48395348848b0973109fc575b0980(eventobject, isValidDateSelected) {
    var field = frmRegisterUser.calIDExpiryDate;
    animateLabel("UP", "lblIDExpiryDatelblIDExpiryDate", frmRegisterUser.calIDExpiryDate.text);
    frmRegisterUser.lblIDExpiryDate.text = field.dateComponents[2] + "-" + (parseInt(field.dateComponents[1]) < 10 ? "0" + field.dateComponents[1] : field.dateComponents[1]) + "-" + (parseInt(field.dateComponents[0]) < 10 ? "0" + field.dateComponents[0] : field.dateComponents[0])
        // frmRegisterUser.lblIDExpiryDate.text = frmRegisterUser.calIDExpiryDate.dateComponents[2] + "-" + MonthExp + "-" + DayExp ;
        // frmRegisterUser.lblIDExpiryDatelblIDExpiryDate.setVisibility(false);
    frmRegisterUser.lblIDExpiryDate.setVisibility(true);
    isNextEnabledNat();
    frmRegisterUser.flxBorderIDExpiryDate.skin = "skntextFieldDividerGreen";
}