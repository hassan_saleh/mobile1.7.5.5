function AS_Button_a4f30c0f1c644c61a4076f0092745f1e(eventobject) {
    if (frmJoMoPay.btnNext.skin == "jomopaynextEnabled") {
        if (kony.sdk.isNetworkAvailable()) {
            kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
            frmJoMoPayConfirmation.lblTransferType.text = frmJoMoPay.lblTransferType.text;
            frmJoMoPayConfirmation.lblAccountType.text = frmJoMoPay.lblAccountType.text + " " + frmJoMoPay.lblAccountCode.text;
            frmJoMoPayConfirmation.lblType.text = frmJoMoPay.lblType.text;
            // var amount = Number.parseFloat(frmJoMoPay.txtFieldAmount.text).toFixed(3);
            frmJoMoPayConfirmation.lblAmount.text = frmJoMoPay.txtFieldAmount.text;
            if (frmJoMoPay.lblType.text == "Mobile Number") {
                frmJoMoPayConfirmation.btnProfilePhoto.skin = "sknbtnProfileMobile";
                frmJoMoPayConfirmation.btnProfilePhoto.focusSkin = "sknbtnProfileMobile";
                frmJoMoPayConfirmation.btnProfilePhoto.text = "F";
                frmJoMoPayConfirmation.lblPhoneNo.text = frmJoMoPay.txtPhoneNo.text;
            } else {
                var Name = frmJoMoPay.txtAliasDetail.text;
                var splittedName = Name.split(" ");
                if (splittedName.length == 2) {
                    var shortName = splittedName[0][0] + splittedName[1][0];
                } else {
                    if (splittedName[0].length > 1) var shortName = splittedName[0][0] + splittedName[0][1];
                    else var shortName = splittedName[0][0];
                }
                shortName = shortName.toUpperCase();
                frmJoMoPayConfirmation.lblPhoneNo.text = frmJoMoPay.txtAliasDetail.text;
                frmJoMoPayConfirmation.btnProfilePhoto.skin = "sknbtnProfileAlias";
                frmJoMoPayConfirmation.btnProfilePhoto.focusSkin = "sknbtnProfileAlias";
                frmJoMoPayConfirmation.btnProfilePhoto.text = shortName;
            }
            kony.boj.getJomopayFee();
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.print("All details are not filled");
    }
}