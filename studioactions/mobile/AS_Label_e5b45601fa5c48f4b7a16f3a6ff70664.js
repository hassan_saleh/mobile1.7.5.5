function AS_Label_e5b45601fa5c48f4b7a16f3a6ff70664(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.Transfer.cancelTra"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.common.cancel"));

    function onClickYesClose() {
        if (gblTModule == "web") {
            popupCommonAlertDimiss();
            frmManageCardsKA.show();
        } else {
            popupCommonAlertDimiss();
            removeDatafrmTransfersform();
            makeInlineErrorsTransfersInvisible();
            frmPaymentDashboard.show();;
        }
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}