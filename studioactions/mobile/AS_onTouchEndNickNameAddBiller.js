function AS_onTouchEndNickNameAddBiller(eventobject, x, y) {
    return AS_TextField_cefda326474e480aac594724c468c4d6(eventobject, x, y);
}

function AS_TextField_cefda326474e480aac594724c468c4d6(eventobject, x, y) {
    animateLabel("DOWN", "lblBillerNumber", frmAddNewPrePayeeKA.tbxBillerNumber.text);
    animateLabel("DOWN", "lblBillerID", frmAddNewPayeeKA.tbxBillerID.text);
    animateLabel("DOWN", "lblAddress", frmAddNewPayeeKA.tbxAddress.text);
    animateLabel("UP", "lblNickName", frmAddNewPayeeKA.tbxNickName.text);
}