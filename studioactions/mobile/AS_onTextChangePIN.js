function AS_onTextChangePIN(eventobject, changedtext) {
    return AS_TextField_a9b6aaf7339746899319bafac5bfe440(eventobject, changedtext);
}

function AS_TextField_a9b6aaf7339746899319bafac5bfe440(eventobject, changedtext) {
    onTextChangePIN();
    isNextEnabled()
    if (gblFromModule == "UpdateSMSNumber" || gblFromModule == "UpdateMobileNumber") {
        if (frmRegisterUser.txtPIN.text !== null) {
            if (frmRegisterUser.txtPIN.text.length == 4) {
                frmRegisterUser.txtMobileNumber.setFocus(true);
            }
        }
        if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null && frmRegisterUser.txtMobileNumber.text !== null) {
            var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
            if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
                frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
            }
        } else {
            frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
        }
    }
}