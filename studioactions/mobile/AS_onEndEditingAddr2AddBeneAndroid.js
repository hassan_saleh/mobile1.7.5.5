function AS_onEndEditingAddr2AddBeneAndroid(eventobject, changedtext) {
    return AS_TextField_f16ca650da7843d682ee99b74cc0d17d(eventobject, changedtext);
}

function AS_TextField_f16ca650da7843d682ee99b74cc0d17d(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress2.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress2", frmAddExternalAccountKA.lblAddress2.text);
}