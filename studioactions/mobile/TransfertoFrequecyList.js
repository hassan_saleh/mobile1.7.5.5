function TransfertoFrequecyList(eventobject, x, y) {
    return AS_Label_f1a563b73db34019bf8526ea5be2031a(eventobject, x, y);
}

function AS_Label_f1a563b73db34019bf8526ea5be2031a(eventobject, x, y) {
    var amount = frmNewTransferKA.txtBox.text;
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {
        kony.print("Amount is either null or empty");
    } else {
        frmNewTransferKA.lblHiddenAmount.text = frmNewTransferKA.txtBox.text;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(amount, frmNewTransferKA.lblDecimal.text);
    }
    enableNext();
    frmFrequencyList.show();
}