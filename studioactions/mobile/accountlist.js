function accountlist(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_ef182e9bd69845269f08bc53f75c0a0b(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_ef182e9bd69845269f08bc53f75c0a0b(eventobject, sectionNumber, rowNumber) {
    if (gblTModule == "AccNumberStatement") {
        onClickAccReqStmntScreen();
    } else if (gblTModule == "AccountOrder") {
        onClickAccSegToOrderCheckBookScreen();
    } else if (gblTModule == "OpenTermDeposite") {
        onClickAccountDetSegDeposite();
    } else {
        onClickAccountDetailsSegment(2);
    }
}