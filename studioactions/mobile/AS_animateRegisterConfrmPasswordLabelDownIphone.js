function AS_animateRegisterConfrmPasswordLabelDownIphone(eventobject, changedtext) {
    return AS_TextField_g0f69379be7649278bc79502e6601aa6(eventobject, changedtext);
}

function AS_TextField_g0f69379be7649278bc79502e6601aa6(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text) && validateConfirmPassword(frmEnrolluserLandingKA.tbxPasswordKA.text, frmEnrolluserLandingKA.tbxConfrmPwdKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxGreenLine";
        confrmPasswordFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxOrangeLine";
        confrmPasswordFlag = false;
    }
    animateLabel("DOWN", "lblConfirmPassword", frmEnrolluserLandingKA.tbxConfrmPwdKA.text);
}