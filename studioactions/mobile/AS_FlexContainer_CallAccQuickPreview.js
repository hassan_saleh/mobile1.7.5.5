function AS_FlexContainer_CallAccQuickPreview(eventobject) {
    return AS_FlexContainer_a48f8de72f8d43778d38b42590c24dda(eventobject);
}

function AS_FlexContainer_a48f8de72f8d43778d38b42590c24dda(eventobject) {
    if (frmEditAccountSettings.flxShowHideSwitchOff.isVisible === true) {
        frmEditAccountSettings.flxShowHideSwitchOn.isVisible = true;
        frmEditAccountSettings.flxShowHideSwitchOff.isVisible = false;
        frmAuthorizationAlternatives.flxAccounts.isVisible = false;
        frmAuthorizationAlternatives.btnQuickCancel.isVisible = true;
        //frmAuthorizationAlternatives.show();
        frmAuthorizationAlternatives.btnSaveQuickBal.isVisible = true;
        frmAuthorizationAlternatives.lblQuickAccess.text = geti18nkey("i18n.appsettings.QuickBalance");
        frmAuthorizationAlternatives.lblQuickAccessBody.text = geti18nkey("i18n.quickBal.quickDescSettings");
        getAccountDataforQuickPreviewAccounts();
    } else {
        frmEditAccountSettings.flxShowHideSwitchOn.isVisible = false;
        frmEditAccountSettings.flxShowHideSwitchOff.isVisible = true;
    }
}