function rowClickofSeg(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_db112cf714c549f286cc83e0352af7af(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_db112cf714c549f286cc83e0352af7af(eventobject, sectionNumber, rowNumber) {
    if (flagtype == flagtransfertype) {
        frmJoMoPay.lblTransferType.text = frmJoMoPay.segPopup.selectedItems[0].lblJP;
    } else if (flagtype == flagaccounttype) {
        frmJoMoPay.lblAccountType.text = frmJoMoPay.segPopup.selectedItems[0].lblJP;
    } else if (flagtype == flagjomopaytype) {
        frmJoMoPay.lblType.text = frmJoMoPay.segPopup.selectedItems[0].lblJP;
        if (frmJoMoPay.lblType.text === geti18Value("i18n.jomopay.benificiaryalias")) {
            aliasselected();
        } else if (frmJoMoPay.lblType.text === geti18Value("i18n.jomopay.mobile")) {
            mobileselected();
        } else corporateselected();
    }
    frmJoMoPay.flxPopupOuter.setVisibility(false);
}