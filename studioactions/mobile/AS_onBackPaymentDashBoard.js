function AS_onBackPaymentDashBoard(eventobject) {
    return AS_Form_d761e9d68b874d39821248fc02f7ed7a(eventobject);
}

function AS_Form_d761e9d68b874d39821248fc02f7ed7a(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        frmAccountsLandingKA.flxDeals.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
        frmAccountsLandingKA.flxAccountsMain.left = "0%"
        frmAccountsLandingKA.flxLoans.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
        frmAccountsLandingKA.flxDeals.zIndex = 1;
        frmAccountsLandingKA.flxLoans.zIndex = 1;
        frmAccountsLandingKA.forceLayout();
        accountDashboardDataCall();
        kony.print("Nav accounts Dadhboard:::");
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}