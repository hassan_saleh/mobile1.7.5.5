function AS_onDoneToSI(eventobject, changedtext) {
    return AS_TextField_j85da92a09c34e6f8317f10a23bc39a6(eventobject, changedtext);
}

function AS_TextField_j85da92a09c34e6f8317f10a23bc39a6(eventobject, changedtext) {
    check_AmountDecimal(frmFilterSI.txtAmountRangeTo, frmFilterSI.lblCurrency.text, frmFilterSI.flxAmountMaxLine);
    if (isNaN(frmFilterSI.txtAmountRangeTo.text)) frmFilterSI.txtAmountRangeTo.text = "";
}