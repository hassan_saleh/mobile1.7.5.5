function AS_Label_d0dbc00abaa841258ba6ccd6d4dabbef(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if ((currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") && currentForm.passwordTextField.text !== "") {
        if (frmLoginKA.lblShowPass.text === "B") {
            currentForm.passwordTextField.secureTextEntry = true;
            frmLoginKA.lblShowPass.text = "A";
        } else {
            currentForm.passwordTextField.secureTextEntry = false;
            frmLoginKA.lblShowPass.text = "B";
        }
    }
}