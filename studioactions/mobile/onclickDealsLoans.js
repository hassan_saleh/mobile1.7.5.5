function onclickDealsLoans(eventobject) {
    return AS_Button_c1b983d863754d019ac8a4b4569e790d(eventobject);
}

function AS_Button_c1b983d863754d019ac8a4b4569e790d(eventobject) {
    frmAccountsLandingKA.btnNewAccount.setVisibility(false); //hide open sub account
    frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
    if (frmAccountsLandingKA.segAccountsKADeals.data !== null && frmAccountsLandingKA.segAccountsKADeals.data.length === 1) {
        depositOnetimeCall();
    } else {
        dealsAnimation();
    }
}