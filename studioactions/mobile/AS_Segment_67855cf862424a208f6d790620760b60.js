function AS_Segment_67855cf862424a208f6d790620760b60(eventobject, sectionNumber, rowNumber) {
    onclicksegment("from");
    selectAccountCard(frmNewTransferKA, "from", 1);
    var index = getSegInternalAccountsKARecordClick("segInternalFromAccountsKA", "frmNewTransferKA", "fromNamePick", "fromAmountPick", "fromlblAccountNumberKA", "fromAccountColorPick");
    setToTransferAccountsAfterSelectionOfFromAccount("frmNewTransferKA", "segInternalTOAccountsKA", index);
    unSelectSameAccount();
}