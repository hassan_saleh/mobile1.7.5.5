function devBackAccDetails(eventobject) {
    return AS_Form_a95e76c015d94d549ce9ccdb5503220e(eventobject);
}

function AS_Form_a95e76c015d94d549ce9ccdb5503220e(eventobject) {
    if (kony.newCARD.applyCardsOption === true) {
        frmApplyNewCardsKA.show();
        kony.newCARD.applyCardsOption = false;
    } else if (kony.application.getPreviousForm().id === "frmCardLinkedAccounts") {
        frmCardLinkedAccounts.show();
    } else if (gblTModule == "AccNumberStatement") {
        onClickAccBackToReqStmntScreen();
    } else if (gblTModule == "AccountOrder") {
        onClickAccBackToOrderCheckBookScreen();
    } else if (gblTModule === "BillPrePayNewBillKA") {
        frmPrePaidPayeeDetailsKA.show();
    } else {
        backFromAccDetails();
    }
}