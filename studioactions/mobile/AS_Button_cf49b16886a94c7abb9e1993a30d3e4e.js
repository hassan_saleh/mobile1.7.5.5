function AS_Button_cf49b16886a94c7abb9e1993a30d3e4e(eventobject) {
    function SHOW_ALERT_ide_onClick_c1c9f62f2b084c1a86a07c26921c711d_True() {
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_c1c9f62f2b084c1a86a07c26921c711d_False() {}

    function SHOW_ALERT_ide_onClick_c1c9f62f2b084c1a86a07c26921c711d_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_c1c9f62f2b084c1a86a07c26921c711d_True();
        } else {
            SHOW_ALERT_ide_onClick_c1c9f62f2b084c1a86a07c26921c711d_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_c1c9f62f2b084c1a86a07c26921c711d_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}