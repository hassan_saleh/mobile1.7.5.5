function AS_onDoneFrmSI(eventobject, changedtext) {
    return AS_TextField_c5b6b757a9414e04bf4805be62e21103(eventobject, changedtext);
}

function AS_TextField_c5b6b757a9414e04bf4805be62e21103(eventobject, changedtext) {
    check_AmountDecimal(frmFilterSI.txtAmountRangeFrom, frmFilterSI.lblCurrency.text, frmFilterSI.flxAmountMinLine);
    if (isNaN(frmFilterSI.txtAmountRangeFrom.text)) frmFilterSI.txtAmountRangeFrom.text = "";
}