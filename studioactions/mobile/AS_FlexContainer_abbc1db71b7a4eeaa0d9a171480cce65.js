function AS_FlexContainer_abbc1db71b7a4eeaa0d9a171480cce65(eventobject) {
    if (gblFromModule == "RegisterUser") {
        frmRegisterUser.show();
    } else if (gblFromModule == "ChangeUsername") {
        frmSettingsKA.show();
    } else if (gblFromModule == "ChangePassword") {
        frmSettingsKA.show();
    } else {
        frmEnrolluserLandingKA.lblFlag.text = "F";
        if (gblReqField == "Username") {
            frmLoginKA.show();
        } else {
            frmRegisterUser.show();
        }
    }
    frmNewUserOnboardVerificationKA.destroy();
    //frmEnrolluserLandingKA.destroy();
}