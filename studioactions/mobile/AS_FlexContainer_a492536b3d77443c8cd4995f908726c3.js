function AS_FlexContainer_a492536b3d77443c8cd4995f908726c3(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountsLandingKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("segAccountsKA", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    controller.loadDataAndShowForm(navObject);
}