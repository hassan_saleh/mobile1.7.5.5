function AS_Button_fd659751407c4d6884f57ac2693fc98e(eventobject) {
    ShowLoadingScreen();
    gblToOTPFrom = "ConfirmPayBill";
    //callRequestOTP();
    if (frmConfirmPrePayBill.flxTotalAmount.isVisible === true) {
        var amnt = frmConfirmPrePayBill.lblTotalAmount.text;
        amnt = amnt.substring(0, amnt.length - 4);
        if (parseFloat(kony.store.getItem("BillPayfromAcc").availableBalance) >= parseFloat(amnt)) {
            performBillPayPrePaid();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInCardAcc"), popupCommonAlertDimiss, "");
        }
    } else {
        performBillPayPrePaid();
    }
}