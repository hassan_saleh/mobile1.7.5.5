function ActiontoLogutfromDocuments(eventobject) {
    return AS_Button_e0ad2e0424784a8ea71307fa1c6a1948(eventobject);
}

function AS_Button_e0ad2e0424784a8ea71307fa1c6a1948(eventobject) {
    function SHOW_ALERT__acecc6791bdc410490e93740e7cbe3ad_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT__acecc6791bdc410490e93740e7cbe3ad_False() {}

    function SHOW_ALERT__acecc6791bdc410490e93740e7cbe3ad_Callback(response) {
        if (response === true) {
            SHOW_ALERT__acecc6791bdc410490e93740e7cbe3ad_True();
        } else {
            SHOW_ALERT__acecc6791bdc410490e93740e7cbe3ad_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__acecc6791bdc410490e93740e7cbe3ad_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}