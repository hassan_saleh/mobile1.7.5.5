function AS_Form_CardlessTransactionOnDeviceBack(eventobject) {
    return AS_Form_df62ef33d09644ffb89d27ae389f1ab4(eventobject);
}

function AS_Form_df62ef33d09644ffb89d27ae389f1ab4(eventobject) {
    if (frmCardlessTransaction.flxBody.isVisible === false) {
        frmCardlessTransaction.flxConfirm.isVisible = false;
        frmCardlessTransaction.flxBody.isVisible = true;
        frmCardlessTransaction.btnNextCardless.isVisible = true;
    } else if (frmCardlessTransaction.BrowserTCCardless.isVisible === true) {
        frmCardlessTransaction.BrowserTCCardless.isVisible = false;
        frmCardlessTransaction.flxBody.isVisible = true;
    } else {
        if (previous_FORM !== null && previous_FORM.id === "frmAccountsLandingKA") {
            back_CARDLESS_TRANSACTION();
        } else {
            customAlertPopup(geti18Value("i18n.common.AreYouSure"), geti18Value("i18n.Bene.backFromBeneError"), kony.boj.onClickYesBackAddCardless, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
            show_Cardless_List("all");
            //kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        }
        frmManageCardLess.btnAll.skin = "slButtonWhiteTab";
        frmManageCardLess.btnPending.skin = "slButtonWhiteTabDisabled";
        frmManageCardLess.btnAccepted.skin = "slButtonWhiteTabDisabled";
        frmManageCardLess.btnCancel.skin = "slButtonWhiteTabDisabled";
    }
}