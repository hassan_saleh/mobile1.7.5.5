function AS_FlexContainer_transactionSwitchOff(eventobject) {
    return AS_FlexContainer_d23e8aa0f78a492cb66e249abdbeff7f(eventobject);
}

function AS_FlexContainer_d23e8aa0f78a492cb66e249abdbeff7f(eventobject) {
    if (frmAuthorizationAlternatives.flxSwitchOn.isVisible === true) {
        frmAuthorizationAlternatives.flxSwitchTransactionsOn.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchTransactionOff.isVisible = false;
    }
}