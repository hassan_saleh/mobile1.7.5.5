function AS_TextField_b474439d8f8a452bb73fd97ddfa5eae1(eventobject, x, y) {
    try {
        var currentForm = kony.application.getCurrentForm();
        if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
            currentForm.borderBottom.skin = "skntextFieldDividerBlue";
            if (currentForm.passwordTextField.text !== null && currentForm.passwordTextField.text !== "") {
                currentForm.lblClosePass.setVisibility(true);
                frmLoginKA.lblShowPass.setVisibility(true);
                currentForm.flxbdrpwd.skin = "skntextFieldDividerGreen";
            } else {
                currentForm.lblClosePass.setVisibility(false);
                frmLoginKA.lblShowPass.setVisibility(false);
                currentForm.flxbdrpwd.skin = "skntextFieldDivider";
            }
            currentForm.lblClose.setVisibility(true);
            currentForm.lblInvalidCredentialsKA.isVisible = false;
        }
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}