function AS_Button_cd951c39e80142dc8e104c90b4a56ed1(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navigationObject.setRequestOptions("form", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    listController.performAction("navigateTo", ["frmAddNewPayeeKA", navigationObject]);
}