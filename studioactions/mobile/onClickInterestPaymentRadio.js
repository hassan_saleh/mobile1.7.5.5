function onClickInterestPaymentRadio(eventobject) {
    return AS_Button_i2c92883041d44448c155f46ca5079cd(eventobject);
}

function AS_Button_i2c92883041d44448c155f46ca5079cd(eventobject) {
    if (eventobject.id === "btnEndOfLoan") {
        radio_SELECTION_INTEREST_PAYMENT(frmLoanPostpone.btnEndOfLoan, frmLoanPostpone.btnCurLoanInst, 1);
    } else if (eventobject.id === "btnCurLoanInst") {
        radio_SELECTION_INTEREST_PAYMENT(frmLoanPostpone.btnEndOfLoan, frmLoanPostpone.btnCurLoanInst, 2);
    }
}