function AS_OnClickIPSPaymentHistory(eventobject) {
    return AS_Button_j32e139de27741d99e33de932d6d5b90(eventobject);
}

function AS_Button_j32e139de27741d99e33de932d6d5b90(eventobject) {
    // frmEPS.flxMain.setVisibility(true);
    // frmEPS.flxConfirmEPS.setVisibility(false);
    // frmEPS.show();
    IPSPaymentHistory = [];
    IPSHistorySeg = [];
    frmIPSRequests.flxNote.setVisibility(false);
    frmIPSRequests.segIPSHistory.setData([]);
    frmIPSRequests.flxReturnPayment.setVisibility(false);
    frmIPSRequests.flxRequestToPay.setVisibility(false);
    frmIPSRequests.flxPaymentHistory.setVisibility(true);
    frmIPSRequests.skin = "CopyslFormCommon0aaa3928efb3d48";
    frmIPSRequests.flxConfirmEPS.setVisibility(false);
    gblTModule = "CLIQHistory";
    LoadIPSRequests();
    //frmIPSRequests.show();
}