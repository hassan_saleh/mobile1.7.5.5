function onTextChangeApplyCardsRequestLimit(eventobject, changedtext) {
    return AS_TextField_e30846faa3e2449c9b8361ac6331f246(eventobject, changedtext);
}

function AS_TextField_e30846faa3e2449c9b8361ac6331f246(eventobject, changedtext) {
    if (new RegExp(/[^0-9]/g).test(frmApplyNewCardsKA.txtRequestLimit.text)) {
        frmApplyNewCardsKA.flxBorderRequestLimit.skin = "sknFlxOrangeLine";
    } else {
        validate_APPLYCARDSDETAILS();
        if (kony.newCARD.cardTYPE === "UpgradeCreditCard") {
            if (frmApplyNewCardsKA.lblPrimaryCardNumber.text !== "") {
                frmApplyNewCardsKA.flxBorderRequestLimit.skin = "skntextFieldDividerGreen";
                show_CARD_TYPE(frmApplyNewCardsKA.txtRequestLimit.text, kony.store.getItem("frmAccount").card_limit);
            } else {
                frmApplyNewCardsKA.txtRequestLimit.text = "";
                frmApplyNewCardsKA.flxBorderCreditCardNumber.skin = "sknFlxOrangeLine";
            }
        } //else	frmApplyNewCardsKA.flxBorderRequestLimit.skin = "skntextFieldDividerGreen";
    }
}