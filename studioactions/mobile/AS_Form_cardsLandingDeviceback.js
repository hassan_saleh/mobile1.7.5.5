function AS_Form_cardsLandingDeviceback(eventobject) {
    return AS_Form_i419549f02754d81ae908afb41f0b73c(eventobject);
}

function AS_Form_i419549f02754d81ae908afb41f0b73c(eventobject) {
    if (frmCardsLandingKA.lblBack.text !== geti18Value("i18n.billsPay.Cards")) {
        if (kony.sdk.isNetworkAvailable()) {
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.left = "0%"
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();
            kony.print("Nav accounts Dadhboard:::");
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        switch_CARDSLANDING_APPLYCARDS("flxScrollMain");
    }
}