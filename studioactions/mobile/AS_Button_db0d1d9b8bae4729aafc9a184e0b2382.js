function AS_Button_db0d1d9b8bae4729aafc9a184e0b2382(eventobject) {
    //frmSettingsKA.show();
    if (frmAccountsReorderKA.lblSettingsTitle.text === geti18nkey("i18n.appsettings.reordercardlist")) {
        callSetCardPreferences();
    } else {
        callSetQucikAccessList();
    }
}