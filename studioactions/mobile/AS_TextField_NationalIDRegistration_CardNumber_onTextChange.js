function AS_TextField_NationalIDRegistration_CardNumber_onTextChange(eventobject, changedtext) {
    return AS_TextField_de2516fe81e24bf8b2bed2eb3f8bd174(eventobject, changedtext);
}

function AS_TextField_de2516fe81e24bf8b2bed2eb3f8bd174(eventobject, changedtext) {
    if (frmRegisterUser.txtIDCardNumber.text === null || frmRegisterUser.txtIDCardNumber.text === "" || frmRegisterUser.txtIDCardNumber.text.trim() === "") frmRegisterUser.flxBorderIDCardNumber.skin = "skntextFieldDividerOrange";
    else frmRegisterUser.flxBorderIDCardNumber.skin = "skntextFieldDividerGreen";
}