function AS_backtoAccountsfrommoreDashboard(eventobject) {
    return AS_FlexContainer_c416ccb05fc048c4bc9c9a0b3c863483(eventobject);
}

function AS_FlexContainer_c416ccb05fc048c4bc9c9a0b3c863483(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        frmAccountsLandingKA.flxDeals.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
        frmAccountsLandingKA.flxAccountsMain.left = "0%"
        frmAccountsLandingKA.flxLoans.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
        frmAccountsLandingKA.flxDeals.zIndex = 1;
        frmAccountsLandingKA.flxLoans.zIndex = 1;
        frmAccountsLandingKA.forceLayout();
        accountDashboardDataCall();
        kony.print("Nav accounts Dadhboard:::");
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}