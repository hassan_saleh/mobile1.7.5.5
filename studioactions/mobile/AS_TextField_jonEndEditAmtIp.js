function AS_TextField_jonEndEditAmtIp(eventobject, changedtext) {
    return AS_TextField_jf64c673d56c46cc96567dd2c966fd7d(eventobject, changedtext);
}

function AS_TextField_jf64c673d56c46cc96567dd2c966fd7d(eventobject, changedtext) {
    var amount = frmNewTransferKA.txtBox.text;
    if (amount === null || amount === "" || amount == "." || amount === 0.00) {
        frmNewTransferKA.flxInvalidAmountField.setVisibility(true);
        frmNewTransferKA.lblLine3.skin = "sknFlxOrangeLine";
        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
        frmNewTransferKA.lblNext.setEnabled(false);
    } else {
        frmNewTransferKA.txtBox.maxTextLength = 20;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(frmNewTransferKA.lblHiddenAmount.text, getDecimalNumForCurr(frmNewTransferKA.lblFromAccCurr.text));
    }
    getCommisionFee();
}